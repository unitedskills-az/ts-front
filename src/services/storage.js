export default class Storage {
  static langKey = "lang";
  static tokenKey = "accessToken";
  static type = "accountType";

  static get(key) {
    const str = localStorage.getItem(key);
    return str || "";
  }

  static set(key, value) {
    localStorage.setItem(key, value);
  }

  static getLang() {
    return localStorage.getItem(Storage.langKey);
  }

  static setLang(lang) {
    localStorage.setItem(Storage.langKey, lang);
  }

  static setType(type) {
    localStorage.setItem(Storage.type, type);
  }

  static getToken() {
    return localStorage.getItem(Storage.tokenKey);
  }

  static setToken(token) {
    localStorage.setItem(Storage.tokenKey, token);
  }

  static clear() {
    localStorage.clear();
  }

  static getItem(item) {
    return localStorage.getItem(item);
  }
}
