import { createBrowserHistory } from "history";

const history = createBrowserHistory();

export const redirect = (path) => {
  return history.push(path);
};

export default history;
