import { filters } from "./includes";

export const URLS = {
  //Login
  LOGIN: (email, password, isRemember) =>
    `/login?email=${email}&password=${password}&remember_me=${isRemember}`,
  REGISTER: (email, password) =>
    `/register?email=${email}&password=${password}`,
  LOG_OUT: () => `/logout`,
  FORGOT_PASSWORD: (email) => `/password/forgot?email=${email}`,
  RESET_PASSWORD: (token, password) =>
    `/password/reset?token=${token}&password=${password}`,

  //Company info
  COMPANY__SELECTABLE: () =>
    `/selectable?include_relation=${filters.toString()}`,
  SELECTABLE__COUNTRY: () => `/selectable/country`,
  SELECTABLE__COUNTRY__ITEMS: (id) => `/selectable/country${id}`,
  BUSINESS__TYPE: () => "/suppliers/business-type",
  COMPANY__INFO: () => "/suppliers/company-information",
  COMPANY__CAPABILITY: () => `/suppliers/export-capability`,
  COMPANY__INTRODUCTION: () => `/suppliers/company-introduction`,
  MANUFACTURING__INTRODUCTION: () => `/suppliers/manufacturing-capability`,
  QUALITY__CONTROL: () => `/suppliers/quality-control`,
  ADD__PRODUCTS: () => `/products`,
  GET__PRODUCTS: (products, page) => `/products?${products}&page=${page}`,
  GET__PRODUCTS__DETAIL: (slug) => `/products/${slug}`,
  GET__SUPPLIERS__PRODUCTS: (slug, page) => `/supplier/products?page=${page}`,
  GET__CURRENT__SUPPLIER: () => `/supplier`,
  CATEGORY: () => `/selectable/category?include_relation=children`,
  CATEGORY__ITEMS: (id) =>
    `/selectable/category/${id}?include_relation=children`,

  //Profile
  GET_USER: () => `/profile`,
  GET__SUPPLIERS: (slug) => `/suppliers${slug}`,
  // verification
  POST__VERIFICATION: () => `/suppliers/verification-detail`,
};
