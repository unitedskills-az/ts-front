const filters = [
  "businessTypes",
  "quantityTypes",
  "industries",
  "productBusinessTypes",
  "totalAnnualRevenues",
  "employeesInTrades",
  "qualityControlStaffs",
  "employeesInTrades",
  "deliveryTerms",
  "languageSpokens",
  "paymentTypes",
  "currencies",
  "distributions",
];

export { filters };
