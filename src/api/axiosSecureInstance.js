import axios from "axios";

import Storage from "../services/storage";
import { baseURL } from "./url";

export const axiosConfig = {
  baseURL,
  timeout: 100000,
  // withCredentials: true,
  // contentType: "application/json"
};

export const getSecureAxios = () => {
  const headers = { "Content-Type": "application/json" };

  const instance = axios.create({
    ...axiosConfig,
    headers,
  });

  instance.interceptors.request.use(
    (config) => {
      if (!!Storage.getToken()) {
        config.headers["authorization"] = "Bearer " + Storage.getToken();
      }
      return config;
    },
    (error) => Promise.reject(error)
  );

  return instance;
};
