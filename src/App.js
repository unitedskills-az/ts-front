import { Switch, Route, withRouter } from "react-router-dom";

// page
import Login from "./pages/Authentication/Login";
import Register from "./pages/Authentication/Register";
import ResetPassword from "./pages/Authentication/ResetPassword";
import LoginWithSocials from "./pages/Authentication/LoginWithSocials";
import Homepage from "./pages/Homepage/Homepage";
import About from "./pages/About/About";
import Terms from "./pages/Terms";
import ProductPage from "./pages/Company/ProductPage";
import ProductPageSuppliers from "./pages/Product/ProductsCompanySuppliers";
import AboutCompany from "./pages/Company/AboutCompany";
import Suppliers from "./pages/Suppliers";
import Products from "./pages/Product/Products";
import Contact from "./pages/Contact";
import Favorites from "./pages/Favorites";
import ProductDetail from "./pages/ProductDetail";
import BuyerAccount from "./pages/Account/Buyer/Account";
import DashboardSupplier from "./pages/Account/Suppliers/Dashboard";
import CompanyInfo from "./pages/Account/Suppliers/CompanyInfo";
import TrainingCenter from "./pages/Account/Suppliers/TrainingCenter";
import Analytics from "./pages/Account/Suppliers/Analytics";
import MessagesSupplier from "./pages/Account/Suppliers/Messages";
import OrdersSupplier from "./pages/Account/Suppliers/Orders";
import Support from "./pages/Account/Suppliers/Support";
import SupplierProducts from "./pages/Account/Suppliers/Products";
import AddProduct from "./pages/Account/Suppliers/AddNewProduct";
import RfqSupplier from "./pages/Account/Suppliers/Rfq";
import Rfq from "./pages/Rfq";
import RfqBuyer from "./pages/Account/Buyer/Rfq";
import MessagesBuyer from "./pages/Account/Buyer/Messages";
import OrdersBuyer from "./pages/Account/Buyer/Orders";
import ChatBuyer from "./pages/Account/Buyer/Chat";
import ChatSupplier from "./pages/Account/Suppliers/Chat";
import OrdersChatBuyer from "./pages/Account/Buyer/ChatOrders";
import OrdersChatSupplier from "./pages/Account/Suppliers/OrderChat";
import DashboardBuyer from "./pages/Account/Buyer/Dashboard";
import VerifyAccount from "./pages/Account/Suppliers/VerifyAccount";
import Header from "./components/Header";
import HeaderBottom from "./components/Header/HeaderBottom";
import ProtectRouter from "./protectRouter/protectRouter";
import { mainRoutes } from "./routes";
import { connect } from "react-redux";

const App = ({ isAuth, accountType, location }) => {
  return (
    <div className="App">
      <Header />
      <ProtectRouter
        isStatic={true}
        isShow={location}
        isAuth={isAuth}
        component={<HeaderBottom />}
      />
      <Switch>
        <ProtectRouter
          path={mainRoutes.login}
          isAuth={isAuth}
          isLogin={true}
          component={<Login />}
        />
        <Route exact path={mainRoutes.home} component={Homepage} />
        <Route exact path={mainRoutes.register} component={Register} />
        <Route path={mainRoutes.resetPassword} component={ResetPassword} />
        <Route path={mainRoutes.loginWithSocials} component={LoginWithSocials} />
        <Route exact path="/contact" component={Contact} />
        <Route exact path="/about" component={About} />
        <Route exact path="/terms" component={Terms} />
        <Route exact path="/products-page" component={ProductPage} />
        <Route
          exact
          path="/products/:id/suppliers"
          component={ProductPageSuppliers}
        />
        <Route exact path="/about-company/:slug" component={AboutCompany} />
        <ProtectRouter
          path="/suppliers"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<Suppliers />}
        />
        <Route path="/products" component={Products} />
        <ProtectRouter
          path="/buyer/favorites"
          isBuyer={accountType}
          isAuth={isAuth}
          exact
          component={<Favorites />}
        />
        <Route path="*/product-detail/:slug" component={ProductDetail} />
        <ProtectRouter
          path="/supplier/dashboard"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<DashboardSupplier />}
        />
        <ProtectRouter
          path="/supplier/company-info"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<CompanyInfo />}
        />
        <ProtectRouter
          path="/supplier/training-center"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<TrainingCenter />}
        />
        <ProtectRouter
          path="/supplier/analytics"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<Analytics />}
        />
        <ProtectRouter
          path="/supplier/messages"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<MessagesSupplier />}
        />
        <ProtectRouter
          path="/supplier/orders"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<OrdersSupplier />}
        />
        <ProtectRouter
          path="/supplier/support"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<Support />}
        />
        <ProtectRouter
          path="/supplier/products"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<SupplierProducts />}
        />
        <ProtectRouter
          path="/supplier/add-product"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<AddProduct />}
        />
        <ProtectRouter
          path="/supplier/rfq"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<RfqSupplier />}
        />

        <ProtectRouter
          path="/supplier/chat"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<ChatSupplier />}
        />

        <ProtectRouter
          path="/supplier/chat/orders"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<OrdersChatSupplier />}
        />

        <ProtectRouter
          path="/supplier/verify-account"
          isSuppliers={accountType}
          isAuth={isAuth}
          exact
          component={<VerifyAccount />}
        />
        <ProtectRouter
          path="/buyer/account"
          isBuyer={accountType}
          isAuth={isAuth}
          exact
          component={<BuyerAccount />}
        />
        <ProtectRouter
          path="/buyer/chat"
          isBuyer={accountType}
          isAuth={isAuth}
          exact
          component={<ChatBuyer />}
        />
        <ProtectRouter
          path="/buyer/rfq"
          isBuyer={accountType}
          isAuth={isAuth}
          exact
          component={<RfqBuyer />}
        />
        <ProtectRouter
          path="/buyer/messages"
          isBuyer={accountType}
          isAuth={isAuth}
          exact
          component={<MessagesBuyer />}
        />
        <ProtectRouter
          path="/buyer/orders"
          isBuyer={accountType}
          isAuth={isAuth}
          exact
          component={<OrdersBuyer />}
        />
        <ProtectRouter
          path="/buyer/chat/orders"
          isBuyer={accountType}
          isAuth={isAuth}
          exact
          component={<OrdersChatBuyer />}
        />
        <ProtectRouter
          path="/buyer/dashboard"
          isBuyer={accountType}
          isAuth={isAuth}
          exact
          component={<DashboardBuyer />}
        />
        <Route exact path="/rfq" component={Rfq} />
      </Switch>
    </div>
  );
};

export default connect((state) => ({
  isAuth: state.isAuth,
  accountType: state.accountType,
}))(withRouter(App));
