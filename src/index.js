import { BrowserRouter } from "react-router-dom";
import ReactDOM from "react-dom";
import { QueryClient, QueryClientProvider } from "react-query";
import { Provider } from "react-redux";

import App from "./App";
import { mainRoutes } from "./routes";
import history from "./services/history";
import configureStore from "./configureStore";

import "bootstrap/dist/css/bootstrap.min.css";
import "./index.scss";
import "./css-minify.scss";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      retry: false,
    },
    mutations: {
      refetchOnWindowFocus: false,
      retry: false,
    },
  },
});

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter basename={mainRoutes.home} history={history}>
      <QueryClientProvider client={queryClient}>
        <App />
      </QueryClientProvider>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
