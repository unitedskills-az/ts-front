import {combineReducers, compose, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import {accountType, isAuth} from "./pages/Authentication/common/redux/reducers";

const rootReducer = combineReducers({
  login: {},
  profile: {},
  accountType: accountType,
  isAuth: isAuth
});

const configureStore = () => {
  const composeEnhancer =
    process.env.NODE_ENV === "development" ? composeWithDevTools : compose;
  const store = createStore(rootReducer, composeEnhancer());

  return store;
};

export default configureStore;
