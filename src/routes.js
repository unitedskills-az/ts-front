import resetPasswordRoutes from "./pages/Authentication/ResetPassword/routes";

export const mainRoutes = {
  home: "/",
  login: "/login",
  register: "/register",
  loginWithSocials: "/logininformation",
  buyerDashboard: "buyer/dashboard",
  ...resetPasswordRoutes,
};
