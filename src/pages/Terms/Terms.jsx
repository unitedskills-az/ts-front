import React from 'react';
import './Terms.scss';
import SideBar from '../../components/sidebar/staticSidebar/sidebar';
const Terms = () => {
    return (
        <div className="terms">
            <div className="container">
                <div className="row">
                    <div className="xl-2 lg-2 md-2 sm-12">
                        <SideBar />
                    </div>
                    <div className="xl-8 lg-8 md-8 sm-12">
                        <h5 className="montserrat font-s-normal font-w-bold font-32 line-h-150_ c-light-black ">
                            Made-in-China.com, service website, owned and operated by United Skills LLC
                        </h5>
                        <div className="terms-content">
                            <h4 className="title">
                                1. Acceptance
                            </h4>
                            <p>1.1	By using Made-in-China.com, you acknowledge the terms and conditions hereof and agree that you will be bound by the terms and conditions contained in this Agreement.</p>
                            <p>1.2	In addition, this Agreement may be changed or updated by FOCUS from time to time without any prior notice to you. Changed terms and conditions will become valid upon being posted on Made-in-China.com. Your continuous use of our Service signifies your acceptance of the changed Agreement.</p>
                            <p>1.3	Unless explicitly stated otherwise, any further upgrade, modification, addition or change to the Service, or any new function that augments or enhances our Service shall be subject to this Agreement. You may stop using the Service at any time.</p>
                            <p>1.4	This Agreement may not be otherwise modified except in writing signed by an authorized officer of  FOCUS.</p>
                            <h4 className="title">
                                2. Users
                            </h4>
                            <p>1.1	By using Made-in-China.com, you acknowledge the terms and conditions hereof and agree that you will be bound by the terms and conditions contained in this Agreement.</p>
                            <p>1.2	In addition, this Agreement may be changed or updated by FOCUS from time to time without any prior notice to you. Changed terms and conditions will become valid upon being posted on Made-in-China.com. Your continuous use of our Service signifies your acceptance of the changed Agreement.</p>
                            <p>1.3	Unless explicitly stated otherwise, any further upgrade, modification, addition or change to the Service, or any new function that augments or enhances our Service shall be subject to this Agreement. You may stop using the Service at any time.</p>
                            <p>1.4	This Agreement may not be otherwise modified except in writing signed by an authorized officer of  FOCUS.</p>
                        </div>
                    </div>
                    <div className="xl-2 lg-2 md-2 sm-12"></div>
                </div>
            </div>
        </div>
    )
}
export default Terms