import { useState } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { TextField, ButtonLoader } from "../../../components";

import { useAuth } from "../common/hooks";
import { mainRoutes } from "../../../routes";

import "./Login.scss";

import Google from "../../../assets/google.svg";
import Facebook from "../../../assets/facebook.svg";

import { CHANGE__ACCOUNT__TYPE, IS__AUTH } from "../common/redux/actionTypes";

const Login = ({ handleAccount, handleIsAuth }) => {
  const { handleSignIn, isSignInLoading } = useAuth(
    handleAccount,
    handleIsAuth
  );

  const [state, setState] = useState({
    email: "",
    password: "",
    isRemember: false,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({ ...prev, [name]: value }));
  };

  const handleClick = () => {
    handleSignIn(state);
  };

  const isDisabled =
    isSignInLoading || !state.email.trim() || !state.password.trim();

  return (
    <div className="login">
      <div className="container">
        <div className="login-content">
          <Link
            to={mainRoutes.home}
            className="logo font-w-bold font-s-normal font-24 lineh-125_ c-dark-gray"
          >
            Turkey <br />
            Suppliers
          </Link>
          <div className="auth-container b-white br-12">
            <div className="auth-container-inner">
              <form action="">
                <div className="df aic jcsb mb-48 resp-mb-32">
                  <span className="font-s-normal font-w-bold font-20 line-h-100_ c-dark-gray montserrat">
                    Sign in
                  </span>

                  <span className="df aic font-w-normal font-14 font-s-normal c-gray auth-a markpro">
                    Need account?{" "}
                    <Link to={mainRoutes.register} className="c-red ml-6">
                      Sign up
                    </Link>
                  </span>
                </div>
                <div className="df aic jcsb mb-32">
                  <a
                    href="https://test.turkey-suppliers.com/auth/google"
                    className="login-google font-s-normal font-w-normal font-14 line-h-100_ c-black df aic jcc h-42 px-14"
                  >
                    <img src={Google} alt="google" className="mr-8 w-20 h-20" />
                    Google
                  </a>
                  <a
                    href="https://test.turkey-suppliers.com/auth/facebook"
                    className="login-facebook font-s-normal font-w-normal font-14 line-100_ c-white df aic jcc h-40 px-10"
                  >
                    <img
                      src={Facebook}
                      alt="facebook"
                      className="mr-8 w-10 h-18"
                    />
                    Facebook
                  </a>
                </div>
                <div className="df fdc mb-24">
                  <label
                    htmlFor=""
                    className="font-s-normal font-w-normal font-14 line-h-100_ c-dark-gray mb-12"
                  >
                    Mail Address
                  </label>
                  <TextField
                    name="email"
                    value={state.email}
                    onChange={handleChange}
                    type="text"
                    placeholder="example@gmail.com"
                  />
                </div>
                <div className="df fdc mb-24">
                  <label
                    htmlFor=""
                    className="font-s-normal font-w-normal font-14 line-h-100_ c-dark-gray mb-12"
                  >
                    Password
                  </label>
                  <TextField
                    onChange={handleChange}
                    value={state.password}
                    name="password"
                    type="password"
                    placeholder="● ● ● ● ● "
                  />
                </div>

                <div className="mb-32 df aic jcsb">
                  <label
                    htmlFor=""
                    className="df aic font-s-normal font-w-normal font-14 line-height-100_  c-dark-gray markpro"
                  >
                    <input
                      onChange={({ target: { name } }) =>
                        handleChange({
                          target: { name, value: !state.isRemember },
                        })
                      }
                      checked={state.isRemember}
                      type="checkbox"
                      name="isRemember"
                    />
                    Remember me
                  </label>

                  <Link
                    to={mainRoutes.resetPassword}
                    className="font-s-normal font-w-500 font-14 line-h-100_  c-dark-gray markpro"
                  >
                    Forgot password
                  </Link>
                </div>
                <div>
                  <ButtonLoader
                    isLoading={isSignInLoading}
                    onClick={handleClick}
                    disabled={isDisabled}
                  >
                    Sign in
                  </ButtonLoader>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    accountType: state.accountType,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAccount: (type) =>
      dispatch({ type: CHANGE__ACCOUNT__TYPE, payload: type }),
    handleIsAuth: (auth) => dispatch({ type: IS__AUTH, payload: auth }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
