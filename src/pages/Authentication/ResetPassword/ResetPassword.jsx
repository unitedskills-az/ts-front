import React, { Component } from 'react'
import './ResetPassword.scss'
import { Link } from "react-router-dom";

class Login extends Component {
    render() {
        return (
            < div className="login" >
                <div className="container">
                    <div className="login-content">
                        <Link to="/" className="logo font-w-bold font-s-normal font-24 lineh-125_ c-dark-gray">
                            Turkey <br />
                            Suppliers
                        </Link>
                        <div className="auth-container b-white br-12">
                            <div className="auth-container-inner">
                                <form action="">
                                    <div className="df aic jcsb mb-48 resp-mb-32">
                                        <span className="font-s-normal font-w-bold font-20 line-h-100_ c-dark-gray montserrat title">Reset password</span>

                                        <span className="df aic font-w-normal font-14 font-s-normal c-gray auth-a markpro">Have an account?  <Link className="c-red ml-6">Sign in</Link></span>
                                    </div>
                                    <div className="df fdc mb-24">
                                        <label htmlFor="" className="font-s-normal font-w-normal font-14 line-h-100_ c-dark-gray mb-12">
                                            Mail Address
                                        </label>
                                        <input type="text" className="input-class" placeholder="example@gmail.com" />
                                    </div>
                                    <div className="infobar  mb-32 p-24">

                                        <span className="markpro font-s-normal font-w-normal font-14 line-h-150_ c-light-black br-2">
                                            An email with password reset instructions has been sent to your email address, if it exists on our system.
                                        </span>
                                    </div>
                                    <div>
                                        <button type="submit" className="b-yellow br-2 font-s-normal font-w-500 font-16 line-h-100_ c-light-black h-48 bnone w-100_  markpro">Sign in</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}

export default Login
