import { Link } from "react-router-dom";
import { useState } from "react";
import { useParams } from "react-router-dom";

import { ButtonLoader, TextField } from "../../../components";

import { mainRoutes } from "../../../routes";
import { useAuth } from "../common/hooks";

import "./ResetPassword.scss";

const ConfirmResetCode = () => {
  const { token } = useParams();

  const [state, setState] = useState({ password: "", passwordCopy: "" });

  const { handleResetPassword, isResetPasswordLoading } = useAuth();

  const handleChange = ({ target }) => {
    const { name, value } = target;
    setState((prev) => ({ ...prev, [name]: value }));
  };

  const isDisabled =
    !state.password.trim() ||
    !state.passwordCopy.trim() ||
    state.password.trim() !== state.passwordCopy.trim();

  return (
    <div className="login">
      <div className="container">
        <div className="login-content">
          <Link
            to={mainRoutes.home}
            className="logo font-w-bold font-s-normal font-24 lineh-125_ c-dark-gray"
          >
            Turkey <br />
            Suppliers
          </Link>
          <div className="auth-container b-white br-12">
            <div className="auth-container-inner">
              <form action="">
                <div className="df aic jcsb mb-48">
                  <span className="font-s-normal font-w-bold font-20 line-h-100_ c-dark-gray montserrat">
                    Set new password
                  </span>
                </div>
                <div className="df fdc mb-24">
                  <label
                    htmlFor=""
                    className="font-s-normal font-w-normal font-14 line-h-100_ c-dark-gray mb-12"
                  >
                    Set new password
                  </label>
                  <TextField
                    name="password"
                    type="password"
                    onChange={handleChange}
                    value={state.password}
                  />
                </div>
                <div className="df fdc mb-24">
                  <label
                    htmlFor=""
                    className="font-s-normal font-w-normal font-14 line-h-100_ c-dark-gray mb-12"
                  >
                    Confirm password
                  </label>
                  <TextField
                    name="passwordCopy"
                    type="password"
                    onChange={handleChange}
                    value={state.passwordCopy}
                  />
                </div>

                <div>
                  <ButtonLoader
                    onClick={() =>
                      handleResetPassword({ token, password: state.password })
                    }
                    isLoading={isResetPasswordLoading}
                    disabled={isDisabled || isResetPasswordLoading}
                  >
                    Complete
                  </ButtonLoader>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ConfirmResetCode;
