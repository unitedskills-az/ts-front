import { Switch, Route } from "react-router-dom";

import routes from "./routes";

import ConfirmResetEmail from "./ConfirmResetEmail";
import ConfirmResetCode from "./ConfirmResetCode";

const ResetPassword = () => {
  return (
    <Switch>
      <Route exact path={routes.resetPassword} component={ConfirmResetEmail} />
      <Route path={routes.confirmResetCode} component={ConfirmResetCode} />
    </Switch>
  );
};

export default ResetPassword;
