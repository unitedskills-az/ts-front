import { useState } from "react";
import { Link } from "react-router-dom";

import { ButtonLoader, TextField } from "../../../components";

import { mainRoutes } from "../../../routes";
import { useAuth } from "../common/hooks";

import "./ResetPassword.scss";

const ConfirmResetEmail = () => {
  const [mailAddress, setMailAddress] = useState("");
  const [isMessageShow, setIsMessageShow] = useState(false);

  const { handleForgotPassword, isForgotPasswordLoading } = useAuth();

  const handleSend = () => {
    handleForgotPassword(mailAddress, {
      onSuccess: ({ data }) => {
        !!data && setIsMessageShow(true);
      },
    });
  };

  return (
    <div className="login">
      <div className="container">
        <div className="login-content">
          <Link
            to={mainRoutes.home}
            className="logo font-w-bold font-s-normal font-24 lineh-125_ c-dark-gray"
          >
            Turkey <br />
            Suppliers
          </Link>
          <div className="auth-container b-white br-12">
            <div className="auth-container-inner">
              <form action="">
                <div className="df aic jcsb mb-48">
                  <span className="font-s-normal font-w-bold font-20 line-h-100_ c-dark-gray montserrat">
                    Reset password
                  </span>

                  <span className="df aic font-w-normal font-14 font-s-normal c-gray auth-a markpro">
                    Have an account?
                    <Link to={mainRoutes.login} className="c-red ml-6">
                      Sign in
                    </Link>
                  </span>
                </div>
                <div className="df fdc mb-24">
                  <label
                    htmlFor=""
                    className="font-s-normal font-w-normal font-14 line-h-100_ c-dark-gray mb-12"
                  >
                    Mail Address
                  </label>
                  <TextField
                    disabled={isMessageShow}
                    onChange={({ target: { value } }) =>
                      setMailAddress(value.trim())
                    }
                    value={mailAddress}
                    type="text"
                    placeholder="example@gmail.com"
                  />
                </div>
                {isMessageShow && (
                  <div className="infobar  mb-32 p-24">
                    <span className="markpro font-s-normal font-w-normal font-14 line-h-150_ c-light-black br-2">
                      An email with password reset instructions has been sent to
                      your email address, if it exists on our system.
                    </span>
                  </div>
                )}
                <div>
                  <ButtonLoader
                    onClick={handleSend}
                    isLoading={isForgotPasswordLoading}
                    disabled={!mailAddress || isForgotPasswordLoading}
                  >
                    {isMessageShow ? "Resend" : "Send"}
                  </ButtonLoader>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ConfirmResetEmail;
