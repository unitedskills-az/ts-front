const routes = {
  resetPassword: "/reset-password",
  confirmResetCode: "/reset-password/code/:token",
};

export default routes;
