import { useMutation } from "react-query";
import Storage from "../../../../services/storage";
import {
  postRegister,
  postLogin,
  logout,
  resetPassword,
  forgotPassword,
} from "../redux/api";
import { mainRoutes } from "../../../../routes";
import { useRedirect } from "../../../../hooks";
import { authKeys } from "../constants";

const useAuth = (handleAccount, handleIsAuth) => {
  const { EXPIRES_AT } = authKeys;
  const { redirect } = useRedirect();

  const handleLoginSuccess = (data) => {
    handleIsAuth(true);
    Storage.setType("buyers");
    handleAccount("buyers");
    Storage.setToken(data?.access_token);
    Storage.set(EXPIRES_AT, data?.expires_at);
    redirect(mainRoutes.buyerDashboard);
  };

  const { mutate: handleSignUp, isLoading: isSignUpLoading } = useMutation(
    (payload) => postRegister(payload),
    {
      onSuccess: () => {
        redirect(mainRoutes.login);
        Storage.setType("buyers");
        handleAccount("buyers");
        handleIsAuth(true);
      },
    }
  );

  const { mutate: handleSignIn, isLoading: isSignInLoading } = useMutation(
    (payload) => postLogin(payload),
    {
      onSuccess: ({ data }) => {
        handleLoginSuccess(data);
      },
    }
  );

  const { mutate: handleLogout, isLoading: isLogoutLoading } = useMutation(
    logout,
    {
      onSuccess: () => {
        Storage.clear();
        redirect(mainRoutes.home);
      },
    }
  );

  const { mutate: handleForgotPassword, isLoading: isForgotPasswordLoading } =
    useMutation((email) => forgotPassword(email));

  const { mutate: handleResetPassword, isLoading: isResetPasswordLoading } =
    useMutation((payload) => resetPassword(payload), {
      onSuccess: () => redirect(mainRoutes.login),
    });

  return {
    handleSignUp,
    isSignUpLoading,
    handleSignIn,
    isSignInLoading,
    handleLogout,
    isLogoutLoading,
    handleForgotPassword,
    isForgotPasswordLoading,
    handleResetPassword,
    isResetPasswordLoading,
    isAuth: !!Storage.getToken(),
  };
};

export default useAuth;
