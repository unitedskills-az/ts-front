import {CHANGE__ACCOUNT__TYPE, IS__AUTH} from "./actionTypes";

const handleAccount = (payload) => {
  return {
    type: CHANGE__ACCOUNT__TYPE,
    payload: payload,
  };
};


const handleIsAuth = (payload) => {
  return {
    type: IS__AUTH,
    payload: payload,
  };
};
