import { sRequestFunc } from "../../../../api";
import { URLS } from "../../../../api/routing";

export const postLogin = ({ email, password, isRemember }) =>
  sRequestFunc
    .post(URLS.LOGIN(email, password, isRemember))
    .then((res) => res.data);

export const postRegister = ({ email, password }) =>
  sRequestFunc.post(URLS.REGISTER(email, password)).then((res) => res.data);

export const logout = () =>
  sRequestFunc.get(URLS.LOG_OUT()).then((res) => res.data);

export const forgotPassword = (email) =>
  sRequestFunc.post(URLS.FORGOT_PASSWORD(email)).then((res) => res.data);

export const resetPassword = ({ token, password }) =>
  sRequestFunc
    .post(URLS.RESET_PASSWORD(token, password))
    .then((res) => res.data);
