import Storage from "../../../../services/storage";
import {CHANGE__ACCOUNT__TYPE, IS__AUTH} from "./actionTypes";

function accountType(
  state = Storage.getItem("accountType") === null
    ? null
    : Storage.getItem("accountType"),
  action
) {
  switch (action.type) {
    case CHANGE__ACCOUNT__TYPE:
      return action.payload;
    default:
      return state;
  }
}

function isAuth(
  state = Storage.getItem("accessToken") === null
    ? false
    : true,
  action
) {
  switch (action.type) {
    case IS__AUTH:
      return action.payload;
    default:
      return state;
  }
}

export {isAuth, accountType}
