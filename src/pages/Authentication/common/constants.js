export const authKeys = {
  EXPIRES_AT: "expires_at",
  TOKEN: "token",
  CREDENTIALS: "credentials",
};
