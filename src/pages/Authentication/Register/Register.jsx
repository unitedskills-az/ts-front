import { useState } from "react";
import { Link } from "react-router-dom";

import { ButtonLoader, TextField } from "../../../components";

import { useAuth } from "../common/hooks";

import "./Register.scss";

import Google from "../../../assets/google.svg";
import Facebook from "../../../assets/facebook.svg";

const Register = () => {
  const { handleSignUp, isSignUpLoading } = useAuth();

  const [state, setState] = useState({
    email: "",
    password: "",
    passwordCopy: "",
  });
  const [isAgree, setIsAgree] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({ ...prev, [name]: value }));
  };

  const handleClick = () => {
    handleSignUp({ email: state.email, password: state.password });
  };

  const isDisabled =
    isSignUpLoading ||
    !isAgree ||
    !state.email.trim() ||
    !state.password.trim() ||
    state.password.trim() !== state.passwordCopy.trim();

  return (
    <div className="login">
      <div className="container">
        <div className="login-content">
          <Link
            to="/"
            className="logo font-w-bold font-s-normal font-24 lineh-125_ c-dark-gray"
          >
            Turkey <br />
            Suppliers
          </Link>
          <div className="auth-container b-white br-12">
            <div className="auth-container-inner">
              <form action="">
                <div className="df aic jcsb mb-48 resp-mb-32">
                  <span className="font-s-normal font-w-bold font-20 line-h-100_ c-dark-gray montserrat">
                    Create new account
                  </span>

                  <span className="df aic font-w-normal font-14 font-s-normal c-gray auth-a markpro">
                    Have an account?{" "}
                    <Link to="/login" className="c-red ml-6">
                      Sign in
                    </Link>
                  </span>
                </div>
                <div className="df aic jcsb mb-32">
                  <a
                    href="https://www.facebook.com/"
                    className="login-google font-s-normal font-w-normal font-14 line-h-100_ c-black df aic jcc h-42 px-14"
                  >
                    <img src={Google} alt="google" className="mr-8 w-20 h-20" />
                    Google
                  </a>
                  <a
                    href="https://www.facebook.com/"
                    className="login-facebook font-s-normal font-w-normal font-14 line-100_ c-white df aic jcc h-40 px-10"
                  >
                    <img
                      src={Facebook}
                      alt="facebook"
                      className="mr-8 w-10 h-18"
                    />
                    Facebook
                  </a>
                </div>
                <div className="df fdc mb-24">
                  <label
                    htmlFor=""
                    className="font-s-normal font-w-normal font-14 line-h-100_ c-dark-gray mb-12"
                  >
                    Mail Address
                  </label>
                  <TextField
                    name="email"
                    value={state.email}
                    onChange={handleChange}
                    type="text"
                    placeholder="example@gmail.com"

                  />
                </div>
                <div className="df fdc mb-24">
                  <label
                    htmlFor=""
                    className="font-s-normal font-w-normal font-14 line-h-100_ c-dark-gray mb-12"
                  >
                    Set new password
                  </label>
                  <TextField
                    onChange={handleChange}
                    value={state.password}
                    name="password"
                    type="password"
                    placeholder="● ● ● ● ● "
                  />
                </div>
                <div className="df fdc mb-24">
                  <label
                    htmlFor=""
                    className="font-s-normal font-w-normal font-14 line-h-100_ c-dark-gray mb-12"
                  >
                    Confirm password
                  </label>
                  <TextField
                    onChange={handleChange}
                    value={state.passwordCopy}
                    name="passwordCopy"
                    type="password"
                    placeholder="● ● ● ● ● "
                  />
                </div>
                <div className="mb-32 register-iagree">
                  <label
                    htmlFor=""
                    className="df aic font-s-normal font-w-normal font-14 line-height-100_ c-gray"
                  >
                    <input
                      checked={isAgree}
                      onChange={() => setIsAgree(!isAgree)}
                      type="checkbox"
                    />
                    I agree to{" "}
                    <Link to="#" className="c-dark-gray font-w-600">
                      Turkey Suppliers Membership Agreement
                    </Link>
                  </label>
                </div>
                <div>
                  <ButtonLoader
                    isLoading={isSignUpLoading}
                    onClick={handleClick}
                    disabled={isDisabled}
                    className="b-yellow br-2 font-s-normal font-w-500 font-16 line-h-100_ c-light-black h-48 bnone w-100_  markpro"
                  >
                    Sign up
                  </ButtonLoader>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
