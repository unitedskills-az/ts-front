/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";

import Storage from "../../../services/storage";
import { useRedirect } from "../../../hooks";
import { authKeys } from "../common/constants";
import { mainRoutes } from "../../../routes";

const LoginWithSocials = () => {
  const { EXPIRES_AT, TOKEN } = authKeys;
  const { redirect } = useRedirect();
  const urlParams = new URLSearchParams(window.location.search);
  const token = urlParams.get(TOKEN);
  const expiresAt = urlParams.get(EXPIRES_AT);

  const setDataAndRedirect = () => {
    Storage.setToken(token);
    Storage.set(EXPIRES_AT, expiresAt);
    redirect(mainRoutes.home);
  };
  
  useEffect(() => {
    if (!!token && !!expiresAt) setDataAndRedirect();
  }, [token, expiresAt]);

  return <div>Loading...</div>;
};

export default LoginWithSocials;
