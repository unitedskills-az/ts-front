import React from "react";
import "./ProductDetail.scss";
import { withRouter } from "react-router-dom";
import ProductCarousel from "../../components/productDetailCarousel/Carousel";
import BreadCrumb from "../../components/breadCrumb/breadCrumb";
import ProductDetails from "../../components/productDetail";
import Recommend from "../../components/Homepage/homeRecommend";
import useGetProductDetail from "./common/hooks/useGetProductDetail";
import renderHTML from "react-render-html";

const ProductDetail = ({ match }) => {
  const { slug } = match.params;

  const { productDetailData, isLoadingProductDetail } =
    useGetProductDetail(slug);

  return (
    <div className="priduct-page">
      <div className="container">
        <BreadCrumb />
        <div className="row">
          <div className="xl-6 lg-6 md-6 sm-12">
            {isLoadingProductDetail === false &&
              productDetailData !== undefined && (
                <ProductCarousel data={productDetailData.data} />
              )}
          </div>
          <div className="xl-6 lg-6 md-6 sm-12">
            {isLoadingProductDetail === false &&
              productDetailData !== undefined && (
                <ProductDetails data={productDetailData.data} />
              )}
          </div>
          {isLoadingProductDetail === false && productDetailData !== undefined && (
            <div className="xl-12 lg-12 md-12 sm-12">
              <div className="detail-content-parent">
                <h5 className="title">Features</h5>
                <div className="row">
                  <div className="xl-6 lg-6 md-6 sm-12">
                    <div className="detail-content">
                      <div className="detail-content-child">
                        <span>Category</span>
                        <span>{productDetailData.data.category}</span>
                      </div>

                      <div className="detail-content-child">
                        <span>Origin Place</span>
                        <span>{productDetailData.data.origin_place}</span>
                      </div>

                      <div className="detail-content-child">
                        <span>Model Number</span>
                        <span>{productDetailData.data.model}</span>
                      </div>

                      <div className="detail-content-child">
                        <span>Business Type</span>
                        <span>
                          {productDetailData.data.business_type.length !== 0 &&
                            productDetailData.data.business_type[0]}
                        </span>
                      </div>
                      <div className="detail-content-child">
                        <span>Accepted Payment Currency</span>

                        {productDetailData.data.supplier.payment_currencies.map(
                          (item) => (
                            <span key={item.id}>{item.name}</span>
                          )
                        )}
                      </div>

                      <div className="detail-content-child">
                        <span>Accepted delivery terms</span>
                        {productDetailData.data.supplier.delivery_terms.map(
                          (item) => (
                            <span key={item.id}>{item.name}</span>
                          )
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="xl-6 lg-6 md-6 sm-12">
                    <div className="detail-content">
                      <div className="detail-content-child">
                        <span>Factory location:</span>
                        <span>
                          {productDetailData.data.supplier.factory_location_id}
                        </span>
                      </div>
                      <div className="detail-content-child">
                        <span>Factory area size</span>
                        <span>
                          {productDetailData.data.supplier.factory_area_size}
                        </span>
                      </div>
                      <div className="detail-content-child">
                        <span>Total annual output</span>
                        <span>
                          {
                            productDetailData.data.supplier
                              .total_annual_output_id
                          }
                        </span>
                      </div>
                      <div className="detail-content-child">
                        <span>Number of production lines</span>
                        <span>
                          {
                            productDetailData.data.supplier
                              .number_of_production_line
                          }
                        </span>
                      </div>
                      <div className="detail-content-child">
                        <span>Export percentage</span>
                        <span>
                          {productDetailData.data.supplier.export_percentage}
                        </span>
                      </div>
                      <div className="detail-content-child">
                        <span>Average lead time</span>
                        <span>
                          {productDetailData.data.supplier.average_lead_time}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
          <div className="xl-12 lg-12 md-12 sm-12">
            <div className="detail-content-parent mb-72 resp-mb-48">
              <h5 className="title">Description</h5>
              {isLoadingProductDetail === false &&
                productDetailData !== undefined &&
                productDetailData.data.description !== null &&
                renderHTML(productDetailData.data.description)}
            </div>
          </div>
          <div className="xl-12 lg-12 md-12 sm-12">
            <Recommend />
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(ProductDetail);
