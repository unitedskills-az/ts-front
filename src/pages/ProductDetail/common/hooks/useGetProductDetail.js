import { useQuery } from "react-query";
import { getProductDetail } from "../redux/api";

const useGetProductDetail = (slug) => {
  const { data: productDetailData, isLoading: isLoadingProductDetail } =
    useQuery(["ProductDetail", slug], getProductDetail);

  return { productDetailData, isLoadingProductDetail };
};

export default useGetProductDetail;
