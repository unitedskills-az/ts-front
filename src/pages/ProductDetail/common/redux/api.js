import { sRequestFunc } from "../../../../api";
import { URLS } from "../../../../api/routing";

export const getProductDetail = (slug) =>
  sRequestFunc
    .get(URLS.GET__PRODUCTS__DETAIL(slug.queryKey[1]))
    .then((res) => res.data);
