import React, { useEffect } from "react";
import "./AboutCompany.scss";
import { Link, withRouter } from "react-router-dom";
import SideBar from "../../../components/sidebar/companyMiniSideBar/sidebar";
import ContactOut from "../../../components/contactSupplier/contactSupplierOut";
import ContactIn from "../../../components/contactSupplier/contactSupplierIn";
import AboutCompanyContent from "../../../components/aboutCompany/aboutCompany/aboutCompany";
import { ReactComponent as Edit } from "../../../assets/edit.svg";
import SlideImage from "../../../components/aboutCompany/aboutCompanySlide/slider";
import { ReactComponent as FaceBook } from "../../../assets/face.svg";
import { ReactComponent as InstaGram } from "../../../assets/instagram.svg";
import { ReactComponent as World } from "../../../assets/wrld.svg";
import { ReactComponent as Linkedin } from "../../../assets/linkedin.svg";
import { ReactComponent as Youtube } from "../../../assets/youtb.svg";

import { useGetCompany } from "../common/hooks";
import { connect } from "react-redux";

const AboutCompany = ({ match, isAuth }) => {
  const { slug } = match.params;

  const { dataSuppliers, isLoadingSuppliers, errorSuppliers } = useGetCompany(
    "/" + slug
  );

  useEffect(() => {
    if (errorSuppliers) {
      alert(errorSuppliers.response.data.error);
    }
  });

  return (
    <div className="about-company">
      <div className="container">
        <div className="row">
          <div className="xl-3 lg-3 md-3 sm-12">
            <SideBar companySlug={slug} />
            {isAuth ? <ContactIn /> : <ContactOut />}
          </div>

          {isLoadingSuppliers === false && dataSuppliers !== undefined && (
            <div className="xl-9 lg-9 md-9 sm-12">
              <div className="about-company-content">
                <div className="about-company-header">
                  {dataSuppliers.data.cover !== null &&
                    dataSuppliers.data.cover !== undefined && (
                      <img src={dataSuppliers.data.cover.path} alt="" />
                    )}
                  <div className="about-compant-header-content">
                    <div className="img-container">
                      {dataSuppliers.data.logo !== null && (
                        <img src={dataSuppliers.data.logo.path} alt="" />
                      )}
                    </div>
                    <div className="about-company-header-child">
                      <div className="title-parent">
                        <h5 className="title">
                          {dataSuppliers.data.company_name}
                        </h5>
                        <a
                          href={dataSuppliers.data.video_introduction}
                          target="_blank"
                          rel="noreferrer"
                        >
                          <Youtube />
                          {dataSuppliers.data.video_introduction}
                        </a>
                      </div>
                      <Link to="" className="edit-button">
                        <Edit />
                        Edit Company Details
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="about-company-header-middle">
                  <h6 className="title montserrat font-s-normal font-w-500 font-16 line-h-100_ c-dark-gray mb-12">
                    About Company:
                  </h6>
                  <p className="markpro font-s-normal font-w-normal font-16 line-h-150_ c-46">
                    {dataSuppliers.data.trade_show_company_advantages}
                  </p>

                  <SlideImage data={dataSuppliers.data.gallery} />
                </div>
              </div>
              <div className="about-company-details">
                <div className="detail">
                  <h5 className="title">Company Operational Details</h5>
                  <div className="detail-content">
                    <div className="detail-content-child">
                      <span>Business Type</span>
                      <span>
                        {dataSuppliers.data.businessTypes.map((item) => (
                          <div key={item.id}>{item.name}</div>
                        ))}
                      </span>
                    </div>
                    <div className="detail-content-child">
                      <span>Location of registration</span>
                      <span>
                        {dataSuppliers.data.location_of_registration_id}
                      </span>
                    </div>
                    <div className="detail-content-child">
                      <span>Country / Region</span>
                      <span>{dataSuppliers.data.country_id}</span>
                    </div>
                    <div className="detail-content-child">
                      <span>City / Town</span>
                      <span>{dataSuppliers.data.city_id}</span>
                    </div>
                    <div className="detail-content-child">
                      <span>Street / Road</span>
                      <span>{dataSuppliers.data.street}</span>
                    </div>
                    <div className="detail-content-child">
                      <span>ZIP code</span>
                      <span>{dataSuppliers.data.zip_code}</span>
                    </div>
                    <div className="detail-content-child">
                      <span>Year of established</span>
                      <span>{dataSuppliers.data.year_of_established}</span>
                    </div>
                    <div className="detail-content-child">
                      <span>Number of Employees</span>
                      <span>{dataSuppliers.data.employees_in_trade_id}</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="about-company-details">
                <div className="detail">
                  <h5 className="title">Products</h5>
                  <div className="detail-content">
                    <div className="detail-content-child">
                      <span>Main Categories</span>
                      <span>{dataSuppliers.data.main_catageories}</span>
                    </div>
                    <div className="detail-content-child">
                      <span>Main Products</span>
                      <span>{dataSuppliers.data.main_products}</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="about-company-details">
                <div className="about-company-details">
                  <div className="detail">
                    <h5 className="title">Export Capability</h5>
                    <div className="detail-content">
                      <div className="detail-content-child">
                        <span>Total annual revenue</span>
                        <span>
                          {dataSuppliers.data.total_annual_revenue_id}
                        </span>
                      </div>
                      <div className="detail-content-child">
                        <span>Export Percentage (%)</span>
                        <span>{dataSuppliers.data.export_percentage}</span>
                      </div>
                      <div className="detail-content-child">
                        <span>Started exporting year</span>
                        <span>{dataSuppliers.data.start_exporting_year}</span>
                      </div>
                      <div className="detail-content-child">
                        <span>Number of employees in Trade Department</span>
                        <span>{dataSuppliers.data.employees_in_trade_id}</span>
                      </div>
                      <div className="detail-content-child">
                        <span>Number of Quality Control Staff</span>
                        <span>
                          {dataSuppliers.data.quality_control_staff_id}
                        </span>
                      </div>
                      <div className="detail-content-child">
                        <span>Average lead time (days)</span>
                        <span>{dataSuppliers.data.average_lead_time}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="about-company-details">
                <div className="detail">
                  <h5 className="title">Social Media</h5>
                  <div className="social-icons-container">
                    <Link to="/">
                      <FaceBook />
                      <span>{dataSuppliers.data.facebook}</span>
                    </Link>
                    <Link to="/">
                      <InstaGram />
                      <span>{dataSuppliers.data.instagram}</span>
                    </Link>
                    <Link to="/">
                      <Linkedin />
                      <span>{dataSuppliers.data.linkedin}</span>
                    </Link>
                    <Link to="/">
                      <World />
                      <span>{dataSuppliers.data.website}</span>
                    </Link>
                  </div>
                  <h6 className="montserrat font-s-normal font-w-500 font-16 line-h-100_ c-dark-gray mb-12">
                    About Company:
                  </h6>
                  <p className="markpro font-s-normal font-w-normal font-16 line-h-150_ c-46">
                    {dataSuppliers.data.advantage}
                  </p>
                </div>
              </div>
              <div className="about-company-details">
                <AboutCompanyContent
                  title={"Oversee offices"}
                  data={dataSuppliers.data.overseeOffices}
                />
              </div>
              <div className="about-company-details">
                <div className="about-company-details">
                  <div className="detail">
                    <h5 className="title">Details</h5>
                    <div className="detail-content">
                      <div className="detail-content-child">
                        <span>Main Market and Distributions</span>
                        <span>
                          {dataSuppliers.data.distributions.map((item) => (
                            <div key={item.id}>{item.name}</div>
                          ))}
                        </span>
                      </div>
                      <div className="detail-content-child">
                        <span>Accepted Delivery Terms</span>
                        <span>
                          {dataSuppliers.data.delivery_terms.map((item) => (
                            <div key={item.id}>{item.name}</div>
                          ))}
                        </span>
                      </div>
                      <div className="detail-content-child">
                        <span>Language Spokens</span>
                        <span>
                          {dataSuppliers.data.languages_spoken.map((item) => (
                            <div key={item.id}>{item.name}</div>
                          ))}
                        </span>
                      </div>
                      <div className="detail-content-child">
                        <span>Accepted Payment Currency</span>
                        <span>
                          {dataSuppliers.data.payment_currencies.map((item) => (
                            <div key={item.id}>{item.name}</div>
                          ))}
                        </span>
                      </div>
                      <div className="detail-content-child">
                        <span> Accepted Payment Type</span>
                        <span>
                          {dataSuppliers.data.payment_types.map((item) => (
                            <div key={item.id}>{item.name}</div>
                          ))}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {dataSuppliers.data.is_trade_show !== false && (
                <div className="about-company-details">
                  <AboutCompanyContent
                    title={"Trade Shows"}
                    data={dataSuppliers.data.tradeShows}
                    text={dataSuppliers.data.trade_show_company_advantages}
                  />
                </div>
              )}
              {dataSuppliers.data.is_show_production_process !== false && (
                <div className="about-company-details">
                  <AboutCompanyContent
                    title={"Production Proccess"}
                    data={dataSuppliers.data.productionProcessGallery}
                    text={
                      dataSuppliers.data
                        .show_production_process_company_advantages
                    }
                  />
                </div>
              )}
              {dataSuppliers.data.is_show_production_equipment !== false && (
                <div className="about-company-details">
                  <AboutCompanyContent
                    title={"Production Equipment"}
                    data={dataSuppliers.data.productionEquipmentGallery}
                    text={
                      dataSuppliers.data
                        .show_production_equipment_company_advantages
                    }
                  />
                  <div className="detail-content">
                    <div className="detail-content-child">
                      <span>Factory Location</span>
                      <span>{dataSuppliers.data.factory_location_id}</span>
                    </div>
                    <div className="detail-content-child">
                      <span>Factory Area Size (square metr)</span>
                      <span>{dataSuppliers.data.factory_area_size}</span>
                    </div>
                    <div className="detail-content-child">
                      <span>Total Annual Output</span>
                      <span>{dataSuppliers.data.total_annual_output_id}</span>
                    </div>
                    <div className="detail-content-child">
                      <span>Number of Production Lines</span>
                      <span>
                        {dataSuppliers.data.number_of_production_line}
                      </span>
                    </div>
                  </div>
                </div>
              )}
              {dataSuppliers.data.is_quality_control_process !== false && (
                <div className="about-company-details">
                  <AboutCompanyContent
                    title={"Control Proccess"}
                    data={dataSuppliers.data.qualityControlGallery}
                    text={
                      dataSuppliers.data
                        .quality_control_process_company_advantages
                    }
                  />
                </div>
              )}
              {dataSuppliers.data.is_quality_control_process !== false && (
                <div className="about-company-details">
                  <AboutCompanyContent
                    title={"Testing Equipment"}
                    data={dataSuppliers.data.testingEquipmentGallery}
                    text={
                      dataSuppliers.data.testing_equipment_company_advantages
                    }
                  />
                  <div className="detail-content">
                    <div className="detail-content-child">
                      <span>Number of QC Staff</span>
                      <span>{dataSuppliers.data.qc_staff_id}</span>
                    </div>
                  </div>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
export default connect((state) => ({
  isAuth: state.isAuth,
}))(withRouter(AboutCompany));
