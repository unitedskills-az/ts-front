import { sRequestFunc } from "../../../../api";
import { URLS } from "../../../../api/routing";

export const getSuppliers = (slug) =>
  sRequestFunc
    .get(URLS.GET__SUPPLIERS(slug.queryKey[1]))
    .then((res) => res.data);
