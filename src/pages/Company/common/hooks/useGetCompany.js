import { useQuery } from "react-query";
import { getSuppliers } from "../redux/api";

const useGetCompany = (slug) => {
  const {
    data: dataSuppliers,
    isLoading: isLoadingSuppliers,
    error: errorSuppliers,
  } = useQuery(["SuppliersDomain", slug], getSuppliers);

  return { dataSuppliers, isLoadingSuppliers, errorSuppliers };
};

export default useGetCompany;
