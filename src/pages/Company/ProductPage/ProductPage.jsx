import React from "react";

import "./ProductPage.scss";
import Filter from "../../../components/filter/productPageFilter";
import ProductCard from "../../../components/productCard";
import SideBar from "../../../components/sidebar/companyMiniSideBar/sidebar";
import { useGetProducts } from "../../Account/Suppliers/common/hooks";

const ProductPage = () => {
  const { dataProducts, isLoadingProducts } = useGetProducts();

  return (
    <div className="productPage">
      <div className="container">
        <div className="row mt-24">
          <div className="xl-3 lg-3 md-3 sm-12">
            <SideBar />
            <div className="filter">
              <Filter />
            </div>
          </div>
          <div className="xl-9 lg-9 md-9 sm-12">
            <div className="row">
              {isLoadingProducts === false &&
                dataProducts !== undefined &&
                dataProducts.data.map((item) => (
                  <div className="xl-4 lg-4 md-4 sm-12" key={item.id}>
                    <ProductCard data={item} />
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ProductPage;
