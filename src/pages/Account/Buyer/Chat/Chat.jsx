import React, { useState } from 'react';
import './Chat.scss';
import Plus from '../../../../assets/plus.png';
import Request from '../../../../assets/request.png';
import ChatAboutImg from '../../../../assets/aboutlogo.png'
import { ReactComponent as Arrow } from '../../../../assets/arrowinner.svg';
import ChatProductCard from '../../../../components/chatProductCard';
import { ReactComponent as Loc } from '../../../../assets/loc.svg';
import { ReactComponent as Rating } from '../../../../assets/rating.svg';
import ChatCard from '../../../../components/chatCard';
import { ReactComponent as EmptyCard } from '../../../../assets/card.svg'
import { Link } from 'react-router-dom';
const Chat = () => {
    const [state, setState] = useState("");
    const toggleAccordion = () => {
        setState(state === "" ? "active" : "");
    }
    const empty = false;
    return (
        <div className="chat background-light-blue pt-24 pb-72">
            <div className="container">
                <div className="row">
                    <div className="xl-8 lg-8 md-8 sm-12">
                        <div className="message-content-right pb-0">
                            <div className="user-header">
                                <Link to="/buyer/chat/orders" className="subtitle font-s-normal montserrat font-w-500 font-14 line-h-100_ c-light-black df aic jcfs">
                                    Order Historys
                                    <Arrow />
                                </Link>
                                <div className="df aic jcfe">
                                    {(empty) ?
                                        ""
                                        :
                                        < p className="mb-0 markpro font-s-normal font-w-normal font-14 line-h-100_ c-light-gray">Last update 01.02.2021 11:22 PM</p>

                                    }
                                    <Link to="/products" className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro ml-48">
                                        <img src={Plus} alt="" />
                                            Add product
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {(empty) ?
                            <div className="message-content-left df aic h-590">
                                <div className="message-content-left-center">
                                    <EmptyCard />
                                    <p>There is nobody here. Add new product and <br /> start to shopping!</p>
                                    <Link to="/products" className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro">
                                        <img src={Plus} alt="" />
                                                  Add product
                                    </Link>
                                </div>
                            </div>
                            :
                            <div className="message-content-left-parent">
                                <div className="message-content-left">
                                    <ChatProductCard />
                                    <ChatProductCard />
                                    <ChatProductCard />
                                    <ChatProductCard />
                                    <ChatProductCard />
                                    <ChatProductCard />
                                    <ChatProductCard />
                                </div>
                                <div className="message-content-bottom">
                                    <div className="message-content-bottom-child">
                                        <div className="df aic jcsb w-55_">
                                            <div>
                                                <div className="title">
                                                    Summary
                                            </div>
                                                <p><span>$</span> 987654.35</p>
                                            </div>
                                            <div>
                                                <div className="title">
                                                    Discount
                                            </div>
                                                <p><span>$</span> 654</p>
                                            </div>
                                            <div>
                                                <div className="title">
                                                    Summary
                                            </div>
                                                <p><span>$</span> 987000.35</p>
                                            </div>

                                        </div>
                                        <button className={`header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro  ${state}`} onClick={toggleAccordion} >
                                            <img src={Request} alt="" />
                                            <span className="add">
                                                Add new product
                                        </span>
                                            <span className="offer">
                                                Offer sent
                                        </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        }
                    </div>
                    <div className="xl-4 lg-4 md-4 sm-12">
                        <div className="chat-message">
                            <div className="company-about-chat">
                                <div className="img-container">
                                    <img src={ChatAboutImg} alt="Chat About Img" />
                                </div>
                                <div>
                                    <h6 className="title montserrat font-s-normal font-w-bold font-16 line-h-100_ c-dark-gray">
                                        Wmt Cnc Industrial Co., Ltd.
                                    </h6>
                                    <div className="df aic">
                                        <p className="location">
                                            <Loc />
                                            Istanbul, Turkey
                                        </p>
                                        <div className="rating">
                                            <Rating />
                                            <Rating />
                                            <Rating />
                                            <Rating />
                                            <Rating />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ChatCard />
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default Chat
