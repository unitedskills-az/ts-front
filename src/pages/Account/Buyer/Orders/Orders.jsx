import React from 'react';
import SideBar from '../../../../components/sidebar/BuyerSidebar/sidebar';
import SelectCountry from '../../../../components/miniSelect';
import OrderCard from '../../../../components/Order/OrderCard'
function Orders() {
    return (
        <div className="orders pt-24 pb-72 background-light-blue">
            <div className="container">
                <div className="row">
                    <div className="xl-3 lg-3 md-3 sm-12">
                        <SideBar />
                    </div>
                    <div className="xl-9 lg-9 md-9 sm-12">
                        <div className="message-content-right">
                            <div className="user-header">
                                <h5 className="title">
                                    Orders
                                    </h5>

                                <SelectCountry />
                            </div>
                            <OrderCard />
                            <OrderCard />
                            <OrderCard />
                            <OrderCard />
                            <OrderCard />
                            <OrderCard />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Orders
