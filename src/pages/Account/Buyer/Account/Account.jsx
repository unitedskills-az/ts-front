import React, { useState } from "react";

import "./Account.scss";
import SideBar from "../../../../components/sidebar/BuyerSidebar/sidebar";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import ArrowDown from "../../../../assets/arrowdown.svg";
import DatePicker from "../../../../components/DatePicker/Datepicker";
import makeAnimated from "react-select/animated";
import Select from "react-select";
import Upload from "../../../../components/uploadImg/upload";

const options = [
  { value: "a", label: "a" },
  { value: "b", label: "b" },
  { value: "c", label: "c" },
];
const animatedComponents = makeAnimated();

const customStyle = {
  control: (provided, styles) => ({
    ...provided,
    margin: 0,
    height: "4.6rem",
    marginLeft: 0,
    fontSize: "1.6rem",
    backgroundColor: "white",
    outline: "none",
    border: `transparent`,
    boxShadow: "none",
    menuPortal: provided => ({ ...provided, zIndex: 9999 }),
    menu: provided => ({ ...provided, zIndex: 9999 }),
    "&:hover": {
      borderColor: "transparent",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: ".5rem 2.4rem 0",
    height: "4.6rem",
  }),
  placeholder: () => ({
    marginTop: "-5.5rem",
    color: "#666666",
  }),
  multiValue: (base, state) => ({
    ...base,
    background: "#FFD05F",
    borderRadius: "2.5rem",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "1.6rem",
    lineHeight: "100%",
    color: "#2D2D38",
    padding: "0 1.6rem",
    height: "3.2rem",
    display: "flex",
    alignItems: "center",
    opacity: "1",
  }),
};
const customStyle2 = {
  control: (provided, styles) => ({
    ...provided,
    margin: 0,
    height: "4.8rem",
    marginLeft: 0,
    fontSize: "1.6rem",
    backgroundColor: "white",
    outline: "none",
    border: `transparent`,
    boxShadow: "none",
    menuPortal: provided => ({ ...provided, zIndex: 9999 }),
    menu: provided => ({ ...provided, zIndex: 9999 }),
    "&:hover": {
      borderColor: "transparent",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: "0 2.4rem",
  }),

  menu: (base) => ({
    ...base,
    zIndex: 100,
  }),
};

const Account = () => {
  const [selectedTab] = useState(0);

  return (
    <div className="account">
      <div className="container">
        <div className="row">
          <div className="xl-3 lg-3 md-3 sm-12">
            <SideBar />
          </div>
          <div className="xl-9 lg-9 md-9 sm-12">
            <div className="tab-section">
              <Tabs defaultTab={selectedTab.toString()}>
                <TabList>
                  <Tab tabFor="0">Profile details</Tab>
                  <Tab tabFor="1">Company Information</Tab>
                  <Tab tabFor="2">Sourcing information</Tab>
                </TabList>

                <TabPanel tabId="0">
                  <div className="tab-header">
                    <Upload />
                    <div>
                      <h6 className="title">Profile Picture</h6>
                      <p>
                        High resolution recommented (jpg, png, webp) | <br />{" "}
                        Max 1000x1000 and 5MB
                      </p>
                    </div>
                  </div>
                  <form className="tab-content">
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">Name</label>
                        <input type="text" placeholder="John Doe" />
                      </div>
                      <div className="input-child">
                        <label htmlFor="">Phone</label>
                        <div className="phone-country">
                          <img src={ArrowDown} alt="" />
                          <select name="" id="">
                            <option value="">+994</option>
                          </select>
                          <input type="number" placeholder="506707500" />
                        </div>
                      </div>
                    </div>
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">Country / City</label>
                        <input type="text" placeholder="Baku, Azerbaijan" />
                      </div>
                      <div className="input-child">
                        <label htmlFor="">Mail</label>
                        <input type="email" placeholder="example@gmail.com" />
                      </div>
                    </div>
                    <div className="input-child mt-72">
                      <button className="yellow-button profile-save">
                        <svg
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <rect width="24" height="24" rx="6" fill="white" />
                          <path
                            d="M17 9L10.8462 15L8 12"
                            stroke="#2D2D38"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                        </svg>
                        Save changes
                        <span></span>
                      </button>
                    </div>
                  </form>
                </TabPanel>
                <TabPanel tabId="1">
                  <form className="tab-content">
                    <div className="tab-header-second">
                      <h6 className="title">Company informations</h6>
                    </div>
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">
                          <span>*</span> Company name
                        </label>
                        <input type="text" placeholder="John Doe LLC" />
                      </div>
                      <div className="input-child">
                        <label htmlFor="">Official website</label>
                        <input type="text" placeholder="http://" />
                      </div>
                    </div>
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">
                          <span>*</span> Location of registration
                        </label>
                        <input type="text" placeholder="John Doe LLC" />
                      </div>
                      <div className="input-child">
                        <label htmlFor="">
                          <span> *</span> Year of Established
                        </label>
                        <DatePicker />
                      </div>
                    </div>
                    <div className="tab-header-second mt-72">
                      <h6 className="title">
                        <span> *</span> Business type
                      </h6>
                    </div>
                    <div className="tab-checkbox-content">
                      <label htmlFor="" className="checkbox-parent">
                        <input type="checkbox" name="filter" />
                        <div className="checkmark"></div>
                        <p>Manufacturer</p>
                      </label>
                      <label htmlFor="" className="checkbox-parent">
                        <input type="checkbox" name="filter" />
                        <div className="checkmark"></div>
                        <p>Trading Company</p>
                      </label>
                      <label htmlFor="" className="checkbox-parent">
                        <input type="checkbox" name="filter" />
                        <div className="checkmark"></div>
                        <p>Buying Office</p>
                      </label>
                      <label htmlFor="" className="checkbox-parent">
                        <input type="checkbox" name="filter" />
                        <div className="checkmark"></div>
                        <p>Distributor / Wholesaler</p>
                      </label>
                      <label htmlFor="" className="checkbox-parent">
                        <input type="checkbox" name="filter" />
                        <div className="checkmark"></div>
                        <p>Manufacturer</p>
                      </label>
                      <label htmlFor="" className="checkbox-parent">
                        <input type="checkbox" name="filter" />
                        <div className="checkmark"></div>
                        <p>Trading Company</p>
                      </label>
                      <label htmlFor="" className="checkbox-parent">
                        <input type="checkbox" name="filter" />
                        <div className="checkmark"></div>
                        <p>Buying Office</p>
                      </label>
                    </div>
                    <div className="tab-main-products mt-56">
                      <div className="input-group resp-mb-0">
                        <div className="input-child mr-0 resp-mb-0">
                          <label htmlFor="">
                            <span>* </span> Main products
                          </label>
                          <Select
                            className="selectbox"
                            isMulti
                            options={options}
                            components={{
                              DropdownIndicator: () => null,
                              IndicatorSeparator: () => null,
                              animatedComponents,
                            }}
                            isSearchable={false}
                            menuPortalTarget={document.body}
                            menuPosition={'fixed'}
                            styles={customStyle}
                            placeholder="Add product"
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="s"></label>
                          <div className="tab-info">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <rect
                                width="24"
                                height="24"
                                rx="6"
                                fill="#0062FF"
                              />
                              <path
                                d="M12 19C15.866 19 19 15.866 19 12C19 8.13401 15.866 5 12 5C8.13401 5 5 8.13401 5 12C5 15.866 8.13401 19 12 19Z"
                                stroke="white"
                                stroke-linecap="round"
                                strokeLinejoin="round"
                              />
                              <path
                                d="M12 14.8V12"
                                stroke="white"
                                stroke-linecap="round"
                                strokeLinejoin="round"
                              />
                              <path
                                d="M12 9.2002H12.007"
                                stroke="white"
                                stroke-linecap="round"
                                strokeLinejoin="round"
                              />
                            </svg>
                            Please check <span>maximum 3</span> marks
                          </div>
                        </div>
                      </div>
                      <div className="input-child mt-72">
                        <button className="yellow-button profile-save">
                          <svg
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <rect width="24" height="24" rx="6" fill="white" />
                            <path
                              d="M17 9L10.8462 15L8 12"
                              stroke="#2D2D38"
                              strokeWidth="2"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                            />
                          </svg>
                          Save changes
                          <span></span>
                        </button>
                      </div>
                    </div>
                  </form>
                </TabPanel>
                <TabPanel tabId="2">
                  <div className="tab-content">
                    <div className="tab-header-second">
                      <h6 className="title">Prefered industries</h6>
                    </div>
                    <div className="tab-select-section">
                      <div className="tab-select">
                        <label htmlFor="">Industry 1</label>
                        <Select
                          className="selectbox"
                          options={options}
                          components={{ IndicatorSeparator: () => null }}
                          isSearchable={false}
                          menuPortalTarget={document.body}
                          menuPosition={'fixed'}
                          styles={customStyle2}
                          placeholder="Select industry"
                        />
                      </div>
                      <div className="tab-select">
                        <label htmlFor="">Industry 2</label>
                        <Select
                          className="selectbox"
                          options={options}
                          components={{ IndicatorSeparator: () => null }}
                          isSearchable={false}
                          menuPortalTarget={document.body}
                          menuPosition={'fixed'}
                          styles={customStyle2}
                          placeholder="Select industry"
                        />
                      </div>
                    </div>
                  </div>
                </TabPanel>
              </Tabs>
              {/* <button onClick={() => setSelectedTab((selectedTab + tabCount - 1) % tabCount)}>Back</button>
                            <button onClick={() => setSelectedTab((selectedTab + 1) % tabCount)}>Next</button> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Account;
