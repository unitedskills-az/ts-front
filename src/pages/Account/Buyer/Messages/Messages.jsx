import React from 'react';
import SideBar from '../../../../components/sidebar/BuyerSidebar/sidebar';
import Search from '../../../../components/miniSearch';
import SelectCountry from '../../../../components/miniSelect';
import MessageCard from '../../../../components/messagesCard';
const Messages = () => {
    return (
        <div className="background-light-blue pt-24 pb-48">
            <div className="container">
                <div className="row">
                    <div className="xl-3 lg-3 md-3 sm-12">
                        <SideBar />
                    </div>
                    <div className="xl-9 lg-9 md-9 sm-12">
                        <div className="message-content-right">
                            <div className="user-header">
                                <h5 className="title">
                                    Messages
                                </h5>
                                <div className="df aic jcsb user-header-child">
                                    <div className="mr-24 resp-mr-0">
                                        <SelectCountry />
                                    </div>
                                    <Search />
                                </div>
                            </div>
                            <MessageCard link={'/supplier/chat'} className="active" />
                            <MessageCard />
                            <MessageCard />
                            <MessageCard />
                            <MessageCard className="active" />
                            <MessageCard />
                            <MessageCard />
                            <MessageCard />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Messages
