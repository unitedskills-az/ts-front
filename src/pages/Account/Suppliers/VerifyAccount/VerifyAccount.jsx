import React from 'react';
import SideBar from '../../../../components/sidebar/SupplierSideBar/sidebar';
import '../CompanyInfo/CompanyInfo.scss';
import Select from 'react-select';
import Datepicker from '../../../../components/DatePicker/Datepicker'
const options = [
    { value: 'a', label: 'a' },
    { value: 'b', label: 'b' },
    { value: 'c', label: 'c' }

]
const customStyle2 = {
    control: (provided, styles) => ({
        ...provided,
        margin: 0,
        height: "4.8rem",
        marginLeft: 0,
        fontSize: "1.6rem",
        backgroundColor: 'white',
        outline: 'none',
        border: `transparent`,
        boxShadow: 'none',
        menuPortal: provided => ({ ...provided, zIndex: 9999 }),
        menu: provided => ({ ...provided, zIndex: 9999 }),
        "&:hover": {
            borderColor: "transparent",
        }
    }),
    valueContainer: (provided, state) => ({
        ...provided,
        padding: '0 2.4rem',
    }),

    menu: base => ({
        ...base,
        zIndex: 100
    })
}
const VerifyAccount = () => {
    return (
        <div className="company-info">
            <div className="container">
                <div className="row">
                    <div className="xl-3 lg-3 md-3 sm-12">
                        <SideBar />
                    </div>
                    <div className="xl-9 lg-9 md-9 sm-12">
                        <div className="company-info-content">
                            <div className="verify-account-content comp-info-tab-2 ">
                                <h2 className="title">
                                    Company Verification details
                                </h2>
                                <div className="input-group">
                                    <div className="input-child">
                                        <label htmlFor=""><span>* </span> Company Name</label>
                                        <input type="text" placeholder="John Doe LLC" />
                                    </div>
                                    <div className="input-child">
                                        <label htmlFor="">
                                            <span>* </span>
                                            366852147
                                        </label>
                                        <input type="number" name="" id="" placeholder="366852147" />
                                    </div>
                                </div>
                                <div className="input-group">
                                    <div className="input-child">
                                        <label htmlFor=""><span>* </span>Location of Registration</label>
                                        <Select className="selectbox" options={options} components={{ IndicatorSeparator: () => null }} isSearchable={false} menuPortalTarget={document.body}
                                            menuPosition={'fixed'} styles={customStyle2} placeholder="Azerbaijan" />
                                    </div>
                                    <div className="input-child">
                                        <label htmlFor="">
                                            <span>*</span>
                                            Year of Established
                                        </label>
                                        <Datepicker />
                                    </div>
                                </div>
                                <button className="yellow-button profile-save mt-56">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect width="24" height="24" rx="6" fill="white" />
                                        <path d="M17 9L10.8462 15L8 12" stroke="#2D2D38" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                                        Save changes
                                                        <span></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        </div >
    )
}

export default VerifyAccount
