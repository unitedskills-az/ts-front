import React, { useRef } from "react";
import SideBar from "../../../../components/sidebar/SupplierSideBar/sidebar";
import "./AddProduct.scss";
import Select from "react-select";
import ArrowDown from "../../../../assets/arrowdown.svg";
import { Editor } from "@tinymce/tinymce-react";
import Upload from "../../../../components/uploadfile/upload";
import makeAnimated from "react-select/animated";
import useAddProducts from "../common/hooks/useAddProduct";
import { useForm, Controller } from "react-hook-form";
import {
  useCategory,
  useSelectTable,
  useSelectTableCountry,
} from "../common/hooks";

const animatedComponents = makeAnimated();

const options = [
  { value: "a", label: "a" },
  { value: "b", label: "b" },
  { value: "c", label: "c" },
];
const customStyle2 = {
  control: (provided, styles) => ({
    ...provided,
    margin: 0,
    height: "4.8rem",
    marginLeft: 0,
    fontSize: "1.6rem",
    backgroundColor: "white",
    outline: "none",
    border: `transparent`,
    boxShadow: "none",
    menuPortal: provided => ({ ...provided, zIndex: 9999 }),
    menu: provided => ({ ...provided, zIndex: 9999 }),
    "&:hover": {
      borderColor: "transparent",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: "0 2.4rem",
  }),

  menu: (base) => ({
    ...base,
    zIndex: 100,
  }),
};
const customStyle = {
  control: (provided, styles) => ({
    ...provided,
    margin: 0,
    height: "4.6rem",
    marginLeft: 0,
    fontSize: "1.6rem",
    backgroundColor: "white",
    outline: "none",
    border: `transparent`,
    boxShadow: "none",
    menuPortal: provided => ({ ...provided, zIndex: 9999 }),
    menu: provided => ({ ...provided, zIndex: 9999 }),
    "&:hover": {
      borderColor: "transparent",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: ".5rem 2.4rem 0",
    height: "4.6rem",
  }),
  placeholder: () => ({
    marginTop: "-5.5rem",
    color: "#666666",
  }),
  multiValue: (base, state) => ({
    ...base,
    background: "#FFD05F",
    borderRadius: "2.5rem",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "1.6rem",
    lineHeight: "100%",
    color: "#2D2D38",
    padding: "0 1.6rem",
    height: "3.2rem",
    display: "flex",
    alignItems: "center",
    opacity: "1",
  }),
};

const AddProduct = () => {
  const editorRef = useRef(null);

  // products
  const { handleSubmit, control } = useForm({
    defaultValues: {
      category_id: "",
      origin_place_id: "",
      quantity_type_id: "",
      name: "",
      model_number: "",
      minimum_order_quantity: "",
      price: "",
      description: "",
      mainPhoto: "",
      secondaryPhoto: [],
      keywords: [],
      business_type: [],
    },
  });

  const { mutateProducts } = useAddProducts();

  const typeRef = useRef("");

  const descriptionRef = useRef("");

  const mainPhotoRef = useRef("");
  const secondaryPhotoRef = useRef("");

  const onSubmit = (data) => {
    data = {
      ...data,
      category_id:
        typeof data.category_id !== "object" ? "" : data.category_id.value,
      origin_place_id:
        typeof data.origin_place_id !== "object"
          ? ""
          : data.origin_place_id.value,
      quantity_type_id: "",
      business_type: [typeRef.current],
      description: descriptionRef.current,
      mainPhoto: mainPhotoRef.current.fileList,
      secondaryPhoto: secondaryPhotoRef.current,
    };

    const formData = new FormData();
    let fileArray = [];

    for (const key in data) {
      if (
        data[key] !== undefined &&
        key !== "gallery" &&
        typeof data[key] !== "object"
      ) {
        formData.append(key, data[key]);
      }
      if (key === "business_type") {
        formData.append(key + `[]`, data[key][0]);
      }
      if (key === "secondaryPhoto") {
        for (let index = 0; index < data[key].fileList.length; index++) {
          fileArray = [...fileArray, data[key].fileList[index].originFileObj];
          formData.append(key + `[]`, ...fileArray);
        }
      }
      if (key === "mainPhoto") {
        if (data[key].length !== 0) {
          formData.append(key, data[key][0].originFileObj);
        }
      }
    }

    mutateProducts(formData);
  };

  const { dataCountry, isLoadingCountry } = useSelectTableCountry("");

  const { category, isLoadingCategory } = useCategory();

  const { data, isLoading } = useSelectTable();

  return (
    <div className="addNewProduct pt-24 pb-72 background-light-blue">
      <div className="container">
        <div className="row">
          <div className="xl-3 lg-3 md-3 sm-12">
            <SideBar />
          </div>
          <div className="xl-9 lg-9 md-9 sm-12">
            <div className="add-product-container">
              <div className="row">
                <div className="xl-1"></div>
                <div className="xl-10 lg-10 md-10 sm-12">
                  <div className="add-product-header">
                    <h2 className="title montserrat font-s-normal font-w-bold font-24 line-h-100_ c-white">
                      Add new product
                    </h2>
                    <p className="markpro font-s-normal font-w-normal font-16 line-h-100% c-white mb-0">
                      Lorem Ipsum is simply dummy text.
                    </p>
                  </div>
                  <form
                    className="add-product-content"
                    onSubmit={handleSubmit(onSubmit)}
                  >
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">
                          <span>*</span>
                          Product Name
                        </label>
                        <Controller
                          name="name"
                          control={control}
                          render={({ field }) => (
                            <input
                              {...field}
                              type="text"
                              placeholder="Add product name"
                            />
                          )}
                        />
                      </div>
                      <div className="input-child">
                        <label htmlFor="">
                          <span>*</span>
                          Origin Place
                        </label>
                        <Controller
                          name="origin_place_id"
                          control={control}
                          render={({ field }) => (
                            <Select
                              {...field}
                              className="selectbox"
                              options={
                                isLoadingCountry === false &&
                                dataCountry !== undefined &&
                                dataCountry.data.map((item) => ({
                                  value: item.id,
                                  label: item.name,
                                }))
                              }
                              components={{ IndicatorSeparator: () => null }}
                              isSearchable={false}
                              menuPortalTarget={document.body}
                              menuPosition={'fixed'}
                              styles={customStyle2}
                              placeholder="Choose country"
                            />
                          )}
                        />
                      </div>
                    </div>
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">
                          <span>*</span>
                          Category
                        </label>
                        <Controller
                          name="category_id"
                          control={control}
                          render={({ field }) => (
                            <Select
                              {...field}
                              className="selectbox"
                              options={
                                isLoadingCategory === false &&
                                category !== undefined &&
                                category.data.map((item) => ({
                                  value: item.id,
                                  label: item.name,
                                }))
                              }
                              components={{ IndicatorSeparator: () => null }}
                              isSearchable={false}
                              menuPortalTarget={document.body}
                              menuPosition={'fixed'}
                              styles={customStyle2}
                              placeholder="Choose country"
                            />
                          )}
                        />
                      </div>
                      <div className="input-child">
                        <label htmlFor="">
                          <span>*</span>
                          Model Number
                        </label>
                        <Controller
                          name="model_number"
                          control={control}
                          render={({ field }) => (
                            <input
                              {...field}
                              type="text"
                              placeholder="Add Model Number"
                            />
                          )}
                        />
                      </div>
                    </div>
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">Reference FOB Price</label>
                        <div className="phone-country price-select">
                          <Controller
                            name="price"
                            control={control}
                            render={({ field }) => (
                              <input
                                {...field}
                                type="number"
                                placeholder="0 USD"
                              />
                            )}
                          />

                          <img src={ArrowDown} alt="" />
                          <select name="" id="">
                            <option value="">Price</option>
                          </select>
                        </div>
                      </div>
                      <div className="input-child">
                        <label htmlFor="">Minimum Order Quantity</label>
                        <Controller
                          name="minimum_order_quantity"
                          control={control}
                          render={({ field }) => (
                            <input
                              {...field}
                              type="text"
                              className="input-order-q"
                              placeholder=">100"
                            />
                          )}
                        />
                      </div>
                    </div>
                    <div className="mt-72 mb  resp-mt-56">
                      <h6 className="title montserrat font-s-normal font-w-bold font-24 line-h-100_ c-light-black mb-24">
                        Business Type
                      </h6>
                      <div className="df aic add-checkbox">
                        {isLoading === false &&
                          data !== undefined &&
                          data.data.productBusinessTypes.map((item) => (
                            <label
                              style={{ marginTop: 10, marginRight: 15 }}
                              htmlFor={item.name}
                              className="checkbox-parent"
                              key={item.id}
                            >
                              <input
                                type="radio"
                                name="filter"
                                id={item.name}
                                onClick={() => {
                                  typeRef.current = item.id;
                                }}
                              />
                              <div className="checkmark"></div>
                              <p>{item.name}</p>
                            </label>
                          ))}
                      </div>
                    </div>
                    <div className="mt-72 mb resp-mt-56">
                      <h6 className="title montserrat font-s-normal font-w-bold font-24 line-h-100_ c-light-black mb-24">
                        Product Description
                      </h6>
                      <Controller
                        name="price"
                        className="resp-none"
                        control={control}
                        render={({ field }) => (
                          <Editor
                            onInit={(evt, editor) =>
                              (editorRef.current = editor)
                            }
                            initialValue="<p>This is the initial content of the editor.</p>"
                            init={{
                              menubar: false,
                              plugins: [
                                "advlist autolink lists link image charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table paste code help wordcount",
                              ],
                              toolbar:
                                "undo redo | formatselect | " +
                                "bold italic backcolor | alignleft aligncenter " +
                                "alignright alignjustify | bullist numlist outdent indent | " +
                                "removeformat | help",
                              content_style:
                                "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                            }}
                            onChange={(e) => {
                              descriptionRef.current = e.level.content;
                            }}
                          />
                        )}
                      />
                    </div>
                    <div className="mt-72 mb resp-mt-56">
                      <h6 className="title montserrat font-s-normal font-w-bold font-24 line-h-100_ c-light-black mb-16">
                        Product Photos
                      </h6>

                      <p className="markpro font-s-normal font-w-bold font-16 line-h-100_ c-gray mb-32">
                        High resolution recommented (jpg, png, webp) | Max
                        1000x1000 and 5MB
                      </p>
                      <div className="row">
                        <div className="xl-2 lg-2 md-2 sm-12 upload-img-first">
                          <h5 className="add-subtitle">Main Picture</h5>
                          <Upload elem={mainPhotoRef} multiple={false} />
                        </div>
                        <div className="xl-2 lg-2 md-2 sm-12"></div>
                        <div className="xl-4 lg-4 md-4 sm-12">
                          <h5 className="add-subtitle">Featured Picture</h5>
                          <Upload elem={secondaryPhotoRef} />
                        </div>
                      </div>
                    </div>
                    <div className="mt-72 mb resp-mt-56 ">
                      <h6 className="title montserrat font-s-normal font-w-bold font-24 line-h-100_ c-light-black mb-32">
                        Keywords
                      </h6>
                      <div className="input-group resp-mb-0">
                        <div className="input-child">
                          <label htmlFor="">Add keywords</label>
                          <Select
                            className="selectbox"
                            isMulti
                            options={options}
                            components={{
                              DropdownIndicator: () => null,
                              IndicatorSeparator: () => null,
                              animatedComponents,
                            }}
                            isSearchable={false}
                            menuPortalTarget={document.body}
                            menuPosition={'fixed'}
                            styles={customStyle}
                            placeholder="Add product"
                          />
                        </div>
                        <div className="input-child resp-mb-0">
                          <label htmlFor="s"></label>
                          <div className="tab-info">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <rect
                                width="24"
                                height="24"
                                rx="6"
                                fill="#0062FF"
                              />
                              <path
                                d="M12 19C15.866 19 19 15.866 19 12C19 8.13401 15.866 5 12 5C8.13401 5 5 8.13401 5 12C5 15.866 8.13401 19 12 19Z"
                                stroke="white"
                                stroke-linecap="round"
                                strokeLinejoin="round"
                              />
                              <path
                                d="M12 14.8V12"
                                stroke="white"
                                stroke-linecap="round"
                                strokeLinejoin="round"
                              />
                              <path
                                d="M12 9.2002H12.007"
                                stroke="white"
                                stroke-linecap="round"
                                strokeLinejoin="round"
                              />
                            </svg>
                            Please check <span>maximum 3</span> marks
                          </div>
                        </div>
                      </div>
                      <div className="input-child mt-56">
                        <button className="yellow-button profile-save">
                          <svg
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <rect width="24" height="24" rx="6" fill="white" />
                            <path
                              d="M17 9L10.8462 15L8 12"
                              stroke="#2D2D38"
                              strokeWidth="2"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                            />
                          </svg>
                          Save changes
                          <span></span>
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
                <div className="xl-1"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddProduct;
