import React from 'react';
import SideBar from '../../../../components/sidebar/SupplierSideBar/sidebar';
import MiniSearch from '../../../../components/miniSearch';
import Request from '../../../../assets/request.png'
import SupportCard from '../../../../components/SupportCard'
const Support = () => {
    return (
        <div className="support background-light-blue pt-24 pb-72">
            <div className="container">
                <div className="row">
                    <div className="xl-3 lg-3 md-3 sm-12">
                        <SideBar />
                    </div>
                    <div className="xl-9 lg-9 md-9 sm-12">
                        <div className="message-content-right">
                            <div className="user-header">
                                <h5 className="title">
                                    Tickets
                                </h5>
                                <div className="df aic">
                                    <MiniSearch />
                                    <button className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro ml-56">
                                        <img src={Request} alt="" />
                                        Post your request
                                    </button>
                                </div>
                            </div>
                            <SupportCard />
                            <SupportCard />
                            <SupportCard />
                            <SupportCard />
                            <SupportCard />
                            <SupportCard />
                            <SupportCard />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Support
