import React, { useState, useRef, useEffect } from "react";
import SideBar from "../../../../components/sidebar/SupplierSideBar/sidebar";
import Request from "../../../../assets/request.png";
import "./CompanyInfo.scss";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import DatePicker from "../../../../components/DatePicker/Datepicker";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import Upload from "../../../../components/uploadfile/upload";

import {
  useBusiness,
  useCompanyInfo,
  useSelectTable,
  useCapability,
  useIntroduction,
  useManufacturing,
  useQualityControl,
  useSelectTableCountry,
  useSelectTableCountryItems,
  useCategory,
  useVerification,
  useSupplier,
  useCategoryItems,
} from "../common/hooks";
import { getDate } from "../common/helper";
import { useForm, Controller } from "react-hook-form";
import Storage from "../../../../services/storage";

const animatedComponents = makeAnimated();

const customStyle2 = {
  control: (provided, styles) => ({
    ...provided,
    margin: 0,
    height: "4.8rem",
    marginLeft: 0,
    fontSize: "1.6rem",
    backgroundColor: "white",
    outline: "none",
    border: `transparent`,
    boxShadow: "none",
    menuPortal: (provided) => ({ ...provided, zIndex: 9999 }),
    menu: (provided) => ({ ...provided, zIndex: 9999 }),
    "&:hover": {
      borderColor: "transparent",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: "0 2.4rem",
  }),

  menu: (base) => ({
    ...base,
    zIndex: 100,
  }),
};

const customStyle = {
  control: (provided, styles) => ({
    ...provided,
    margin: 0,
    height: "4.6rem",
    marginLeft: 0,
    fontSize: "1.6rem",
    backgroundColor: "white",
    outline: "none",
    border: `transparent`,
    boxShadow: "none",
    menuPortal: (provided) => ({ ...provided, zIndex: 9999 }),
    menu: (provided) => ({ ...provided, zIndex: 9999 }),
    "&:hover": {
      borderColor: "transparent",
    },
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    padding: ".5rem 2.4rem 0",
    height: "4.6rem",
  }),
  placeholder: () => ({
    marginTop: "-5.5rem",
    color: "#666666",
  }),
  multiValue: (base, state) => ({
    ...base,
    background: "#FFD05F",
    borderRadius: "2.5rem",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "1.6rem",
    lineHeight: "100%",
    color: "#2D2D38",
    padding: "0 1.6rem",
    height: "3.2rem",
    display: "flex",
    alignItems: "center",
    opacity: "1",
  }),
};
const CompanyInfo = () => {
  // category
  const { category, isLoadingCategory } = useCategory();
  const { mutateCategory, isLoadingCategoryItems, categoryItems } =
    useCategoryItems();

  // selectable
  const { data, isLoading } = useSelectTable();

  const [selectedTab] = useState(0);

  const [businessType, setBusinessType] = useState({
    business_types: [],
  });

  // mutationBusiness
  const { mutateBusiness } = useBusiness();

  // mutationCompanyInfo
  const { mutateCompanyInfo } = useCompanyInfo();

  // companyInfo form
  const { handleSubmit, control } = useForm({
    defaultValues: {
      company_name: "",
      country_id: "",
      city_id: "",
      location_of_registration_id: "",
      zip_code: "",
      street: "",
      year_of_established: "",
      number_of_employees: "",
      website: "",
      facebook: "",
      instagram: "",
      linkedin: "",
      advantage: "",
      main_products_category: [],
    },
  });

  const onSubmit = (data) => {
    let mainCategories = [];

    for (const key in data.main_products_category) {
      mainCategories = [
        ...mainCategories,
        data.main_products_category[key].value,
      ];
    }

    data = {
      ...data,
      country_id: selectId.value,
      city_id: typeof data.city_id !== "object" ? "" : data.city_id.value,
      location_of_registration_id:
        typeof data.location_of_registration_id !== "object"
          ? ""
          : data.location_of_registration_id.value,
      year_of_established:
        typeof data.year_of_established !== "object"
          ? ""
          : getDate(data.year_of_established),
      main_products_category: mainCategories,
    };

    mutateCompanyInfo(data);
  };

  // mutationCapability
  const { mutateCompanyCapability } = useCapability();

  const { handleSubmit: handleSubmitCapability, control: controlCapability } =
    useForm({
      defaultValues: {
        payment_types: [],
        payment_currencies: [],
        spoken_languages: [],
        delivery_terms: [],
        distributions: [],
        total_annual_revenue_id: "",
        export_percentage: "",
        start_exporting_year: "",
        employees_in_trade_id: "",
        average_lead_time: "",
        quality_control_staff_id: "",
        is_oversee_office: "",
        office_detail: "",
        overseeOffices: "",
      },
    });

  const isOfficeRef = useRef("");

  const distributionsRef = useRef([]);

  const [distributions, setDistributions] = useState([]);

  const [deliveryTerms, setdeliveryTerms] = useState([]);

  const [spokenLanguage, setspokenLanguage] = useState([]);

  const [paymentCurrencies, setpaymentCurrencies] = useState([]);

  const [paymentTypes, setpaymentTypes] = useState([]);

  const overseeOfficesRef = useRef("");

  const onSubmitCapability = (data) => {
    data = {
      ...data,
      total_annual_revenue_id:
        typeof data.total_annual_revenue_id !== "object"
          ? ""
          : data.total_annual_revenue_id.value,
      employees_in_trade_id:
        typeof data.employees_in_trade_id !== "object"
          ? ""
          : data.employees_in_trade_id.value,
      quality_control_staff_id:
        typeof data.quality_control_staff_id !== "object"
          ? ""
          : data.quality_control_staff_id.value,
      start_exporting_year:
        typeof data.start_exporting_year !== "object"
          ? ""
          : getDate(data.start_exporting_year),
      is_oversee_office: isOfficeRef.current,
      distributions: distributions,
      delivery_terms: deliveryTerms,
      spoken_languages: spokenLanguage,
      payment_currencies: paymentCurrencies,
      payment_types: paymentTypes,
      overseeOffices: overseeOfficesRef.current,
    };

    const formData = new FormData();
    let fileArray = [];

    for (const key in data) {
      if (
        data[key] !== undefined &&
        key !== "overseeOffices" &&
        typeof data[key] !== "object"
      ) {
        formData.append(key, data[key]);
      } else if (key === "overseeOffices") {
        for (
          let index = 0;
          index < data.overseeOffices.fileList.length;
          index++
        ) {
          fileArray = [
            ...fileArray,
            data.overseeOffices.fileList[index].originFileObj,
          ];
          formData.append("overseeOffices[]", ...fileArray);
        }
      } else if (typeof data[key] === "object" && key !== "overseeOffices") {
        for (const arr of data[key]) {
          formData.append(key + `[]`, arr);
        }
      }
    }

    mutateCompanyCapability(formData);
  };

  const isTradeShow = useRef("");
  const tradeShowsIntroductionRef = useRef("");
  const logoIntroductionRef = useRef("");
  const coverIntroductionRef = useRef("");
  const galleryIntroductionRef = useRef("");

  // company introduction
  const {
    handleSubmit: handleSubmitIntroduction,
    control: controlIntroduction,
  } = useForm({
    defaultValues: {
      detailed_company_introduction: "",
      video_introduction: "",
      is_trade_show: "",
      trade_show_company_advantages: "",
      gallery: [],
      tradeShows: [],
      logo: "",
      cover: "",
    },
  });

  const { mutateCompanyIntroduction } = useIntroduction();

  const onSubmitIntroduction = (data) => {
    data = {
      ...data,
      is_trade_show: isTradeShow.current,
      gallery: galleryIntroductionRef.current,
      tradeShows: tradeShowsIntroductionRef.current,
      logo: logoIntroductionRef.current.fileList,
      cover: coverIntroductionRef.current.fileList,
    };

    const formData = new FormData();
    let fileArray = [];

    for (const key in data) {
      if (
        data[key] !== undefined &&
        key !== "gallery" &&
        key !== "tradeShows" &&
        typeof data[key] !== "object"
      ) {
        formData.append(key, data[key]);
      }
      if (key === "tradeShows" || key === "gallery") {
        for (let index = 0; index < data[key].fileList.length; index++) {
          fileArray = [...fileArray, data[key].fileList[index].originFileObj];
          formData.append(key + `[]`, ...fileArray);
        }
      }
      if (key === "logo") {
        if (data[key].length !== 0) {
          formData.append(key, data[key][0].originFileObj);
        }
      }
      if (key === "cover") {
        if (data[key].length !== 0) {
          formData.append(key, data[key][0].originFileObj);
        }
      }
    }

    mutateCompanyIntroduction(formData);
  };

  // manufacturingCapability
  const isShowProductionProcessRef = useRef();
  const isShowProductionEquipmentRef = useRef();
  const { mutateManufacturing } = useManufacturing();
  const productionProcessGalleryRef = useRef();
  const productionEquipmentGalleryRef = useRef();

  const {
    handleSubmit: handleSubmitManufacturing,
    control: controlManufacturing,
  } = useForm({
    defaultValues: {
      is_show_production_process: "",
      show_production_process_company_advantages: "",
      is_show_production_equipment: "",
      show_production_equipment_company_advantages: "",
      factory_location_id: "",
      factory_area_size: "",
      total_annual_output_id: "",
      number_of_production_line: "",
      productionProcessGallery: [],
      productionEquipmentGallery: [],
    },
  });

  const onSubmitManufacturing = (data) => {
    data = {
      ...data,
      is_show_production_process: isShowProductionProcessRef.current,
      is_show_production_equipment: isShowProductionEquipmentRef.current,
      productionEquipmentGallery: productionEquipmentGalleryRef.current,
      productionProcessGallery: productionProcessGalleryRef.current,
      factory_location_id:
        typeof data.factory_location_id !== "object"
          ? ""
          : data.factory_location_id.value,
    };

    const formData = new FormData();
    let fileArray = [];

    for (const key in data) {
      if (
        data[key] !== undefined &&
        key !== "productionEquipmentGallery" &&
        key !== "productionProcessGallery" &&
        typeof data[key] !== "object"
      ) {
        formData.append(key, data[key]);
      }
      if (
        key === "productionEquipmentGallery" ||
        key === "productionProcessGallery"
      ) {
        for (let index = 0; index < data[key].fileList.length; index++) {
          fileArray = [...fileArray, data[key].fileList[index].originFileObj];
          formData.append(key + `[]`, ...fileArray);
        }
      }
    }

    mutateManufacturing(formData);
  };

  // qualityControl
  const { mutateQualityControl } = useQualityControl();

  const isQualityControlProcessRef = useRef("");
  const isTestingEquipmentRef = useRef("");
  const qualityControlGalleryRef = useRef("");
  const testingEquipmentGalleryRef = useRef("");

  const { handleSubmit: handleSubmitQuality, control: controlQuality } =
    useForm({
      defaultValues: {
        is_quality_control_process: "",
        quality_control_process_company_advantages: "",
        is_testing_equipment: "",
        testing_equipment_company_advantages: "",
        qc_staff_id: "",
        qualityControlGallery: [],
        testingEquipmentGallery: [],
      },
    });

  const onSubmitQuality = (data) => {
    data = {
      ...data,
      is_quality_control_process: isQualityControlProcessRef.current,
      is_testing_equipment: isTestingEquipmentRef.current,
      qualityControlGallery: qualityControlGalleryRef.current,
      testingEquipmentGallery: testingEquipmentGalleryRef.current,
    };

    const formData = new FormData();
    let fileArray = [];

    for (const key in data) {
      if (
        data[key] !== undefined &&
        key !== "productionEquipmentGallery" &&
        key !== "productionProcessGallery" &&
        typeof data[key] !== "object"
      ) {
        formData.append(key, data[key]);
      }
      if (
        key === "qualityControlGallery" ||
        key === "testingEquipmentGallery"
      ) {
        for (let index = 0; index < data[key].fileList.length; index++) {
          fileArray = [...fileArray, data[key].fileList[index].originFileObj];
          formData.append(key + `[]`, ...fileArray);
        }
      }
    }

    mutateQualityControl(formData);
  };

  // selectTableCountry

  const [selectId, setSelectId] = useState({
    value: "",
    label: "",
    slug: "",
  });
  const { dataCountry, isLoadingCountry } = useSelectTableCountry();

  const { dataCountryItems, isLoadingCountryItems } =
    useSelectTableCountryItems(selectId.slug);

  // verification
  const { supplier, isLoadingSupplier } = useSupplier("");

  // --- bu kod -- silinecey
  const { mutateVerification, verificationData } = useVerification();
  useEffect(() => {
    if (isLoadingSupplier === false) {
      Storage.set("subDomain", supplier.data.sub_domain);

      if (verificationData !== undefined) {
        Storage.set("subDomain", verificationData.data.sub_domain);
      }
    }
  });

  return (
    <div className="company-info">
      <div className="container">
        <div className="row">
          <div className="xl-3 lg-3 md-3 sm-12">
            <SideBar />
          </div>
          <div className="xl-9 lg-9 md-9 sm-12">
            <div className="company-info-progress-bar">
              <div className="progress-bar-parent">
                <h5 className="title markpro font-s-normal font-w-normal font-14 line-h-100_ c-light-black ">
                  Complete all tabs to{" "}
                  <span className="font-w-600">verify your shop!</span>
                </h5>
                <div className="df aic">
                  <div className="progressbar-percent">
                    <span>15%</span>
                    Completed
                  </div>
                  <div className="progress-bar-status">weak</div>
                </div>
                <div className="progress-bar"></div>
              </div>
              <div>
                <button
                  className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro"
                  onClick={() => {
                    mutateVerification();
                  }}
                >
                  <img src={Request} alt="" />
                  Sent to verification
                </button>
              </div>
            </div>
            <div className="company-info-content">
              <Tabs defaultTab={selectedTab.toString()}>
                <TabList>
                  <Tab tabFor="0">Business type</Tab>
                  <Tab tabFor="1">Company Details</Tab>
                  <Tab tabFor="2">Export Capability</Tab>
                  <Tab tabFor="3">Com. Introduction</Tab>
                  <Tab tabFor="4">Manufacturing Capability</Tab>
                  <Tab tabFor="5">Quality Control</Tab>
                </TabList>

                <TabPanel tabId="0">
                  <h2 className="title">
                    <span> *</span> Business type
                  </h2>
                  <div className="tab-checkbox-content">
                    {isLoading === false &&
                      data !== undefined &&
                      data.data !== null &&
                      data.data.businessTypes.map((item) => (
                        <label
                          htmlFor={item.name}
                          className="checkbox-parent"
                          key={item.id}
                        >
                          <input
                            type="checkbox"
                            name="filter"
                            id={item.name}
                            onClick={(e) => {
                              if (e.target.checked) {
                                setBusinessType((prev) => ({
                                  ...prev,
                                  business_types: [
                                    ...prev.business_types,
                                    item.id,
                                  ],
                                }));
                              } else {
                                const index =
                                  businessType.business_types.indexOf(
                                    item.id,
                                    0
                                  );
                                if (index > -1) {
                                  setBusinessType((prev) => ({
                                    ...prev,
                                    business_types: prev.business_types.filter(
                                      (e) => item.id !== e
                                    ),
                                  }));
                                }
                              }
                            }}
                          />
                          <div className="checkmark"></div>
                          <p>{item.name}</p>
                        </label>
                      ))}

                    <div className="input-child mt-72 resp-mt-56">
                      <button
                        className="yellow-button profile-save"
                        onClick={() => {
                          mutateBusiness(businessType);
                        }}
                      >
                        <svg
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <rect width="24" height="24" rx="6" fill="white" />
                          <path
                            d="M17 9L10.8462 15L8 12"
                            stroke="#2D2D38"
                            strokeWidth="2"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                          />
                        </svg>
                        Save changes
                        <span></span>
                      </button>
                    </div>
                  </div>
                </TabPanel>
                <TabPanel tabId="1">
                  <h2 className="title">Company operational details</h2>
                  <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="tab-checkbox-content comp-info-tab-2">
                      <div className="input-group">
                        <div className="input-child">
                          <label htmlFor="">
                            <span>* </span> Company Name
                          </label>
                          <Controller
                            name="company_name"
                            control={control}
                            render={({ field }) => (
                              <input
                                type="text"
                                placeholder="John Doe LLC"
                                {...field}
                              />
                            )}
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="">
                            <span>* </span> Country / Region
                          </label>
                          <Controller
                            name="country_id"
                            control={control}
                            render={({ field }) => (
                              <Select
                                {...field}
                                className="selectbox"
                                options={
                                  isLoadingCountry === false &&
                                  dataCountry !== undefined &&
                                  dataCountry.data !== null &&
                                  dataCountry.data.map((item) => ({
                                    value: item.id,
                                    label: item.name,
                                  }))
                                }
                                value={{
                                  name: selectId,
                                  label: selectId.label,
                                }}
                                onChange={(e) => {
                                  setSelectId((prev) => ({
                                    ...prev,
                                    value: e.value,
                                    label: e.label,
                                    slug: `/${e.value}`,
                                  }));
                                }}
                                components={{ IndicatorSeparator: () => null }}
                                isSearchable={false}
                                menuPortalTarget={document.body}
                                menuPosition={"fixed"}
                                styles={customStyle2}
                                placeholder="Azerbaijan"
                              />
                            )}
                          />
                        </div>
                      </div>
                      <div className="input-group">
                        <div className="input-child">
                          <label htmlFor="">
                            <span>* </span> Location of Registration
                          </label>
                          <Controller
                            name="location_of_registration_id"
                            control={control}
                            render={({ field }) => (
                              <Select
                                {...field}
                                className="selectbox"
                                options={
                                  isLoadingCountry === false &&
                                  dataCountry !== undefined &&
                                  dataCountry.data !== null &&
                                  dataCountry.data.map((item) => ({
                                    value: item.id,
                                    label: item.name,
                                  }))
                                }
                                components={{ IndicatorSeparator: () => null }}
                                isSearchable={false}
                                menuPortalTarget={document.body}
                                menuPosition={"fixed"}
                                styles={customStyle2}
                                placeholder="Azerbaijan"
                              />
                            )}
                          />
                        </div>

                        <div className="input-child">
                          <label htmlFor="">
                            <span>* </span>City / Town
                          </label>
                          <Controller
                            name="city_id"
                            control={control}
                            render={({ field }) => (
                              <Select
                                {...field}
                                className="selectbox"
                                options={
                                  isLoadingCountryItems === false &&
                                  dataCountryItems !== undefined &&
                                  dataCountry.data !== null &&
                                  dataCountryItems.data.map((item) => ({
                                    value: item.id,
                                    label: item.name,
                                  }))
                                }
                                components={{ IndicatorSeparator: () => null }}
                                isSearchable={false}
                                menuPortalTarget={document.body}
                                menuPosition={"fixed"}
                                styles={customStyle2}
                                placeholder="Select"
                              />
                            )}
                          />
                        </div>
                      </div>
                      <div className="input-group">
                        <div className="input-child">
                          <label htmlFor="">
                            <span>* </span> Zip Code
                          </label>
                          <Controller
                            name="zip_code"
                            control={control}
                            render={({ field }) => (
                              <input
                                type="text"
                                placeholder="AZ1001"
                                {...field}
                              />
                            )}
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="">
                            <span>* </span> Street / Road
                          </label>
                          <Controller
                            name="street"
                            control={control}
                            render={({ field }) => (
                              <input
                                {...field}
                                type="text"
                                placeholder="Bakikhanov str."
                              />
                            )}
                          />
                        </div>
                      </div>
                      <div className="input-group company-section">
                        <div className="input-child">
                          <label htmlFor="">
                            <span>*</span>
                            Year of Established
                          </label>
                          <Controller
                            control={control}
                            name="year_of_established"
                            render={({ field }) => <DatePicker field={field} />}
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="">
                            <span>*</span>
                            Number of Employees
                          </label>
                          <Controller
                            name="number_of_employees"
                            control={control}
                            render={({ field }) => (
                              <input
                                type="text"
                                placeholder="10 - 100"
                                {...field}
                              />
                            )}
                          />
                        </div>
                      </div>
                      <div className="input-group">
                        <div className="input-child">
                          <label htmlFor="">
                            <span>*</span> Main Categories
                          </label>
                          <Controller
                            name=""
                            control={control}
                            render={({ field }) => (
                              <Select
                                {...field}
                                className="selectbox"
                                options={
                                  isLoadingCategory === false &&
                                  category.data !== undefined &&
                                  category.data !== null &&
                                  category.data.map((item) => ({
                                    value: item.id,
                                    label: item.name,
                                  }))
                                }
                                components={{
                                  DropdownIndicator: () => null,
                                  IndicatorSeparator: () => null,
                                  animatedComponents,
                                }}
                                onChange={(e) => {
                                  mutateCategory(e.value);
                                }}
                                isSearchable={false}
                                menuPortalTarget={document.body}
                                menuPosition={"fixed"}
                                styles={customStyle}
                                placeholder="Add product"
                              />
                            )}
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="s"></label>
                          <div className="tab-info">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <rect
                                width="24"
                                height="24"
                                rx="6"
                                fill="#0062FF"
                              />
                              <path
                                d="M12 19C15.866 19 19 15.866 19 12C19 8.13401 15.866 5 12 5C8.13401 5 5 8.13401 5 12C5 15.866 8.13401 19 12 19Z"
                                stroke="white"
                                stroke-linecap="round"
                                strokeLinejoin="round"
                              />
                              <path
                                d="M12 14.8V12"
                                stroke="white"
                                stroke-linecap="round"
                                strokeLinejoin="round"
                              />
                              <path
                                d="M12 9.2002H12.007"
                                stroke="white"
                                stroke-linecap="round"
                                strokeLinejoin="round"
                              />
                            </svg>
                            Please check <span>maximum 3</span> marks
                          </div>
                        </div>
                      </div>
                      <div className="input-group">
                        <div className="input-child resp-mb-0">
                          <label htmlFor="">
                            <span>* </span>Main Products
                          </label>
                          <Controller
                            name="main_products_category"
                            control={control}
                            render={({ field }) => (
                              <Select
                                {...field}
                                className="selectbox"
                                isMulti
                                options={
                                  isLoadingCategoryItems === false &&
                                  categoryItems !== undefined &&
                                  categoryItems.data.children.map((item) => ({
                                    value: item.id,
                                    label: item.name,
                                  }))
                                }
                                components={{
                                  DropdownIndicator: () => null,
                                  IndicatorSeparator: () => null,
                                  animatedComponents,
                                }}
                                isSearchable={false}
                                menuPortalTarget={document.body}
                                menuPosition={"fixed"}
                                styles={customStyle}
                                placeholder="Add product"
                              />
                            )}
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="s"></label>
                          <div className="tab-info">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <rect
                                width="24"
                                height="24"
                                rx="6"
                                fill="#0062FF"
                              />
                              <path
                                d="M12 19C15.866 19 19 15.866 19 12C19 8.13401 15.866 5 12 5C8.13401 5 5 8.13401 5 12C5 15.866 8.13401 19 12 19Z"
                                stroke="white"
                                stroke-linecap="round"
                                strokeLinejoin="round"
                              />
                              <path
                                d="M12 14.8V12"
                                stroke="white"
                                stroke-linecap="round"
                                strokeLinejoin="round"
                              />
                              <path
                                d="M12 9.2002H12.007"
                                stroke="white"
                                stroke-linecap="round"
                                strokeLinejoin="round"
                              />
                            </svg>
                            Please check <span>maximum 3</span> marks
                          </div>
                        </div>
                      </div>
                      <h2 className="title">Social</h2>
                      <div className="input-group">
                        <div className="input-child">
                          <label htmlFor="">Website</label>
                          <Controller
                            name="website"
                            control={control}
                            render={({ field }) => (
                              <input
                                type="text"
                                placeholder="http://"
                                {...field}
                              />
                            )}
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="">Facebook</label>
                          <Controller
                            name="facebook"
                            control={control}
                            render={({ field }) => (
                              <input
                                type="text"
                                placeholder="http://"
                                {...field}
                              />
                            )}
                          />
                        </div>
                      </div>
                      <div className="input-group">
                        <div className="input-child">
                          <label htmlFor="">Instagram</label>
                          <Controller
                            name="instagram"
                            control={control}
                            render={({ field }) => (
                              <input
                                type="text"
                                placeholder="http://"
                                {...field}
                              />
                            )}
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="">Linkedin</label>
                          <Controller
                            name="linkedin"
                            control={control}
                            render={({ field }) => (
                              <input
                                type="text"
                                placeholder="http://"
                                {...field}
                              />
                            )}
                          />
                        </div>
                      </div>
                      <div className="input-group">
                        <div className="input-child m-0">
                          <label htmlFor="">Company Advantages</label>
                          <Controller
                            name="advantage"
                            control={control}
                            render={({ field }) => (
                              <textarea
                                name=""
                                id=""
                                cols="30"
                                rows="10"
                                placeholder="Tell us more"
                                {...field}
                              ></textarea>
                            )}
                          />
                        </div>
                      </div>
                      <div className="input-child mt-72 resp-mt-56">
                        <button
                          className="yellow-button profile-save"
                          type="submit"
                        >
                          <svg
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <rect width="24" height="24" rx="6" fill="white" />
                            <path
                              d="M17 9L10.8462 15L8 12"
                              stroke="#2D2D38"
                              strokeWidth="2"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                            />
                          </svg>
                          Save changes
                          <span></span>
                        </button>
                      </div>
                    </div>
                  </form>
                </TabPanel>
                <TabPanel tabId="2">
                  <h2 className="title">Export Capability</h2>
                  <form onSubmit={handleSubmitCapability(onSubmitCapability)}>
                    <div className="tab-checkbox-content comp-info-tab-2">
                      <div className="input-group">
                        <div className="input-child">
                          <label htmlFor="">Total Annual Revenue</label>
                          <Controller
                            name="total_annual_revenue_id"
                            control={controlCapability}
                            render={({ field }) => (
                              <Select
                                {...field}
                                className="selectbox"
                                options={data.data.totalAnnualRevenues.map(
                                  (item) => ({
                                    value: item.id,
                                    label: item.name,
                                  })
                                )}
                                components={{
                                  IndicatorSeparator: () => null,
                                }}
                                isSearchable={false}
                                menuPortalTarget={document.body}
                                menuPosition={"fixed"}
                                styles={customStyle2}
                                placeholder="50 - 100"
                              />
                            )}
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="">Export Percentage (%)</label>
                          <Controller
                            name="export_percentage"
                            control={controlCapability}
                            render={({ field }) => (
                              <input
                                {...field}
                                type="text"
                                name="export_percentage"
                                id=""
                                placeholder="95%"
                              />
                            )}
                          />
                        </div>
                      </div>
                      <div className="input-group">
                        <div className="input-child">
                          <label htmlFor="">Start Exporting Year</label>
                          <Controller
                            name="start_exporting_year"
                            control={controlCapability}
                            render={({ field }) => <DatePicker field={field} />}
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="">
                            Number of Employees in Trade Departmet
                          </label>
                          <Controller
                            name="employees_in_trade_id"
                            control={controlCapability}
                            render={({ field }) => (
                              <Select
                                {...field}
                                className="selectbox"
                                options={data.data.employeesInTrades.map(
                                  (item) => ({
                                    value: item.id,
                                    label: item.name,
                                  })
                                )}
                                components={{ IndicatorSeparator: () => null }}
                                isSearchable={false}
                                menuPortalTarget={document.body}
                                menuPosition={"fixed"}
                                styles={customStyle2}
                                placeholder="50 - 100"
                              />
                            )}
                          />
                        </div>
                      </div>
                      <div className="input-group">
                        <div className="input-child">
                          <label htmlFor="">Average Lead Time (days)</label>
                          <Controller
                            name="average_lead_time"
                            control={controlCapability}
                            render={({ field }) => (
                              <input
                                {...field}
                                type="text"
                                placeholder="14 days"
                              />
                            )}
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="">
                            Number of Quality Control Staff
                          </label>
                          <Controller
                            name="quality_control_staff_id"
                            control={controlCapability}
                            render={({ field }) => (
                              <Select
                                {...field}
                                className="selectbox"
                                options={data.data.qualityControlStaffs.map(
                                  (item) => ({
                                    value: item.id,
                                    label: item.name,
                                  })
                                )}
                                components={{ IndicatorSeparator: () => null }}
                                isSearchable={false}
                                menuPortalTarget={document.body}
                                menuPosition={"fixed"}
                                styles={customStyle2}
                                placeholder="1 - 10"
                              />
                            )}
                          />
                        </div>
                      </div>
                      <div className="tab-content-question">
                        <div className="mt-40 df aifs w-100_ mb-24 resp-question">
                          <span className="montserrat font-s-normal font-w-500 font-16 line-h-100_ c-light-black">
                            Does your Company has an oversee office(s)?
                          </span>

                          <div className="df aic ml-72 resp-display-row">
                            <label htmlFor="" className="checkbox-parent">
                              <input
                                type="radio"
                                name="filter"
                                onClick={() => {
                                  isOfficeRef.current = 1;
                                }}
                                checked
                              />
                              <div className="checkmark"></div>
                              <p>Yes</p>
                            </label>
                            <label htmlFor="" className="checkbox-parent ml-32">
                              <input
                                onClick={() => {
                                  isOfficeRef.current = 0;
                                }}
                                type="radio"
                                name="filter"
                                className="input-no"
                              />
                              <div className="checkmark"></div>
                              <p>No</p>
                            </label>
                          </div>
                        </div>
                        <div className="input-group">
                          <div className="input-child">
                            <label htmlFor="">Upload Files</label>
                            <Upload elem={overseeOfficesRef} />
                          </div>
                        </div>
                        <div className="input-group w-100_">
                          <div className="input-child m-0">
                            <label htmlFor="">Office Details</label>
                            <Controller
                              name="office_detail"
                              control={controlCapability}
                              render={({ field }) => (
                                <textarea
                                  {...field}
                                  name=""
                                  id=""
                                  cols="30"
                                  rows="10"
                                  placeholder="Tell us more"
                                ></textarea>
                              )}
                            />
                          </div>
                        </div>
                        <div className="company-info-progress-bar mt-72 mb-48">
                          <div className="progress-bar-parent">
                            <h5 className="title markpro font-s-normal font-w-normal font-14 line-h-100_ c-light-black ">
                              Complete persentage to{" "}
                              <span className="font-w-600">100%</span>
                            </h5>
                            <div className="df aic">
                              <div className="progressbar-percent">
                                <span>15%</span>
                                Completed
                              </div>
                            </div>
                            <div className="progress-bar"></div>
                          </div>
                        </div>
                        <h2 className="title">
                          <span> *</span> Main markets & distributions
                        </h2>

                        <div className="main-dist  mb-48">
                          <div className="row">
                            {isLoading === false &&
                              data.data.distributions.map((item, index) => (
                                <div
                                  className="xl-3 lg-3 md-3 sm-12"
                                  key={item}
                                >
                                  <div className="main-dist-child">
                                    <label htmlFor="">{item.name}</label>
                                    <input
                                      type="number"
                                      placeholder="0%"
                                      onChange={(e) => {
                                        if (e.target.value !== "")
                                          distributionsRef.current[index] =
                                            Number(e.target.value);
                                        else
                                          distributionsRef.current[index] =
                                            e.target.value;

                                        setDistributions(
                                          distributionsRef.current
                                        );
                                      }}
                                    />
                                  </div>
                                </div>
                              ))}
                          </div>
                        </div>
                        <h2 className="title">
                          <span> *</span>Accepted delivery terms
                        </h2>
                        <div className="tab-checkbox-content tab-checkbox-content-2">
                          {isLoading === false &&
                            data !== undefined &&
                            data.data !== null &&
                            data.data.deliveryTerms.map((item) => (
                              <label
                                htmlFor={item.name}
                                className="checkbox-parent"
                                key={item.id}
                              >
                                <input
                                  type="checkbox"
                                  name="filterDelivery"
                                  id={item.id}
                                  onClick={(e) => {
                                    if (e.target.checked) {
                                      setdeliveryTerms((prev) => [
                                        ...prev,
                                        item.id,
                                      ]);
                                    } else {
                                      const index = deliveryTerms.indexOf(
                                        item.id,
                                        0
                                      );
                                      if (index > -1) {
                                        setdeliveryTerms((prev) =>
                                          prev.filter((e) => item.id !== e)
                                        );
                                      }
                                    }
                                  }}
                                />
                                <div className="checkmark"></div>
                                <p>{item.name}</p>
                              </label>
                            ))}
                        </div>
                        <h2 className="title">
                          <span> *</span>Language spoken
                        </h2>
                        <div className="tab-checkbox-content tab-checkbox-content-2">
                          {isLoading === false &&
                            data !== undefined &&
                            data.data !== null &&
                            data.data.languageSpokens.map((item) => (
                              <label
                                htmlFor={item.name}
                                className="checkbox-parent"
                                key={item.id}
                              >
                                <input
                                  type="checkbox"
                                  name={item.name}
                                  onClick={(e) => {
                                    if (e.target.checked) {
                                      setspokenLanguage((prev) => [
                                        ...prev,
                                        item.id,
                                      ]);
                                    } else {
                                      const index = spokenLanguage.indexOf(
                                        item.id,
                                        0
                                      );
                                      if (index > -1) {
                                        setspokenLanguage((prev) =>
                                          prev.filter((e) => item.id !== e)
                                        );
                                      }
                                    }
                                  }}
                                />
                                <div className="checkmark"></div>
                                <p>{item.name}</p>
                              </label>
                            ))}
                        </div>
                        <h2 className="title">
                          <span> *</span> Accepted payment currency
                        </h2>
                        <div className="tab-checkbox-content tab-checkbox-content-2">
                          {isLoading === false &&
                            data !== undefined &&
                            data.data !== null &&
                            data.data.currencies.map((item) => (
                              <label
                                htmlFor={item.name}
                                className="checkbox-parent"
                                key={item.id}
                              >
                                <input
                                  type="checkbox"
                                  name={item.name}
                                  onClick={(e) => {
                                    if (e.target.checked) {
                                      setpaymentCurrencies((prev) => [
                                        ...prev,
                                        item.id,
                                      ]);
                                    } else {
                                      const index = paymentCurrencies.indexOf(
                                        item.id,
                                        0
                                      );
                                      if (index > -1) {
                                        setpaymentCurrencies((prev) =>
                                          prev.filter((e) => item.id !== e)
                                        );
                                      }
                                    }
                                  }}
                                />
                                <div className="checkmark"></div>
                                <p>{item.name}</p>
                              </label>
                            ))}
                        </div>
                        <h2 className="title">
                          <span> *</span> Accepted payment type
                        </h2>
                        <div className="tab-checkbox-content tab-checkbox-content-2">
                          {isLoading === false &&
                            data !== undefined &&
                            data.data !== null &&
                            data.data.paymentTypes.map((item) => (
                              <label
                                htmlFor={item.name}
                                className="checkbox-parent"
                                key={item.id}
                              >
                                <input
                                  type="checkbox"
                                  name={item.name}
                                  onClick={(e) => {
                                    if (e.target.checked) {
                                      setpaymentTypes((prev) => [
                                        ...prev,
                                        item.id,
                                      ]);
                                    } else {
                                      const index = paymentTypes.indexOf(
                                        item.id,
                                        0
                                      );
                                      if (index > -1) {
                                        setpaymentTypes((prev) =>
                                          prev.filter((e) => item.id !== e)
                                        );
                                      }
                                    }
                                  }}
                                />
                                <div className="checkmark"></div>
                                <p>{item.name}</p>
                              </label>
                            ))}
                        </div>
                        <div className="input-child">
                          <button
                            className="yellow-button profile-save"
                            type="submit"
                          >
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <rect
                                width="24"
                                height="24"
                                rx="6"
                                fill="white"
                              />
                              <path
                                d="M17 9L10.8462 15L8 12"
                                stroke="#2D2D38"
                                strokeWidth="2"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                              />
                            </svg>
                            Save changes
                            <span></span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </TabPanel>
                <TabPanel tabId="3">
                  <form
                    onSubmit={handleSubmitIntroduction(onSubmitIntroduction)}
                  >
                    <div className="tab-header mb-32 resp-mb-32 upload-img-tab">
                      <Upload elem={logoIntroductionRef} multiple={false} />
                      <div>
                        <h6 className="title">Company Logo</h6>
                        <p>
                          High resolution recommented (jpg, png, webp) | <br />{" "}
                          Max 1000x1000 and 5MB
                        </p>
                      </div>
                    </div>
                    <div className="tab-header mb-72 resp-mb-32 upload-img-tab">
                      <Upload elem={coverIntroductionRef} multiple={false} />
                      <div>
                        <h6 className="title">Cover Photo</h6>
                        <p>
                          High resolution recommented (jpg, png, webp) | <br />{" "}
                          Max 1920x1080 and 5MB
                        </p>
                      </div>
                    </div>
                    <div className="input-group mb-56 resp-mb-32">
                      <div className="input-child">
                        <label htmlFor="">
                          <span>*</span> Detailed Company Introduction
                        </label>
                        <Controller
                          name="detailed_company_introduction"
                          control={controlIntroduction}
                          render={({ field }) => (
                            <textarea
                              {...field}
                              name=""
                              id=""
                              cols="30"
                              rows="10"
                              placeholder="Tell us more"
                            ></textarea>
                          )}
                        />
                      </div>
                    </div>
                    <div className="input-group w-45_ mb-56 resp-mb-32">
                      <div className="input-child">
                        <label htmlFor="">
                          Company Video Introduction (comp URL from youtube or
                          vimeo)
                        </label>
                        <Controller
                          name="video_introduction"
                          control={controlIntroduction}
                          render={({ field }) => (
                            <input
                              {...field}
                              type="text"
                              placeholder="http://"
                            />
                          )}
                        />
                      </div>
                    </div>
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">Gallery</label>
                        <Upload elem={galleryIntroductionRef} />
                      </div>
                    </div>
                    <div className="mt-40 df aifs w-100_ mt-72 mb-24 resp-mt-0 resp-question">
                      <span className="montserrat font-s-normal font-w-500 font-16 line-h-100_ c-light-black">
                        Does your Company has an oversee office(s)?
                      </span>

                      <div className="df aic ml-72  resp-display-row resp-mb-16">
                        <label htmlFor="" className="checkbox-parent">
                          <input
                            type="radio"
                            name="filter"
                            onClick={() => {
                              isTradeShow.current = 1;
                            }}
                            checked
                          />
                          <div className="checkmark"></div>
                          <p>Yes</p>
                        </label>
                        <label htmlFor="" className="checkbox-parent ml-32">
                          <input
                            type="radio"
                            name="filter"
                            onClick={() => {
                              isTradeShow.current = 0;
                            }}
                            className="input-no"
                          />
                          <div className="checkmark"></div>
                          <p>No</p>
                        </label>
                      </div>
                    </div>
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">Upload Files</label>
                        <Upload elem={tradeShowsIntroductionRef} />
                      </div>
                    </div>
                    <div className="input-group mb-56">
                      <div className="input-child">
                        <label htmlFor="">
                          <span>*</span> Detailed Company Introduction
                          Advantages
                        </label>

                        <Controller
                          name="trade_show_company_advantages"
                          control={controlIntroduction}
                          render={({ field }) => (
                            <textarea
                              {...field}
                              name=""
                              id=""
                              cols="30"
                              rows="10"
                              placeholder="Tell us more"
                            ></textarea>
                          )}
                        />
                      </div>
                    </div>
                    <button
                      className="yellow-button profile-save"
                      type="submit"
                    >
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <rect width="24" height="24" rx="6" fill="white" />
                        <path
                          d="M17 9L10.8462 15L8 12"
                          stroke="#2D2D38"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                      Save changes
                      <span></span>
                    </button>
                  </form>
                </TabPanel>
                <TabPanel tabId="4">
                  <form
                    onSubmit={handleSubmitManufacturing(onSubmitManufacturing)}
                  >
                    <div className="df aifs w-100_  mb-24 resp-question">
                      <span className="montserrat font-s-normal font-w-500 font-16 line-h-100_ c-light-black">
                        Whether to show production proccess?
                      </span>

                      <div className="df aic ml-72 resp-display-row">
                        <label htmlFor="" className="checkbox-parent">
                          <input
                            onClick={() => {
                              isShowProductionProcessRef.current = 1;
                            }}
                            type="radio"
                            name="filterManufacturing"
                            checked
                          />
                          <div className="checkmark"></div>
                          <p>Yes</p>
                        </label>
                        <label htmlFor="" className="checkbox-parent ml-32">
                          <input
                            onClick={() => {
                              isShowProductionProcessRef.current = 0;
                            }}
                            type="radio"
                            name="filterManufacturing"
                            className="input-no"
                          />
                          <div className="checkmark"></div>
                          <p>No</p>
                        </label>
                      </div>
                    </div>
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">Upload Files</label>
                        <Upload elem={productionProcessGalleryRef} />
                      </div>
                    </div>
                    <div className="input-group mb-56 resp-mb-32">
                      <div className="input-child">
                        <label htmlFor="">
                          <span>*</span> Production Proccess Details
                        </label>
                        <Controller
                          name="show_production_process_company_advantages"
                          control={controlManufacturing}
                          render={({ field }) => (
                            <textarea
                              {...field}
                              name=""
                              id=""
                              cols="30"
                              rows="10"
                              placeholder="Tell us more"
                            ></textarea>
                          )}
                        />
                      </div>
                    </div>
                    <div className="mt-40 df aifs w-100_  mb-24 resp-question">
                      <span className="montserrat font-s-normal font-w-500 font-16 line-h-100_ c-light-black">
                        Whether to show production equipment?
                      </span>

                      <div className="df aic ml-72 resp-display-row">
                        <label htmlFor="" className="checkbox-parent">
                          <input
                            type="radio"
                            name="filterEquipment"
                            onClick={() => {
                              isShowProductionEquipmentRef.current = 1;
                            }}
                            checked
                          />
                          <div className="checkmark"></div>
                          <p>Yes</p>
                        </label>
                        <label htmlFor="" className="checkbox-parent ml-32">
                          <input
                            type="radio"
                            name="filterEquipment"
                            onClick={() => {
                              isShowProductionEquipmentRef.current = 0;
                            }}
                            className="input-no"
                          />
                          <div className="checkmark"></div>
                          <p>No</p>
                        </label>
                      </div>
                    </div>
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">Upload Files</label>
                        <Upload elem={productionEquipmentGalleryRef} />
                      </div>
                    </div>
                    <div className="input-group mb-72 resp-mb-48">
                      <div className="input-child">
                        <label htmlFor="">Production Equipment Details</label>
                        <Controller
                          name="show_production_equipment_company_advantages"
                          control={controlManufacturing}
                          render={({ field }) => (
                            <textarea
                              {...field}
                              name=""
                              id=""
                              cols="30"
                              rows="10"
                              placeholder="Tell us more"
                            ></textarea>
                          )}
                        />
                      </div>
                    </div>
                    <div className="comp-info-tab-2 mb-56 mt-72 resp-mt-0 ">
                      <div className="input-group">
                        <div className="input-child">
                          <label htmlFor="">
                            <span>*</span> Factory Locations
                          </label>
                          <Controller
                            name="factory_location_id"
                            control={controlManufacturing}
                            render={({ field }) => (
                              <Select
                                {...field}
                                className="selectbox"
                                options={
                                  isLoadingCountry === false &&
                                  dataCountry !== undefined &&
                                  dataCountry.data !== null &&
                                  dataCountry.data.map((item) => ({
                                    value: item.id,
                                    label: item.name,
                                  }))
                                }
                                components={{ IndicatorSeparator: () => null }}
                                isSearchable={false}
                                menuPortalTarget={document.body}
                                menuPosition={"fixed"}
                                styles={customStyle2}
                                placeholder="Azerbaijan"
                              />
                            )}
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="">
                            <span>*</span> Factory Area Size (square metr)
                          </label>
                          <Controller
                            name="factory_area_size"
                            control={controlManufacturing}
                            render={({ field }) => (
                              <input
                                {...field}
                                type="text"
                                placeholder="0  kv. m."
                              />
                            )}
                          />
                        </div>
                      </div>
                      <div className="input-group">
                        <div className="input-child">
                          <label htmlFor="">
                            <span>*</span> Total Annual Output
                          </label>
                          <Controller
                            name="total_annual_output_id"
                            control={controlManufacturing}
                            render={({ field }) => (
                              <input
                                {...field}
                                type="text"
                                placeholder="50.000 - 60.000"
                              />
                            )}
                          />
                        </div>
                        <div className="input-child">
                          <label htmlFor="">
                            <span>*</span> Number of Production Lines
                          </label>
                          <Controller
                            name="number_of_production_line"
                            control={controlManufacturing}
                            render={({ field }) => (
                              <input {...field} type="text" placeholder="4" />
                            )}
                          />
                        </div>
                      </div>
                    </div>
                    <button
                      className="yellow-button profile-save"
                      type="submit"
                    >
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <rect width="24" height="24" rx="6" fill="white" />
                        <path
                          d="M17 9L10.8462 15L8 12"
                          stroke="#2D2D38"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                      Save changes
                      <span></span>
                    </button>
                  </form>
                </TabPanel>
                <TabPanel tabId="5">
                  <form onSubmit={handleSubmitQuality(onSubmitQuality)}>
                    <div className="df aifs w-100_  mb-24 resp-question">
                      <span className="montserrat font-s-normal font-w-500 font-16 line-h-100_ c-light-black">
                        Whether to show production proccess?
                      </span>

                      <div className="df aic ml-72 resp-display-row resp-mb-16">
                        <label htmlFor="" className="checkbox-parent">
                          <input
                            type="radio"
                            name="filter"
                            onClick={() => {
                              isQualityControlProcessRef.current = 1;
                            }}
                            checked
                          />
                          <div className="checkmark"></div>
                          <p>Yes</p>
                        </label>
                        <label htmlFor="" className="checkbox-parent ml-32">
                          <input
                            type="radio"
                            name="filter"
                            onClick={() => {
                              isQualityControlProcessRef.current = 0;
                            }}
                            className="input-no"
                          />
                          <div className="checkmark"></div>
                          <p>No</p>
                        </label>
                      </div>
                    </div>
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">Upload Files</label>
                        <Upload elem={qualityControlGalleryRef} />
                      </div>
                    </div>
                    <div className="input-group mb-72">
                      <div className="input-child">
                        <label htmlFor="">Company Advantages</label>
                        <Controller
                          name="quality_control_process_company_advantages"
                          control={controlQuality}
                          render={({ field }) => (
                            <textarea
                              {...field}
                              name=""
                              id=""
                              cols="30"
                              rows="10"
                              placeholder="Tell us more"
                            ></textarea>
                          )}
                        />
                      </div>
                    </div>
                    <div className="df aifs w-100_  mb-24 resp-question">
                      <span className="montserrat font-s-normal font-w-500 font-16 line-h-100_ c-light-black">
                        Whether to show production proccess?
                      </span>

                      <div className="df aic ml-72 resp-display-row resp-mb-16">
                        <label htmlFor="" className="checkbox-parent">
                          <input
                            type="radio"
                            name="filter"
                            onClick={() => {
                              isTestingEquipmentRef.current = 1;
                            }}
                            checked
                          />
                          <div className="checkmark"></div>
                          <p>Yes</p>
                        </label>
                        <label htmlFor="" className="checkbox-parent ml-32">
                          <input
                            type="radio"
                            name="filter"
                            onClick={() => {
                              isTestingEquipmentRef.current = 1;
                            }}
                            className="input-no"
                          />
                          <div className="checkmark"></div>
                          <p>No</p>
                        </label>
                      </div>
                    </div>
                    <div className="input-group">
                      <div className="input-child">
                        <label htmlFor="">Upload Files</label>
                        <Upload elem={testingEquipmentGalleryRef} />
                      </div>
                    </div>
                    <div className="input-group mb-72 resp-mb-48">
                      <div className="input-child">
                        <label htmlFor="">Company Advantages</label>
                        <Controller
                          name="testing_equipment_company_advantages"
                          control={controlQuality}
                          render={({ field }) => (
                            <textarea
                              {...field}
                              name=""
                              id=""
                              cols="30"
                              rows="10"
                              placeholder="Tell us more"
                            ></textarea>
                          )}
                        />
                      </div>
                    </div>
                    <div className="input-group w-45_ mb-56">
                      <div className="input-child">
                        <label htmlFor="">Number of QC Staff</label>
                        <Controller
                          name="qc_staff_id"
                          control={controlQuality}
                          render={({ field }) => (
                            <input
                              {...field}
                              type="text"
                              placeholder="10 - 50"
                            />
                          )}
                        />
                      </div>
                    </div>
                    <button
                      className="yellow-button profile-save"
                      type="submit"
                    >
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <rect width="24" height="24" rx="6" fill="white" />
                        <path
                          d="M17 9L10.8462 15L8 12"
                          stroke="#2D2D38"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                      Save changes
                      <span></span>
                    </button>
                  </form>
                </TabPanel>
              </Tabs>
              {/* <button onClick={() => setSelectedTab((selectedTab + tabCount - 1) % tabCount)}>Back</button>
                            <button onClick={() => setSelectedTab((selectedTab + 1) % tabCount)}>Next</button> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CompanyInfo;
