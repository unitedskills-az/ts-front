import React, { useState } from 'react';
import './Chat.scss';
import Plus from '../../../../assets/plus.png';
import Request from '../../../../assets/request.png';
import { ReactComponent as Arrow } from '../../../../assets/arrowinner.svg';
import ChatProductCard from '../../../../components/chatProductCard';
import ChatCard from '../../../../components/chatCard';
import { ReactComponent as EmptyCard } from '../../../../assets/card.svg'
import { Link } from 'react-router-dom';

const Chat = () => {
    const [state, setState] = useState("");
    const toggleAccordion = () => {
        setState(state === "" ? "active" : "");
    }
    const empty = false;
    const total = false;
    let totalBUtton;
    if (total) {

        totalBUtton = <button className="total-discount-button active"> Total discount</button>

    } else {

        totalBUtton = <button className="total-discount-button"> Total discount</button>
    }
    return (
        <div className="chat suppliers-chat background-light-blue pt-24 pb-72">
            <div className="container">
                <div className="row">
                    <div className="xl-8 lg-8 md-8 sm-12">
                        <div className="message-content-right pb-0">
                            <div className="user-header">
                                <Link to="/supplier/chat/orders" className="subtitle font-s-normal montserrat font-w-500 font-14 line-h-100_ c-light-black df aic jcfs">
                                    Order Historys
                                    <Arrow />
                                </Link>
                                <div className="df aic jcfe">
                                    {totalBUtton}
                                    <Link to="/products" className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro ml-24">
                                        <img src={Plus} alt="" />
                                            Add product
                                    </Link>
                                </div>
                            </div>
                        </div>
                        {(empty) ?
                            <div className="message-content-left df aic h-590">
                                <div className="message-content-left-center">
                                    <EmptyCard />
                                    <p>There is nobody here. Add new product and <br /> start to shopping!</p>
                                    <Link to="/products" className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro ml-48">
                                        <img src={Plus} alt="" />
                                                  Add product
                                    </Link>
                                </div>
                            </div>
                            :
                            <div className="message-content-left-parent">
                                <div className="message-content-left">
                                    <ChatProductCard className='suppliersOrder' />
                                </div>
                                <div className="message-content-bottom">
                                    <div className="message-content-bottom-child">
                                        <div className="df aic jcsb w-55_">
                                            <div>
                                                <div className="title">
                                                    Summary
                                            </div>
                                                <p><span>$</span> 987654.35</p>
                                            </div>
                                            <div>
                                                <div className="title">
                                                    Discount
                                            </div>
                                                <p><span>$</span> 654</p>
                                            </div>
                                            <div>
                                                <div className="title">
                                                    Summary
                                            </div>
                                                <p><span>$</span> 987000.35</p>
                                            </div>

                                        </div>
                                        <button className={`header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro  ${state}`} onClick={toggleAccordion} >
                                            <img src={Request} alt="" />
                                            <span className="add">
                                                Confirm invoice
                                            </span>
                                            <span className="offer">
                                                Confirmed
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        }
                    </div>
                    <div className="xl-4 lg-4 md-4 sm-12">
                        <div className="chat-message">
                            <ChatCard />
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default Chat
