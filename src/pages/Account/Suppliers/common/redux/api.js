import { sRequestFunc } from "../../../../../api";
import { URLS } from "../../../../../api/routing";

export const postBusinessType = (businessType) =>
  sRequestFunc
    .post(URLS.BUSINESS__TYPE(), businessType)
    .then((res) => res.data);

export const getCompaniesSelectable = () =>
  sRequestFunc.get(URLS.COMPANY__SELECTABLE()).then((res) => res.data);

export const postCompanyInfo = (companyInfo) =>
  sRequestFunc.post(URLS.COMPANY__INFO(), companyInfo).then((res) => res.data);

export const postCompanyCapability = (capability) =>
  sRequestFunc
    .post(URLS.COMPANY__CAPABILITY(), capability)
    .then((res) => res.data);

export const postCompanyIntroduction = (introduction) =>
  sRequestFunc
    .post(URLS.COMPANY__INTRODUCTION(), introduction)
    .then((res) => res.data);

export const postManufacturingCapability = (introduction) =>
  sRequestFunc
    .post(URLS.MANUFACTURING__INTRODUCTION(), introduction)
    .then((res) => res.data);

export const postQualityControl = (introduction) =>
  sRequestFunc
    .post(URLS.QUALITY__CONTROL(), introduction)
    .then((res) => res.data);

export const getSelectTableCountry = (id) =>
  sRequestFunc
    .get(URLS.SELECTABLE__COUNTRY(id.queryKey[1]))
    .then((res) => res.data);

export const getSelectableCountryItem = (id) =>
  sRequestFunc
    .get(URLS.SELECTABLE__COUNTRY__ITEMS(id.queryKey[1]))
    .then((res) => res.data);

export const postAddProduts = (products) =>
  sRequestFunc.post(URLS.ADD__PRODUCTS(), products).then((res) => res.data);

export const getCategory = () =>
  sRequestFunc.get(URLS.CATEGORY()).then((res) => res.data);

export const getCategoryItems = (id) =>
  sRequestFunc.get(URLS.CATEGORY__ITEMS(id)).then((res) => res.data);

export const getProducts = (products) =>
  sRequestFunc
    .get(URLS.GET__PRODUCTS(products.queryKey[1], products.queryKey[2]))
    .then((res) => res.data);

export const postVerification = (verification) =>
  sRequestFunc
    .post(URLS.POST__VERIFICATION(verification))
    .then((res) => res.data);

export const getSuppliers = (supplier) =>
  sRequestFunc
    .get(URLS.GET__SUPPLIERS(supplier.queryKey[1]))
    .then((res) => res.data);
export const getSuppliersProduct = (supplier) =>
  sRequestFunc
    .get(
      URLS.GET__SUPPLIERS__PRODUCTS(supplier.queryKey[1], supplier.queryKey[2])
    )
    .then((res) => res.data);

export const getCurrentSuppliers = () =>
  sRequestFunc.get(URLS.GET__CURRENT__SUPPLIER()).then((res) => res.data);
