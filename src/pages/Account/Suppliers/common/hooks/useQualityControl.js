import { useMutation } from "react-query";
import { postQualityControl } from "../redux/api";
import { showNotification } from "../helper";

const useQualityControl = () => {
  const { mutate: mutateQualityControl, isLoading } = useMutation(
    (params) => postQualityControl(params),
    {
      onSuccess: () => {
        showNotification("", "Success!", "success");
      },
      onError: () => {
        showNotification("", "Error!", "error");
      },
    }
  );
  return { mutateQualityControl, isLoading };
};

export default useQualityControl;
