import { useQuery } from "react-query";
import { getSuppliersProduct } from "../redux/api";

const useSupplierProduct = (slug, page) => {
  const { data: supplierProduct, isLoading: isLoadingSupplierProduct } =
    useQuery(["getSupplierProduct", slug, page], getSuppliersProduct);

  return { supplierProduct, isLoadingSupplierProduct };
};

export default useSupplierProduct;
