import { useMutation } from "react-query";
import { getCategoryItems } from "../redux/api";

const useCategoryItems = () => {
  const {
    data: categoryItems,
    mutate: mutateCategory,
    isLoading: isLoadingCategoryItems,
  } = useMutation((id) => getCategoryItems(id));

  return { categoryItems, mutateCategory, isLoadingCategoryItems };
};

export default useCategoryItems;
