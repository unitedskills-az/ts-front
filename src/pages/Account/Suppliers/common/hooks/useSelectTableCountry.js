import { useQuery } from "react-query";
import { getSelectTableCountry } from "../redux/api";

const useSelectTableCountry = (id) => {
  const { data: dataCountry, isLoading: isLoadingCountry } = useQuery(
    ["selectTableCountry", id],
    getSelectTableCountry
  );

  return { dataCountry, isLoadingCountry };
};

export default useSelectTableCountry;
