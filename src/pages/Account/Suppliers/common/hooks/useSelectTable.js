import { useQuery } from "react-query";
import { getCompaniesSelectable } from "../redux/api";

const useSelectTable = () => {
  const { data, isLoading } = useQuery(
    ["companiesSelectable"],
    getCompaniesSelectable
  );

  return { data, isLoading };
};

export default useSelectTable;
