import { useQuery } from "react-query";
import { getSuppliers } from "../redux/api";

const useSupplier = (slug) => {
  const { data: supplier, isLoading: isLoadingSupplier } = useQuery(
    ["getSupplier", slug],
    getSuppliers
  );

  return { supplier, isLoadingSupplier };
};

export default useSupplier;
