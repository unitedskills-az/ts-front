import { useMutation } from "react-query";
import { postVerification } from "../redux/api";
import { showNotification } from "../helper";

const useVerification = () => {
  const {
    mutate: mutateVerification,
    data: verificationData,
    isLoading: isLoadingVerification,
  } = useMutation((verification) => postVerification(verification), {
    onSuccess: () => {
      showNotification("", "Success!", "success");
    },
    onError: () => {
      showNotification("", "Error!", "error");
    },
  });

  return { mutateVerification, verificationData, isLoadingVerification };
};

export default useVerification;
