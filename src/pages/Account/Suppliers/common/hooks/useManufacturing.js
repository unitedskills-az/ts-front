import { useMutation } from "react-query";
import { postManufacturingCapability } from "../redux/api";
import { showNotification } from "../helper";

const useManufacturing = () => {
  const { mutate: mutateManufacturing, isLoading } = useMutation(
    (params) => postManufacturingCapability(params),
    {
      onSuccess: () => {
        showNotification("", "Success!", "success");
      },
      onError: () => {
        showNotification("", "Error!", "error");
      },
    }
  );

  return { mutateManufacturing, isLoading };
};

export default useManufacturing;
