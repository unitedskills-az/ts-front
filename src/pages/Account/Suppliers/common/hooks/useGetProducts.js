import { useQuery } from "react-query";
import { getProducts } from "../redux/api";

const useGetProducts = (products, page) => {
  console.log(products);
  const { data: dataProducts, isLoading: isLoadingProducts } = useQuery(
    ["AllProducts", products, page],
    getProducts
  );

  return { dataProducts, isLoadingProducts };
};

export default useGetProducts;
