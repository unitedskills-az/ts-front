import { useMutation } from "react-query";
import { postCompanyInfo } from "../redux/api";
import { showNotification } from "../helper";

const useCompanyInfo = (reset) => {
  const { mutate: mutateCompanyInfo, isLoading } = useMutation(
    (params) => postCompanyInfo(params),
    {
      onSuccess: () => {
        showNotification("", "Success!", "success");
      },
      onError: () => {
        showNotification("", "Error!", "error");
      },
    }
  );

  return { mutateCompanyInfo, isLoading };
};

export default useCompanyInfo;
