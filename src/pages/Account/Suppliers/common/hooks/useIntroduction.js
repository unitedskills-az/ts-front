import { useMutation } from "react-query";
import { postCompanyIntroduction } from "../redux/api";
import { showNotification } from "../helper";

const useIntroduction = () => {
  const {
    mutate: mutateCompanyIntroduction,
    isLoading,
    isSuccess: isSuccessIntroduction,
  } = useMutation((params) => postCompanyIntroduction(params), {
    onSuccess: () => {
      showNotification("", "Success!", "success");
    },
    onError: () => {
      showNotification("", "Error!", "error");
    },
  });

  return { mutateCompanyIntroduction, isLoading, isSuccessIntroduction };
};

export default useIntroduction;
