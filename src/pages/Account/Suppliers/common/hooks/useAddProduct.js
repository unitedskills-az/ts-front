import { useMutation } from "react-query";
import { postAddProduts } from "../redux/api";
import { showNotification } from "../helper";

const useAddProducts = () => {
  const { mutate: mutateProducts, isLoading: isLoadingPorudcts } = useMutation(
    (params) => postAddProduts(params),
    {
      onSuccess: () => {
        showNotification("", "Success!", "success");
      },
      onError: () => {
        showNotification("", "Error!", "error");
      },
    }
  );

  return { mutateProducts, isLoadingPorudcts };
};

export default useAddProducts;
