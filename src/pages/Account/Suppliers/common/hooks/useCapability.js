import { useMutation } from "react-query";
import { postCompanyCapability } from "../redux/api";
import { showNotification } from "../helper";

const useCapability = () => {
  const { mutate: mutateCompanyCapability, isLoading } = useMutation(
    (params) => postCompanyCapability(params),
    {
      onSuccess: () => {
        showNotification("", "Success!", "success");
      },
      onError: () => {
        showNotification("", "Error!", "error");
      },
    }
  );

  return { mutateCompanyCapability, isLoading };
};

export default useCapability;
