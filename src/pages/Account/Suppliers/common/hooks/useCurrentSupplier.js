import { useQuery } from "react-query";
import { getCurrentSuppliers } from "../redux/api";

const useCurrentSupplier = () => {
  const {
    data: currentSuppliers,
    isLoading: isLoadingCurentSuppliers,
    refetch,
  } = useQuery(["CurrentSupplier"], getCurrentSuppliers, {
    cacheTime: 0,
  });

  return { currentSuppliers, isLoadingCurentSuppliers, refetch };
};

export default useCurrentSupplier;
