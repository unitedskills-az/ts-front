import { useQuery } from "react-query";
import { getCategory } from "../redux/api";

const useCategory = () => {
  const { data: category, isLoading: isLoadingCategory } = useQuery(
    ["getCategory"],
    getCategory
  );

  return { category, isLoadingCategory };
};

export default useCategory;
