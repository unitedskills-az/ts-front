import { useQuery } from "react-query";
import { getSelectableCountryItem } from "../redux/api";

const useSelectTableCountryItems = (id) => {
  const { data: dataCountryItems, isLoading: isLoadingCountryItems } = useQuery(
    ["selectTableCountryItems", id],
    getSelectableCountryItem
  );

  return { dataCountryItems, isLoadingCountryItems };
};

export default useSelectTableCountryItems;
