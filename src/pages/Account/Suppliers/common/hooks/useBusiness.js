import { useMutation } from "react-query";
import { postBusinessType } from "../redux/api";
import { showNotification } from "../helper";

const useBusinessType = () => {
  const { mutate: mutateBusiness, isLoading } = useMutation(
    (params) => postBusinessType(params),
    {
      onSuccess: () => {
        showNotification("", "Success!", "success");
      },
      onError: () => {
        showNotification("", "Error!", "error");
      },
    }
  );

  return { mutateBusiness, isLoading };
};

export default useBusinessType;
