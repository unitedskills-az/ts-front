import React from 'react';
import "./Dashboard.scss";
import Sidebar from '../../../../components/sidebar/SupplierSideBar/sidebar';
import Dashboard1 from '../../../../assets/dashboard.svg';
import Dashboard2 from '../../../../assets/dashboard2.svg';
import Dashboard3 from '../../../../assets/dashboard3.svg';
import Dashboard4 from '../../../../assets/dashboard4.svg';
import Dashboard5 from '../../../../assets/dashboard5.svg';
import Dashboard6 from '../../../../assets/dashboard6.svg';
import DashboardCard from '../../../../components/Dashboard/dashboard';
import DashboardInfoCard from '../../../../components/sidebar/SupplierSideBar/dashboardInfoCard'
const Dashboard = () => {
    return (
        <div className="dashboard">
            <div className="container">
                <div className="row resp-display-column">
                    <div className="xl-3 lg-3 md-3 sm-12">
                        <Sidebar />
                    </div>
                    <div className="xl-6 lg-6 md-6 sm-12">
                        <div className="row">
                            <div className="xl-6 lg-6 md-6 sm-12">
                                <DashboardCard link="/buyer/account" title={'Orders'} desc={'Lorem Ipsum is simply dummy text'} image={Dashboard1} />
                            </div>
                            <div className="xl-6 lg-6 md-6 sm-12">
                                <DashboardCard link="/buyer/account" title={'Messages'} desc={'Lorem Ipsum is simply dummy text'} image={Dashboard2} />
                            </div>
                            <div className="xl-6 lg-6 md-6 sm-12">
                                <DashboardCard link="/buyer/account" title={'RFQ Inquiries'} desc={'Lorem Ipsum is simply dummy text'} image={Dashboard3} />
                            </div>
                            <div className="xl-6 lg-6 md-6 sm-12">
                                <DashboardCard link="/buyer/account" title={'Company Page'} desc={'Lorem Ipsum is simply dummy text'} image={Dashboard4} />
                            </div>
                            <div className="xl-6 lg-6 md-6 sm-12">
                                <DashboardCard link="/buyer/account" title={'Training Center'} desc={'Lorem Ipsum is simply dummy text'} image={Dashboard5} />
                            </div>
                            <div className="xl-6 lg-6 md-6 sm-12">
                                <DashboardCard link="/buyer/account" title={'Analytics'} desc={'Lorem Ipsum is simply dummy text'} image={Dashboard6} />
                            </div>
                        </div>
                    </div>
                    <div className="xl-3 lg-3 md-3 sm-12 resp-order-1">
                        <DashboardInfoCard />
                    </div>
                </div>
            </div>
        </div >
    )
}

export default Dashboard
