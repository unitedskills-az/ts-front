import React, { useState } from "react";
import SideBar from "../../../../components/sidebar/SupplierSideBar/sidebar";
import ProductCard from "../../../../components/productCardSuppliers";
import Request from "../../../../assets/request.png";
import MiniSearch from "../../../../components/miniSearch";
import SelectCountry from "../../../../components/miniSelect";
import { Link } from "react-router-dom";
import { useSupplierProduct } from "../common/hooks";

const Products = () => {
  const [page] = useState(0);

  const { supplierProduct, isLoadingSupplierProduct } = useSupplierProduct(
    "",
    page
  );

  return (
    <div className="products background-light-blue pt-24 pb-48">
      <div className="container">
        <div className="row">
          <div className="xl-3 lg-3 md-3 sm-12">
            <SideBar />
          </div>
          <div className="xl-9 lg-9 md-9 sm-12">
            <div className="message-content-right">
              <div className="user-header">
                <h6 className="subtitle">
                  Published
                  <span>Unpublished</span>
                </h6>
                <div className="df aic">
                  <div className="mr-32">
                    <SelectCountry />
                  </div>
                  <MiniSearch />
                  <Link
                    to="/supplier/add-product"
                    className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro ml-56"
                  >
                    <img src={Request} alt="" />
                    Add new product
                  </Link>
                </div>
              </div>
            </div>
            <div className="row">
              {isLoadingSupplierProduct === false &&
                supplierProduct !== undefined &&
                supplierProduct.data.map((item) => (
                  <div className="xl-4 lg-4 md-4 sm-12" key={item.id}>
                    <ProductCard data={item} />
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Products;
