import React from 'react';
import SideBar from '../../../../components/sidebar/SupplierSideBar/sidebar';
import AnalyticsCard from '../../../../components/Dashboard/dashboard';
import Dashboard1 from '../../../../assets/dashboard.svg';
import Dashboard2 from '../../../../assets/dashboard2.svg';
import Dashboard3 from '../../../../assets/dashboard3.svg';
import Dashboard5 from '../../../../assets/dashboard7.svg';
import Dashboard6 from '../../../../assets/dashboard6.svg';

const Analytics = () => {
    return (
        <div className="background-light-blue pt-24 pb-48 analytics-card">
            <div className="container">
                <div className="row">
                    <div className="xl-3 lg-3 md-3 sm-12">
                        <SideBar />
                    </div>
                    <div className="xl-9 lg-9 md-9 sm-12">
                        <div className="row">
                            <div className="xl-4 lg-4 md-4 sm-12">
                                <AnalyticsCard link="/buyer/account" title={'website clicks'} number="3641" desc={'Lorem Ipsum is simply dummy text'} image={Dashboard6} />
                            </div>
                            <div className="xl-4 lg-4 md-4 sm-12">
                                <AnalyticsCard link="/buyer/account" title={'products count'} number="3641" desc={'Lorem Ipsum is simply dummy text'} image={Dashboard5} />
                            </div>
                            <div className="xl-4 lg-4 md-4 sm-12">
                                <AnalyticsCard link="/buyer/account" title={'orders'} number="3641" subtitle="(lifetime)" desc={'Lorem Ipsum is simply dummy text'} image={Dashboard1} />
                            </div>
                            <div className="xl-4 lg-4 md-4 sm-12">
                                <AnalyticsCard link="/buyer/account" title={'messages'} number="3641" desc={'Lorem Ipsum is simply dummy text'} image={Dashboard2} />
                            </div>
                            <div className="xl-4 lg-4 md-4 sm-12">
                                <AnalyticsCard link="/buyer/account" title={'RFQ requiries'} number="3641" desc={'Lorem Ipsum is simply dummy text'} image={Dashboard3} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Analytics
