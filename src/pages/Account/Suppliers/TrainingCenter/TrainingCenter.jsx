import React from 'react';
import SideBar from '../../../../components/sidebar/SupplierSideBar/sidebar';
import TrainingCard from '../../../../components/trainingVideoCard/trainingVideoCard';
const TrainingCenter = () => {
    return (
        <div className='background-light-blue pt-24 pb-48 traning-center-page'>
            <div className="container">
                <div className="row">
                    <div className="xl-3 lg-3 md-3 sm-12">
                        <SideBar />
                    </div>
                    <div className="xl-9 lg-9 md-9 sm-12">
                        <div className="traningCenter-content">
                            <div className="row">
                                <div className="xl-6 lg-6 md-6 sm-12">
                                    <TrainingCard />
                                </div>
                                <div className="xl-6 lg-6 md-6 sm-12">
                                    <TrainingCard />
                                </div>
                                <div className="xl-6 lg-6 md-6 sm-12">
                                    <TrainingCard />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TrainingCenter
