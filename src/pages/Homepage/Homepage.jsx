import React from "react";
import HomeTop from "../../components/Homepage/homeTop";
import HomeCategory from "../../components/Homepage/homeCategory";
import HomeCompanies from "../../components/Homepage/homeCompanies";
import HomeRequest from "../../components/Homepage/homeRequest";
import HomeRecommend from "../../components/Homepage/homeRecommend";

const Homepage = () => {
  return (
    <div className="homepage">
      <div className="homepage-top">
        <HomeTop />
        <HomeCategory />
        <HomeCompanies />
        <HomeRequest />
        <HomeRecommend />
      </div>
    </div>
  );
};

export default Homepage;
