import React from 'react';
import './About.scss';
import Sidebar from '../../components/sidebar/staticSidebar/sidebar';
// img
import AboutImg from '../../assets/categroy.png';
import AboutCard from '../../assets/about.svg';
import AboutCard2 from '../../assets/about2.svg';
import AboutCard3 from '../../assets/about3.svg';
import AboutCard4 from '../../assets/about4.png';
const About = () => {
    return (
        <div className="about">
            <div className="container">
                <div className="row">
                    <div className="xl-2 lg-2 md-2 sm-12">
                        <Sidebar />
                    </div>
                    <div className="xl-9 lg-9 md-9 sm-12 about-main-img">
                        <h4 className="title montserrat font-s-normal font-w-bold font-32 line-h-150_ c-black w-50_ mb-24">
                            Our E-commerce solutions are
                            your online trade vision
                        </h4>
                        <img src={AboutImg} alt="about page" />
                        <p>
                            Lorem Ipsum is simply dummy text of the printing
                            and typesetting industry. Lorem Ipsum has been
                            the industry's standard dummy text ever since
                            Lorem Ipsum is simply dummy text of the printing
                            and typesetting industry. Lorem Ipsum has been the industry's
                            standard dummy text ever since
                        </p>
                        <p>
                            Lorem Ipsum is simply dummy
                            text of the printing and
                            typesetting industry. Lorem Ipsum
                            has been the industry's standard dummy
                            text ever since Lorem Ipsum is simply
                            dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since
                        </p>
                        <div className="about-card-parent">
                            <div className="row">
                                <div className="xl-5 lg-5 md-5 sm-12">
                                    <div className="about-card">
                                        <div className="title">
                                            <img src={AboutCard} alt="" />
                                            <span>
                                                Ownership
                                            </span>
                                        </div>
                                        <p>
                                            Lorem Ipsum is simply dummy
                                            text of the printing and
                                            typesetting industry. Lorem Ipsum
                                            has been the industry's standard dummy
                                            text ever since Lorem Ipsum is simply
                                        </p>
                                    </div>
                                </div>
                                <div className="xl-1"></div>
                                <div className="xl-5 lg-5 md-5 sm-12">
                                    <div className="about-card">
                                        <div className="title">
                                            <img src={AboutCard2} alt="" />
                                            <span>
                                                Think Big
                                            </span>
                                        </div>
                                        <p>
                                            Lorem Ipsum is simply dummy
                                            text of the printing and
                                            typesetting industry. Lorem Ipsum
                                            has been the industry's standard dummy
                                            text ever since Lorem Ipsum is simply
                                        </p>
                                    </div>
                                </div>
                                <div className="xl-5 lg-5 md-5 sm-12">
                                    <div className="about-card">
                                        <div className="title">
                                            <img src={AboutCard3} alt="" />
                                            <span>
                                                Ownership
                                            </span>
                                        </div>
                                        <p>
                                            Lorem Ipsum is simply dummy
                                            text of the printing and
                                            typesetting industry. Lorem Ipsum
                                            has been the industry's standard dummy
                                            text ever since Lorem Ipsum is simply
                                        </p>
                                    </div>
                                </div>
                                <div className="xl-1"></div>
                                <div className="xl-5 lg-5 md-5 sm-12">
                                    <div className="about-card">
                                        <div className="title">
                                            <img src={AboutCard4} alt="" />
                                            <span>
                                                Think Big
                                            </span>
                                        </div>
                                        <p>
                                            Lorem Ipsum is simply dummy
                                            text of the printing and
                                            typesetting industry. Lorem Ipsum
                                            has been the industry's standard dummy
                                            text ever since Lorem Ipsum is simply
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="xl-1"></div>
                </div>
            </div>
        </div>
    )
}
export default About