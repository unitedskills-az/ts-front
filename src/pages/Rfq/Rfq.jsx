import React from 'react';
import './Rfq.scss';
import BreadCrumb from '../../components/breadCrumb/breadCrumb';
import Select from 'react-select';
import ArrowDown from '../../assets/arrowdown.svg';
import DatePicker from '../../components/DatePicker/Datepicker';
import Upload from '../../components/uploadfile/upload'
const options = [
    { value: 'a', label: 'a' },
    { value: 'b', label: 'b' },
    { value: 'c', label: 'c' }
]
const customStyle2 = {
    control: (provided, styles) => ({
        ...provided,
        margin: 0,
        height: "4.8rem",
        marginLeft: 0,
        fontSize: "1.6rem",
        backgroundColor: 'white',
        outline: 'none',
        border: `transparent`,
        boxShadow: 'none',
        menuPortal: provided => ({ ...provided, zIndex: 9999 }),
        menu: provided => ({ ...provided, zIndex: 9999 }),
        "&:hover": {
            borderColor: "transparent",
        }
    }),
    valueContainer: (provided, state) => ({
        ...provided,
        padding: '0 2.4rem',
    }),

    menu: base => ({
        ...base,
        zIndex: 100
    })
}
const Rfq = () => {
    return (
        <div className='rfq-page background-light-blue'>
            <div className="container">
                <BreadCrumb />

                <div className="rfq-content">
                    <div className="row">
                        <div className="xl-1"></div>
                        <div className="xl-10 lg-10 md-10 sm-12">
                            <div className="rfq-content-child">
                                <div className="row">
                                    <div className="xl-6 lg-6 md-6 sm-12">
                                        <h6>
                                            Tell Supplier What You Need
                                        </h6>
                                        <p>More information helps you faster response.</p>

                                        <div className="input-group mb-32">
                                            <div className="input-child">
                                                <label htmlFor="">
                                                    Product Name
                                                </label>
                                                <input type="text" placeholder="Keywords or name of product you want" />
                                            </div>
                                        </div>
                                        <div className="input-group mb-32">
                                            <div className="input-child">
                                                <label htmlFor="">
                                                    Category
                                                </label>
                                                <Select className="selectbox" options={options} components={{ IndicatorSeparator: () => null }} isSearchable={false} menuPortalTarget={document.body}
                                                    menuPosition={'fixed'} styles={customStyle2} placeholder="Select Category" />
                                            </div>
                                        </div>
                                        <div className="input-group mb-32">
                                            <div className="input-child">
                                                <label htmlFor="">
                                                    Product Description
                                                </label>
                                                <textarea name="" id="" cols="30" rows="10" placeholder="Add product details"></textarea>
                                            </div>
                                        </div>
                                        <div className="input-group  mb-32 ">
                                            <div className="input-child">
                                                <label htmlFor="">
                                                    Product Description
                                                </label>
                                                <div className="phone-country price-select">
                                                    <input type="number" placeholder='Quantity' />
                                                    <img src={ArrowDown} alt="" />
                                                    <select name="" id="">
                                                        <option value="">
                                                            Pieces
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="input-child input-datepicker">
                                                <label htmlFor="">
                                                    Valid to
                                                </label>
                                                <DatePicker />
                                            </div>
                                        </div>
                                        <div className="input-group mb-56">
                                            <div className="input-child">
                                                <label htmlFor="">Upload Files</label>
                                                <Upload />
                                            </div>
                                        </div>
                                        <div className="input-group w-340">
                                            <button className="yellow-button font-w-500 class-send">
                                                Send Request
                                            </button>
                                        </div>
                                    </div>
                                    <div className="xl-1"></div>
                                    <div className="xl-5 lg-5 md-5 sm-12">
                                        <div className="rfq-progress-bar"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="xl-1"></div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Rfq
