import React from 'react';
import './Favorites.scss';
import SideBar from '../../components/sidebar/BuyerSidebar/sidebar';
import FavCard from '../../components/favoriteCard'
const Favorites = () => {
    return (
        <div className="favorites">
            <div className="container">
                <div className="row">
                    <div className="xl-3 lg-3 md-3 sm-12">
                        <SideBar />
                    </div>
                    <div className="xl-9 lg-9 md-9 sm-12">
                        <div className="row">
                            <div className="xl-4 lg-4 md-4 sm-12">
                                <FavCard />
                            </div>
                            <div className="xl-4 lg-4 md-4 sm-12">
                                <FavCard />
                            </div>
                            <div className="xl-4 lg-4 md-4 sm-12">
                                <FavCard />
                            </div>
                            <div className="xl-4 lg-4 md-4 sm-12">
                                <FavCard />
                            </div>
                            <div className="xl-4 lg-4 md-4 sm-12">
                                <FavCard />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Favorites