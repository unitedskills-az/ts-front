import React from 'react';
import Filter from '../../components/filter/productPageFilter';
import Breadcrumb from '../../components/breadCrumb/breadCrumb';
import './Suppliers.scss';
import TopFilter from '../../components/filter/topFilter';
import SupplierCard from '../../components/suppliers/suppliers card'
const Suppliers = () => {
    return (
        <div className="suppliers">
            <div className="container">
                <Breadcrumb />
                <div className="row mt-24">
                    <div className="xl-3 lg-3 md-3 sm-12">
                        <Filter />
                    </div>
                    <div className="xl-9 lg-9 md-9 sm-12">
                        <TopFilter />
                        <div className="row">
                            <div className="xl-12 lg-12 md-12 sm-12">
                                <SupplierCard />
                                <SupplierCard />
                                <SupplierCard />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Suppliers