import React from 'react';
import './Contact.scss';
import SideBar from '../../components/sidebar/staticSidebar/sidebar';
import Select from 'react-select'

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' }
]
const customStyle = {
    control: (provided, styles) => ({
        ...provided,
        margin: 0,
        maxHeight: "4.6rem",
        marginLeft: 0,
        fontSize: "1.6rem",
        backgroundColor: 'white',
        outline: 'none',
        border: `transparent`,
        boxShadow: 'none',
        "&:hover": {
            borderColor: "transparent",
        }
    }),

    valueContainer: (provided, state) => ({
        ...provided,
        padding: '0 2.4rem'
    }),
    placeholder: (defaultStyles) => {
        return {
            ...defaultStyles,
            color: '#666666',
        }
    }
}

const Contact = () => {
    return (
        <div className="contact">
            <div className="container">
                <div className="row">
                    <div className="xl-2 lg-2 md-2 sm-12">
                        <SideBar />
                    </div>
                    <div className="xl-3 lg-3 md-3 sm-12 df fdc jcsb">
                        <div className="contact-head">
                            <h5 className="title">
                                Get in touch with us
                            </h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        </div>
                        <div className="contact-bottom">
                            <a href="https://www.facebook.com/" className="title">
                                support@turkeysuppliers.com
                            </a>
                            <div className='df aic jcsb div-a'>
                                <a href="https://www.facebook.com/">LINKEDIN</a>
                                <a href="https://www.facebook.com/">FACEBOOK</a>
                            </div>
                        </div>
                    </div>
                    <div className="xl-1"></div>
                    <div className="xl-6 lg-6 md-6 sm-12">
                        <form action="">
                            <div className="input-group resp-display-flex">
                                <div className="input-child">
                                    <label htmlFor="">Name</label>
                                    <input type="text" name="contact" id="" placeholder='John Doe' className="input-class" />
                                </div>
                                <div className="input-child">
                                    <label htmlFor="">Name</label>
                                    <input type="mail" name="contact" id="" placeholder="johndoe@gmail.com" className="input-class" />
                                </div>
                            </div>

                            <div className="input-group">
                                <div className="input-child">
                                    <label htmlFor="">Subject</label>

                                    <Select className="selectbox" options={options} isSearchable={false} styles={customStyle} placeholder="Select topic"
                                        components={{ IndicatorSeparator: () => null }}
                                    />
                                </div>
                            </div>
                            <div className="input-group">
                                <div className="input-child">
                                    <label htmlFor="">Message</label>
                                    <textarea name="contact" id="" placeholder="Tell us about issue" className="input-class"></textarea>
                                </div>
                            </div>
                            <div className="input-child df jcfe">
                                <button className="black-button">
                                    Send
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Contact;