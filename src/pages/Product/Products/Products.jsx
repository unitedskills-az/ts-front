import React, { useEffect, useState } from "react";
import "./Products.scss";
import Filter from "../../../components/filter/productPageFilter";
import BreadCrumb from "../../../components/breadCrumb/breadCrumb";
import MultyFilter from "../../../components/filter/productsFilter";
import TopFilter from "../../../components/filter/topFilter";
import ProductCard from "../../../components/productCard";
import { ReactComponent as Grid } from "../../../assets/grid.svg";
import { useGetProducts } from "../../Account/Suppliers/common/hooks";
import { useLocation } from "react-router-dom";

function useQueryData() {
  return new URLSearchParams(useLocation().search);
}

const Products = () => {
  const query = useQueryData();
  const keyWord = query.get("keyword");
  const category = query.get("category");

  const [stateParams, setStateParams] = useState([]);

  useEffect(() => {
    const params = {
      keyword: keyWord,
    };
    setStateParams([]);
    for (const key in params) {
      setStateParams((prev) => [...prev, `${key}=${params[key]}`]);
    }
  }, [keyWord]);

  const { dataProducts, isLoadingProducts } = useGetProducts(
    category === null
      ? keyWord !== null
        ? stateParams.toString()
        : ""
      : `category=${category}`,
    0
  );

  const [state, setState] = useState("");
  const toggleAccordion = () => {
    setState(state === "" ? "xl-12 lg-12 md-12 sm-12 grid-row" : "");
  };

  return (
    <div className="products-page">
      <div className="container">
        <div className="df aic jcsb">
          <BreadCrumb />
          <button onClick={toggleAccordion} className="button-grid">
            <Grid />
          </button>
        </div>
        <div className="row">
          <div className="xl-3 lg-3 md-3 sm-12">
            <Filter />
            <MultyFilter />
          </div>
          <div className="xl-9 lg-9 md-9 sm-12">
            <TopFilter />
            <div className="row">
              {isLoadingProducts === false &&
                dataProducts !== undefined &&
                dataProducts.data.map((item) => (
                  <div
                    className={`xl-4 lg-4 md-4 sm-12 ${state}`}
                    key={item.id}
                  >
                    <ProductCard data={item} />
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Products;
