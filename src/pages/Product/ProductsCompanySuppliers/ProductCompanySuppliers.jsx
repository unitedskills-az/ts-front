import React, { useState } from "react";
import "../Products/Products.scss";
import Filter from "../../../components/filter/productPageFilter";
import BreadCrumb from "../../../components/breadCrumb/breadCrumb";
import MultyFilter from "../../../components/filter/productsFilter";
import TopFilter from "../../../components/filter/topFilter";
import ProductCard from "../../../components/productCard";
import { ReactComponent as Grid } from "../../../assets/grid.svg";
import { useSupplierProduct } from "../../Account/Suppliers/common/hooks";

const Products = () => {
  const [page] = useState(0);

  const { supplierProduct, isLoadingSupplierProduct } = useSupplierProduct(
    "",
    page
  );

  const [state, setState] = useState("");
  const toggleAccordion = () => {
    setState(state === "" ? "xl-12 lg-12 md-12 sm-12 grid-row" : "");
  };

  return (
    <div className="products-page">
      <div className="container">
        <div className="df aic jcsb">
          <BreadCrumb />
          <button onClick={toggleAccordion} className="button-grid">
            <Grid />
          </button>
        </div>
        <div className="row">
          <div className="xl-3 lg-3 md-3 sm-12">
            <Filter />
            <MultyFilter />
          </div>
          <div className="xl-9 lg-9 md-9 sm-12">
            <TopFilter />
            <div className="row">
              {isLoadingSupplierProduct === false &&
                supplierProduct !== undefined &&
                supplierProduct.data.map((item) => (
                  <div
                    className={`xl-4 lg-4 md-4 sm-12 ${state}`}
                    key={item.id}
                  >
                    <ProductCard data={item} />
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Products;
