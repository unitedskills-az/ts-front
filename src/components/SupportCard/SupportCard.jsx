import React from 'react';

const SupportCard = () => {
    return (
        <div className="supportCard">
            <div className="df fdc">
                <h6 className="title">
                    Subject
                </h6>
                <p>
                    Product Details
                </p>
            </div>
            <div className="df fdc">
                <h6 className="title">
                    Ticket ID
                </h6>
                <p>
                    #98765421
                </p>
            </div>
            <div className="df fdc">
                <h6 className="title">
                    Ticket Date
                </h6>
                <p>
                    03.21.2021
                </p>
            </div>
            <div className="df fdc">
                <h6 className="title">
                    Ticket title
                </h6>
                <p>
                    “Error message while register”
                </p>
            </div>
            <button className="support-button">
                ticket details
            </button>
        </div>
    )
}

export default SupportCard
