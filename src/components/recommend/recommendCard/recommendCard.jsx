import React from 'react';
import './recommendCard.scss';
import recommendImg from '../../../assets/categroy.png'
const recommendCard = () => {
    return (
        <div className="recommendCard">
            <div className="recommendCard-img">
                <img src={recommendImg} alt="" />
            </div>
            <div className="recommend-inner">
                <h4 className="markpro font-s-normal font-w-normal font-14 line-h-125_ c-dark-gray">
                    10.1 Inch Touch Screen Octa Deca Core Android Tablet...
                </h4>
                <div className="recommend-price">
                    <span>
                        US$105
                    </span> / Piece
                </div>

                <p className="markpro font-s-normal font-w-normal font-14 line-h-125_ c-gray">Min Order: 100 Piece(s)</p>
            </div>
        </div>
    )
}
export default recommendCard