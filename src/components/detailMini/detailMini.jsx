import React from 'react'

const detailMini = () => {
    return (
        <div className="detail-content-child">
            <span>Business Type</span>
            <span>Manufacturer, Association, Agent</span>
        </div>
    )
}

export default detailMini
