import React from 'react'
import { NavLink } from 'react-router-dom'
const sidebar = () => {
    return (
        <div>
            <div className="sidebar-static-page">
                <NavLink to="/about" activeClassName="active">About Us</NavLink>
                <NavLink to="/terms" activeClassName="active">Term & <br /> Conditions</NavLink>
                <NavLink to="/contact" activeClassName="active">Contact</NavLink>
            </div>

        </div>
    )
}
export default sidebar