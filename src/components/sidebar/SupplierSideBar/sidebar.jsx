import React from 'react';
import { NavLink } from 'react-router-dom';
const Sidebar = () => {
    return (
        <div className="inner-sidebar">
            <NavLink to="/supplier/dashboard" activeClassName='active'>Dashboard</NavLink>
            <NavLink to="/supplier/company-info" activeClassName='active'>Company info</NavLink>
            <NavLink to="/supplier/messages" className="deactive" activeClassName='active'>Messages</NavLink>
            <NavLink to="/supplier/orders" className="deactive" activeClassName='active'>Orders</NavLink>
            <NavLink to='/supplier/products' className="deactive" activeClassName="active">Products</NavLink>
            <NavLink to="/supplier/rfq" className="deactive" activeClassName='active '>RFQ Inquiries</NavLink>
            <NavLink to="/supplier/training-center" className="deactive" activeClassName='active'>Training Center</NavLink>
            <NavLink to="/supplier/analytics" className="deactive" activeClassName='active'>Analytics</NavLink>
            <NavLink to="/supplier/support" className="deactive" activeClassName='active'>Support</NavLink>
        </div>
    )
}
export default Sidebar;