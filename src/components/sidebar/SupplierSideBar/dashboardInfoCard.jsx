import React from 'react'
import Img from '../../../assets/aboutlogo.png';
import { ReactComponent as Rating } from '../../../assets/rating.svg';
import { ReactComponent as Logout } from '../../../assets/logout.svg';

import { Link } from 'react-router-dom';
const dashboardInfoCard = () => {
    return (
        <div className='dashboard-info-card'>
            <div className="dashboard-info-card-header">
                <div className="img-container">
                    <img src={Img} alt="" />
                </div>
                <div className="dashboard-info-right">
                    <h6 className="title">
                        Wmt Cnc Co. hj  hjvb bhgvh
                    </h6>
                    <div className="rating-section">
                        <Rating />
                        <Rating />
                        <Rating />
                        <Rating />
                        <Rating />
                    </div>
                    <Link to='/' className="website">
                        view website
                    </Link>
                </div>
                <div className="gradient-right"></div>
            </div>
            <div className="dashboard-info-card-body">
                <Link to="/" className="edit">
                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect width="22" height="22" rx="6" fill="#A5B6D0" />
                        <path d="M13.8214 5.95958C13.9671 5.81387 14.1401 5.69829 14.3304 5.61944C14.5208 5.54059 14.7248 5.5 14.9309 5.5C15.137 5.5 15.341 5.54059 15.5314 5.61944C15.7217 5.69829 15.8947 5.81387 16.0404 5.95958C16.1861 6.10528 16.3017 6.27826 16.3806 6.46863C16.4594 6.659 16.5 6.86304 16.5 7.0691C16.5 7.27515 16.4594 7.47919 16.3806 7.66956C16.3017 7.85993 16.1861 8.03291 16.0404 8.17861L8.55117 15.6679L5.5 16.5L6.33214 13.4488L13.8214 5.95958Z" stroke="white" stroke-width="1.2" stroke-linecap="round" strokeLinejoin="round" />
                    </svg>
                    Edit Profile
                    <span></span>
                </Link>
                <Link to="" className="logout-button">
                    <Logout />
                    Log out
                </Link>
                <Link to="/supplier/verify-account" className="verify-account-button">
                    <span></span>
                    Verify Account
                </Link>
                <div className="sub-status">
                    <h5>
                        Subscribtion status
                    </h5>
                    <p>Start date:  <span>01.12.2021</span></p>
                    <p>End date: <span>03.12.2023</span></p>
                </div>
                <button className="yellow-button dashboard-new-product">
                    Add New Product
                </button>
            </div>

        </div>
    )
}

export default dashboardInfoCard
