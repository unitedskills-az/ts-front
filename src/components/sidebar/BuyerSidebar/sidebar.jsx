import React from "react";
import { NavLink } from "react-router-dom";
const Sidebar = () => {
  return (
    <div className="inner-sidebar">
      <NavLink to="/buyer/dashboard" activeClassName="active">
        Dashboard
      </NavLink>
      <NavLink to="/buyer/account" activeClassName="active">
        Account
      </NavLink>
      <NavLink to="/buyer/messages" activeClassName="active">
        Messages
      </NavLink>
      <NavLink to="/buyer/orders" activeClassName="active">
        Orders
      </NavLink>
      <NavLink to="/buyer/rfq" activeClassName="active">
        RFQ Inquiries
      </NavLink>
      <NavLink to="/buyer/favorites" activeClassName="active">
        Favorites
      </NavLink>
      <NavLink to="/buyer/about" activeClassName="active">
        Support
      </NavLink>
    </div>
  );
};
export default Sidebar;
