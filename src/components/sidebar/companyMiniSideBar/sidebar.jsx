import React from "react";
import { NavLink } from "react-router-dom";
import Storage from "../../../services/storage";

const sidebar = ({ companySlug }) => {
  console.log(companySlug);

  return (
    <div>
      <div className="product-page-left-details">
        <NavLink
          to={`/about-company/${Storage.get("subDomain")}`}
          activeClassName="active"
        >
          About Company
        </NavLink>
        <NavLink
          to={`/products/${Storage.get("subDomain")}/suppliers`}
          activeClassName="active"
        >
          Products
        </NavLink>
      </div>
    </div>
  );
};
export default React.memo(sidebar);
