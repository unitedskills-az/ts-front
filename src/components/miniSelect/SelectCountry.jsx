import React from 'react';
import { ReactComponent as ArrowDown } from '../../assets/arrowdown.svg';

const SelectCountry = () => {
    return (
        <div className="selectContry">
            Country
            <ArrowDown />
        </div>
    )
}

export default SelectCountry
