import React, { useState } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import renderHTML from "react-render-html";
import "./Product.scss";
const ProductCard = ({ data }) => {
  const [state, setState] = useState("");
  const toggleAccordion = () => {
    setState(state === "" ? "active" : "");
  };

  return (
    <div className="product-card">
      <div className="product-card-img">
        {!!data.img && <img src={data.img.path} alt="" />}
      </div>
      <div onClick={toggleAccordion} className="edit-button">
        <svg
          width={18}
          height={4}
          viewBox="0 0 18 4"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <ellipse
            cx="16.0149"
            cy={2}
            rx="1.98507"
            ry={2}
            fill="#2D2D38"
            fillOpacity="0.5"
          />
          <ellipse
            cx="9.0002"
            cy={2}
            rx="1.98507"
            ry={2}
            fill="#2D2D38"
            fillOpacity="0.5"
          />
          <ellipse
            cx="1.98507"
            cy={2}
            rx="1.98507"
            ry={2}
            fill="#2D2D38"
            fillOpacity="0.5"
          />
        </svg>
      </div>
      <div className={`edit-content ${state}`}>
        <Link>Edit</Link>
        <button>Delete</button>
      </div>
      <div>
        <h4 className="title">
          <Link to={`/${data.name}/product-detail/${data.slug}`}>
            {data.name}
          </Link>
        </h4>
        <div className="product-detail">
          <div>
            <div className="price">
              <span>{data.price}</span> / Piece
            </div>
            <p className="order-detail">
              Min Order: <span>{data.min_order} Piece(s)</span>
            </p>
            {data.description !== null && renderHTML(data.description)}
            <Link
              to={`/about-company/${data.supplier.sub_domain}`}
              className="company-name"
            >
              {data.name}
            </Link>
          </div>

          <Link
            to={`/${data.name}/product-detail/${data.slug}`}
            className="company-name"
          >
            Product Details
          </Link>
        </div>
      </div>
    </div>
  );
};

ProductCard.propTypes = {
  data: PropTypes.object.isRequired,
};
export default React.memo(ProductCard);
