import React from "react";
import "./aboutCompany.scss";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
import { Link } from "react-router-dom";
const aboutCompany = ({ data, title, text }) => {
  return (
    <div className="detail">
      <h5 className="title">{title}</h5>
      <p>{text}</p>
      <Swiper
        id="aboutCompany"
        tag="section"
        wrapperTag="ul"
        loop={false}
        spaceBetween={24}
        slidesPerView={3.8}
        onSlideChange={(swiper) => {
          console.log("Slide index changed to: ", swiper.activeIndex);
        }}
        breakpoints={{
          1199: {
            slidesPerView: 3.8,
          },
          991: {
            slidesPerView: 3.2,
          },
          768: {
            slidesPerView: 2.5,
          },

          320: {
            slidesPerView: 1.5,
          },
        }}
      >
        {data !== undefined &&
          data.map((item) => (
            <SwiperSlide key={item.id}>
              <Link to={"/"}>
                <img src={item.path} alt="logo" />
              </Link>
            </SwiperSlide>
          ))}
      </Swiper>
    </div>
  );
};
export default aboutCompany;
