import React from "react";
import "./Detail.scss";
import DetailMini from "../../detailMini/detailMini";
const Detail = ({ data }) => {
  return (
    <div className="detail">
      <h5 className="title">Company Operational Details</h5>
      <div className="detail-content">
        <DetailMini />
        <DetailMini />
        <DetailMini />
        <DetailMini />
        <DetailMini />
      </div>
    </div>
  );
};
export default Detail;
