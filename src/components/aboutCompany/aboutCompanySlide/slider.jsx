import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
import { Link } from "react-router-dom";
const slider = ({ data }) => {
  return (
    <div>
      <Swiper
        id="about"
        tag="section"
        wrapperTag="ul"
        loop={false}
        spaceBetween={24}
        slidesPerView={3.8}
        onSlideChange={(swiper) => {
          console.log("Slide index changed to: ", swiper.activeIndex);
        }}
        breakpoints={{
          1199: {
            slidesPerView: 3.8,
          },
          991: {
            slidesPerView: 3.2,
          },
          768: {
            slidesPerView: 2.5,
          },

          320: {
            slidesPerView: 2.2,
          },
        }}
      >
        {data !== undefined &&
          data.map((item) => (
            <SwiperSlide key={item.id}>
              <Link to={"/"}>
                <img src={item.path} alt="logo" />
              </Link>
            </SwiperSlide>
          ))}
      </Swiper>
    </div>
  );
};
export default slider;
