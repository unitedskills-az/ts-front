import React from 'react';
import './rfqCard.scss';
import RfqImg from '../../../assets/aboutlogo.png';
// import { Link } from 'react-router-dom';
import ButtonImg from '../../../assets/buttonmail.png'
const RfqCard = () => {
    return (
        <div className="rfqCard">
            <div className="df aifs mr-48">
                <div className="img-container">
                    <img src={RfqImg} alt="" />
                </div>
                <div className="df fdc">
                    <h4>
                        Zhangjiagang Joy
                    </h4>
                    <p className='mb-30'>Baku, Azerbaijan</p>
                    <p>21.09.2021 <span>14:08</span></p>
                </div>
            </div>
            <div className="df fdc w-70_">
                <div className="df aifs jcsb mb-32 ">
                    <div className="rfq-details">
                        <span>
                            Product Category
                        </span>
                        <h6>
                            Machinery for Food, Beverage & Cereal
                        </h6>
                    </div>
                    <div className="rfq-details">
                        <span>
                            Purchase Quantity
                        </span>
                        <h6 className="font-w-bold">
                            12000 <span> / pieces</span>
                        </h6>
                    </div>
                    <div className="rfq-details">
                        <span>
                            Price
                        </span>
                        <h6 className="font-w-bold c-red">
                            USD 17.59 <span> / pieces</span>
                        </h6>
                    </div>
                </div>
                <h4>
                    “Memory Card for Class10”
                </h4>
                <p className="mb-0 df aife">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took type...
               
                    <span className="read-more">
                        read more
                    </span>
                </p>
                <div className="df aic mt-16">
                    <button className="class-download">
                        <span></span>
                        Download attachment
                    </button>
                    <button className="class-contact">
                        <img src={ButtonImg} alt=""/>
                        Contact
                    </button>
                </div>
            </div>
        </div>
    )
}

export default RfqCard
