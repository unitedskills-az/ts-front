import React from 'react';
import './favCard.scss';
import ProductImg from '../../assets/product.png';
import { Link } from 'react-router-dom';
import { ReactComponent as Heart } from '../../assets/heart.svg';
const favCard = () => {
    return (
        <div className="product-card">
            <div className="product-card-img">
                <img src={ProductImg} alt="" />
            </div>
            <div className="fav-card-position">
                <h4 className="title">Hot Selling Full Capacity
                Memory Card for Class10 2GB-512GB Micro SD Card</h4>
                <div className="product-detail">
                    <div>
                        <div className="price">
                            <span>
                                US$105
                                </span> / Piece
                        </div>
                        <p className="order-detail">Min Order: <span>100 Piece(s)</span></p>
                        <p className="description">
                            Features: CNC lathes features a cast mono-block,slant bed design and has configurable tooling options.
                        </p>
                        <Link to="" className="company-name">
                            Wmt Cnc Industrial
                        </Link>
                    </div>
                    <div className="heart">
                        <Heart />
                    </div>

                    <button>Contact</button>
                </div>

            </div>
        </div>
    )
}
export default favCard