import React from "react";
import "./productDetail.scss";
import { Link } from "react-router-dom";
const ProductDetail = ({ data }) => {
  return (
    <div className="product-detail-content">
      <div className="product-detail-inner">
        <h4>{data.name}</h4>
      </div>
      <div className="product-detail-full mb-32">
        <div>
          <h5>Min. Order</h5>
          <p>{data.min_order}</p>
        </div>
        <div>
          <h5>Reference FOB Price</h5>
          <p>
            <span>{data.price}</span> / Piece
          </p>
        </div>
      </div>
      <div className="product-detail-inner">
        <div>
          <p>Category:</p>
          <span>{data.category}</span>
        </div>
        <div>
          <p>Production Capacity:</p>
          <span>10 Sets/Month</span>
        </div>
        <div>
          <p>Payment Terms:</p>
          <span>L/C, T/T, Western Union, Paypal</span>
        </div>
        <div className="line"></div>
        <div>
          <p>After-sales Service:</p>
          <span>Online Support</span>
        </div>
        <div>
          <p>Application: </p>
          <span>Metal</span>
        </div>
        <div>
          <p>Process Usage:</p>
          <span>Metal-Cutting CNC Machine Tools</span>
        </div>
        <div>
          <p>Movement Method: </p>
          <span>Linear Control</span>
        </div>
        <Link to="/contact" className="yellow-button detail-button-contact">
          Contact Supplier
        </Link>
      </div>
    </div>
  );
};

export default ProductDetail;
