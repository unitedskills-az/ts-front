import React from "react";
import "./HeaderBottom.scss";
import { Link } from "react-router-dom";
import { useCategory } from "../../../pages/Account/Suppliers/common/hooks";

// const DropDownContainer = styled("div")``;
// const DropDownHeader = styled("div")``;
// const DropDownListContainer = styled("div")``;
// const DropDownList = styled("ul")``;
// const ListItem = styled("li")``;

const HeaderBottom = () => {
  // const [isOpen, setIsOpen] = useState(false);
  // const toggling = () => setIsOpen(!isOpen);
  // const [isOpen2, setIsOpen2] = useState(false);
  // const toggling2 = () => setIsOpen2(!isOpen2);

  const { category, isLoadingCategory } = useCategory();

  return (
    <>
      <div className="header-bottom">
        <div className="container">
          <ul className="df aic  w-100_ mb-0 jcsb">
            {isLoadingCategory === false &&
              category !== undefined &&
              category.data.map((item) => (
                <li key={item.id}>
                  <Link to={`/products?category=${item.slug}`}>
                    {item.name}
                  </Link>
                </li>
              ))}

            {/* <DropDownContainer className="menu-ul">
            <DropDownHeader className="menu-header" onClick={toggling}>
              EXTERNAL PAGES
              <svg
                width="12"
                height="7"
                viewBox="0 0 12 7"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M1 1L6 6L11 1"
                  stroke="#363636"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </DropDownHeader>
            {isOpen && (
              <DropDownListContainer className="menu-ul-child">
                <DropDownList>
                  <ListItem>
                    <Link to={mainRoutes.login}>login</Link>
                  </ListItem>
                  <ListItem>
                    <Link to={mainRoutes.register}>register</Link>
                  </ListItem>
                  <ListItem>
                    <Link to={mainRoutes.resetPassword}>reset password</Link>
                  </ListItem>
                  <ListItem>
                    <Link to="/products">products</Link>
                  </ListItem>
                  <ListItem>
                    <Link to="/product-detail">product detail</Link>
                  </ListItem>
                  <ListItem>
                    <Link to="/about-company/Test1">about company</Link>
                  </ListItem>
                  <ListItem>
                    <Link to="/product-page"> company product page</Link>
                  </ListItem>
                  <ListItem>
                    <Link to="/rfq">rfq</Link>
                  </ListItem>
                  <ListItem>
                    <Link to="/suppliers">suppliers</Link>
                  </ListItem>
                </DropDownList>
              </DropDownListContainer>
            )}
          </DropDownContainer> */}
          </ul>
        </div>
      </div>
    </>
  );
};
export default HeaderBottom;
