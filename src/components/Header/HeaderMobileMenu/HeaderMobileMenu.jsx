import React, { useState } from 'react'
import CategoryMenu from '../../category/categoryHometop';
import { ReactComponent as Mobile } from '../../../assets/menu-button.svg';
const HeaderMobileMenu = () => {
    const [state, setState] = useState("");
    const toggleAccordion = () => {
        setState(state === "" ? "active" : "");
    }
    return (
        <>
            <button type="button" onClick={toggleAccordion} className="mobile-button-head desk-none">
                <Mobile />
            </button>
            <div className={`header-bottom-left-child ${state}`}>
                <div className="mobile-menu">
                    <CategoryMenu />
                </div>
            </div>
        </>
    )
}

export default HeaderMobileMenu
