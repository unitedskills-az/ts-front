import React, { useState } from "react";
import { Link } from "react-router-dom";
import Logo from "../../../assets/logo2.svg";
import Search from "../../Search";
import Request from "../../../assets/request.png";
import Account from "../../../assets/account.svg";
import "./HeaderMiddle.scss";
import { connect } from "react-redux";
import { CHANGE__ACCOUNT__TYPE } from "../../../pages/Authentication/common/redux/actionTypes";
import MobileAccountMenu from '../../../components/Header/MobileAccountMenu'
import CloseButton from '../../../assets/close.svg'
import MobileLogoutMenu from '../MobileLogoutMobile'
const HeaderMiddle = ({ isAuth }) => {
  const [state, setState] = useState("");
  const toggleAccount = () => {
    setState(state === "" ? "active" : "");
  }
  const isLogout = false
  return (

    <div className="header-middle">
      {isLogout ?
        <div className={`mobile-account-menu-parent ${state}`}>
          <MobileAccountMenu />
        </div>
        :
        <div className={`mobile-account-menu-parent ${state}`}>
          <MobileLogoutMenu />
        </div>
      }
      <div className="row py-12">
        <div className="xl-2 lg-2 md-2 sm-12">
          <Link to="/" className="logo">
            <img src={Logo} alt="" />
          </Link>
        </div>
        <div className="xl-6 lg-6 md-6 sm-12 h-100_ df aic">
          <Search />
        </div>
        <div className="xl-1"></div>
        <div className="xl-2 lg-2 md-2 sm-12 h-100_ df aic jcc">
          <Link
            to={"/login"}
            className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro"
          >
            <img src={Request} alt="" />
            Post your request
          </Link>
        </div>
        <div className="xl-1 lg-1 md-1 sm-12h-100_ df aic mobile-position-account">
          <Link
            to={isAuth ? "/buyer/account" : "/login"}
            className="account-button resp-none"
          >
            <img src={Account} alt="" />
            <span>
              Account
            </span>
          </Link>
          <button type="button" onClick={toggleAccount} className={`account-button desk-none ${state}`}>
            <img src={Account} alt="" />
            <img src={CloseButton} alt="close button" className="close-button" />
          </button>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    accountTypeState: state.accountType,
    isAuth: state.isAuth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAccount: (type) =>
      dispatch({ type: CHANGE__ACCOUNT__TYPE, payload: type }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMiddle);
