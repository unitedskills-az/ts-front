// partials
import HeaderTop from "./HeaderTop";
// logout
import HeaderMiddle from "./HeaderMiddle";
// login
import HeaderMiddleLogin from "./HeaderMiddleLogin";
import HeaderMiddleBuyer from "./HeaderMiddleBuyer";
import HeaderMiddleSupplier from "./HeaderMiddleSupplier";
import { connect } from "react-redux";
import { useLocation } from "react-router-dom";
import HeaderMobileMenu from './HeaderMobileMenu'
const Header = ({ accountType, isAuth }) => {
  const { pathname } = useLocation();

  const pathSplit = pathname.split("/")[1];

  return (
    <div className="header">
      <div className="container">
        <HeaderTop />
        {!isAuth && <HeaderMiddle />}
        {isAuth && pathSplit !== "buyer" && pathSplit !== "supplier" && (
          <HeaderMiddleLogin checkedStatus={false} />
        )}
        {isAuth && accountType === "buyers" && pathSplit === "buyer" && (
          <HeaderMiddleBuyer />
        )}
        {isAuth && accountType === "suppliers" && pathSplit === "supplier" && (
          <HeaderMiddleSupplier />
        )}
        <div className="header-menu-left">
          <HeaderMobileMenu />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    accountType: state.accountType,
    isAuth: state.isAuth,
  };
};

export default connect(mapStateToProps)(Header);
