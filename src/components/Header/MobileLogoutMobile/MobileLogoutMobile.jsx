import React from 'react';
import '../MobileAccountMenu/MobileAccountMenu.scss';
import { Link } from 'react-router-dom';
import Request from "../../../assets/request.png";
import Register from '../../../assets/register.svg';
import { ReactComponent as ArrowDown } from '../../../assets/arrowdown.svg';

const MobileLogoutMobile = () => {
    return (
        <div className="mobile-account-menu">
            <div className="df fdc">
                <Link to="/login" className="logout-menu-a signin-button">
                    Sign in
                </Link>
                <Link to="/register" className="logout-menu-a signup-button">
                    Sign Up
                    <img src={Register} alt="registere icon" />
                </Link>
                <Link to={"/login"}
                    className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro mb-32">
                    <img src={Request} alt="request icon" />
                    Request For Quote
                </Link>
                <Link to="" className="mobile-account-a">
                    Support
                </Link>
                <Link to="" className="mobile-account-a">
                    Settings
                </Link>
                <div className="menu-account-inner-mobile">
                    <Link className="mobile-account-a">
                        Services & Membership
                </Link>
                    <Link className="mobile-account-a">
                        Help & Community
                </Link>
                    <span className="mobile-account-a">
                        English <ArrowDown />
                    </span>
                </div>

            </div>
        </div >
    )
}

export default MobileLogoutMobile
