import { Link } from "react-router-dom";
import { ReactComponent as ArrowDown } from '../../../assets/arrowdown.svg';
import "./HeaderTop.scss";
// this is log out header
const HeaderTop = () => {
  return (
    <div className="header-top df aic jcsb py-12">
      <div className="lang df aic">
        <Link
          to=""
          className="font-s-normal font-w-normal font-14 line-h-100_ c-light-black  markpro"
        >
          English
          <ArrowDown className="ml-8" />
        </Link>
      </div>
      <div className="header-top-right df aic">
        <Link className="markpro font-s-normal font-w-normal font-14 line-h-100_ c-gray  mr-24">
          Services  & Membership
        </Link>
        <Link className="markpro font-s-normal font-w-normal font-14 line-h-100_ c-gray">
          Help & Community
        </Link>

      </div>
    </div>
  );
};

export default HeaderTop;
