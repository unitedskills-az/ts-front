import React from "react";
import "./MobileAccountMenu.scss";
import { Link } from "react-router-dom";
import Request from "../../../assets/request.png";
import DashboardImg from "../../../assets/aboutlogo.png";
import { ReactComponent as ArrowDown } from "../../../assets/arrowdown.svg";
import Switch from "react-switch";
const MobileAccountMenu = () => {
  const isSupplier = false;
  return (
    <div className="mobile-account-menu">
      <div className="df mb-24">
        <Link className="menu-dashboard-link">
          <img src={DashboardImg} alt="dashboard img" />
          John Doe
        </Link>
        <div className="header-input-checkbox">
          {isSupplier ? (
            <label htmlFor="switch">Became a Supplier</label>
          ) : (
            <div className="menu-account-toggle">
              <span className="markpro font-s-normal font-w-normal font-16 line-h-100_ c-light-black mr-8">
                Buyers
              </span>
              <Switch />
              <span className="markpro font-s-normal font-w-normal font-16 line-h-100_ c-light-black ml-8">
                Suppliers
              </span>
            </div>
          )}
        </div>
      </div>

      <Link
        to={"/login"}
        className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro mb-32"
      >
        <img src={Request} alt="" />
        Request For Quote
      </Link>
      <Link to="" className="mobile-account-a">
        Messages
        <span className="blue-span">2</span>
      </Link>
      <Link to="" className="mobile-account-a">
        Favorites
        <span className="red-span">4</span>
      </Link>
      <Link to="" className="mobile-account-a">
        Support
      </Link>
      <Link to="" className="mobile-account-a">
        Settings
      </Link>
      <div className="menu-account-inner-mobile">
        <Link className="mobile-account-a">Services & Membership</Link>
        <Link className="mobile-account-a">Help & Community</Link>
        <span className="mobile-account-a">
          English <ArrowDown />
        </span>
      </div>
    </div>
  );
};

export default MobileAccountMenu;
