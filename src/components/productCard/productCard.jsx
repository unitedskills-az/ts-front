import React from "react";
import "./productCard.scss";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import renderHTML from "react-render-html";

const ProductCard = ({ data }) => {
  return (
    <div className="product-card">
      <div className="product-card-img">
        {!!data.img && <img src={data.img.path} alt="" />}
      </div>
      <div>
        <h4 className="title">
          <Link
            to={`/${data.supplier.company_name}/product-detail/${data.slug}`}
          >
            {data.name}
          </Link>
        </h4>
        <div className="product-detail">
          <div>
            <div className="price">
              <span>{data.price}</span> / Piece
            </div>
            <p className="order-detail">
              Min Order: <span>{data.min_order} Piece(s)</span>
            </p>
            {data.description !== null && renderHTML(data.description)}
            <Link
              to={`/about-company/${data.supplier.sub_domain}`}
              className="company-name"
            >
              {data.supplier.company_name}
            </Link>
          </div>
          <button>Contact</button>
        </div>
      </div>
    </div>
  );
};

ProductCard.prototype = {
  data: PropTypes.object.isRequired,
};

export default ProductCard;
