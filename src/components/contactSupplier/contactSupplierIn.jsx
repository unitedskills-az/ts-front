import React from 'react';
import './contactSupplier.scss';
// import { Link } from 'react-router-dom';

// img
import UserImg from '../../assets/categroy.png';
import { ReactComponent as Loc } from '../../assets/loc.svg';
import { ReactComponent as Tel } from '../../assets/tel.svg';
import { ReactComponent as Mes } from '../../assets/mes.svg';
import { ReactComponent as FaceBook } from '../../assets/face.svg';
import { ReactComponent as InstaGram } from '../../assets/instagram.svg';
import { ReactComponent as Twitter } from '../../assets/tw.svg';
import { ReactComponent as Whatsapp } from '../../assets/wp.svg';
import { ReactComponent as Mail } from '../../assets/mail.svg';
import { ReactComponent as Youtube } from '../../assets/youtube.svg';

const contactSupplierIn = () => {
    return (
        <div className="contact-suppliers login">
            <div className="title">Contact Supplier</div>
            <div className="contact-about">
                <img src={UserImg} alt="" />
                <div>
                    <h5 className="subtitle">
                        Ms. Elizabet Beth

                        <span>Chat with supplier</span>
                    </h5>
                </div>
            </div>
            <div className="contact-bottom">
                <div className="contact-about-child">
                    <Loc />
                    Istanbul, Turkey
                </div>
                <div className="contact-about-child">
                    <Tel />
                    0532 000 00 00
                </div>
                <div className="contact-about-child">
                    <Mes />
                    supplierexample@gmail.com
                </div>
                <div className="social-icons">
                    <a href="https://www.facebook.com/">
                        <FaceBook />
                    </a>
                    <a href="https://www.facebook.com/">
                        <InstaGram />
                    </a>
                    <a href="https://www.facebook.com/">
                        <Twitter />
                    </a>
                    <a href="https://www.facebook.com/">
                        <Whatsapp />
                    </a>
                    <a href="https://www.facebook.com/">
                        <Mail />
                    </a>
                    <a href="https://www.facebook.com/">
                        <Youtube />
                    </a>
                </div>
            </div>
        </div>
    )
}
export default contactSupplierIn