import React from 'react';
import './contactSupplier.scss';
import UserImg from '../../assets/categroy.png';
import { Link } from 'react-router-dom';
const contactSupplierOut = () => {
    return (
        <div className="contact-suppliers logout">
            <div className="title">Contact Supplier</div>
            <div className="contact-about">
                <img src={UserImg} alt="" />
                <div>
                    <h5 className="subtitle">
                        Ms. Elizabet Beth

                        <span>online</span>
                    </h5>
                </div>
            </div>
            <div className="contact-botttom">
                <Link to="/login" className="yellow-button">
                    Login / Register to see details
                </Link>
            </div>
        </div>
    )
}
export default contactSupplierOut