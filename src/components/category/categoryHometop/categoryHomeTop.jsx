import React from "react";
import "./categoryHomeTop.scss";
import categoryDot from "../../../assets/cat.svg";
import { Link } from "react-router-dom";
import { useCategory } from "../../../pages/Account/Suppliers/common/hooks";

const CategoryHomeTop = () => {
  const { category, isLoadingCategory } = useCategory();

  return (
    <div className="categoryHomeTop">
      <div className="title font-s-normal montserrat font-w-bold font-14 line-h-125_ c-dark-gray df aic">
        <img src={categoryDot} alt="category dot" />
        CATEGORIES
      </div>
      <ul>
        {isLoadingCategory === false &&
          category !== undefined &&
          category.data.map((item) => (
            <li key={item.id}>
              <Link to={`/products/?category=${item.slug}`}>{item.name}</Link>
            </li>
          ))}
      </ul>
    </div>
  );
};

export default React.memo(CategoryHomeTop);
