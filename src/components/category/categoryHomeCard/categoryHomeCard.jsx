import React from 'react';
import { Link } from 'react-router-dom';
import './categoryHomeCard.scss';
import Category from '../../../assets/categroy.png'
const categoryHomeCard = () => {
    return (
        <div className="category-card-home">
            <Link to="/">
                <img src={Category} alt="categroy img" />
                <h5 className="title markpro font-s-normal font-w-normal font-14 line-h-100_ c-dark-gray">Electronics</h5>
            </Link>
        </div>
    )
}
export default categoryHomeCard