import React from 'react';
import OrderImg from '../../../assets/videoImg.png';
import { ReactComponent as Rating } from '../../../assets/rating.svg';
import { ReactComponent as Gray } from '../../../assets/gray.svg';
import { ReactComponent as Yellow } from '../../../assets/yellow.svg';

const OrderCard = () => {
    return (
        <div className="orderCard">
            <div className="df aic">
                <div className="img-container">
                    <img src={OrderImg} alt="order" />
                </div>
                <div className="df fdc ">
                    <h5 className="markpro font-s-normal font-w-500 font-16 line-g-100_ c-light-black mb-8">
                        Ms. Elizabet Beth
                    </h5>
                    <p className="markpro font-s-normal font-w-normal font-14 line-h-100_ c-light-gray m-0">
                        Baku, Azerbaijan
                    </p>
                </div>
            </div>
            <div className="data">
                <h5 className="markpro font-s-normal font-w-normal font-12 line-h-100_ c-light-gray mb-8">
                    Order ID
                </h5>

                <p className='markpro font-s-normal font-w-normal font-16 line-h-100_ c-light-black m-0'>987654321</p>
            </div>
            <div className="data">
                <h5 className="markpro font-s-normal font-w-normal font-12 line-h-100_ c-light-gray mb-8">
                    Order Date
                </h5>

                <p className='markpro font-s-normal font-w-normal font-16 line-h-100_ c-light-black m-0'>03.09.2021</p>
            </div>
            <div className="data">
                <h5 className="markpro font-s-normal font-w-normal font-12 line-h-100_ c-light-gray mb-8">
                    Order Date
                </h5>
                <p className='markpro font-s-normal font-w-normal font-16 line-h-100_ c-light-black m-0'>
                    <span className="markpro font-s-normal font-w-bold font-16 line-h-100_  c-red mr-8">US $105</span>
                    <span className="markpro font-s-normal font-w-normal font-14 line-h-100_ c-light-black-25">$116</span>
                </p>
            </div>
            <div className="data">
                <h5 className="markpro font-s-normal font-w-normal font-12 line-h-100_ c-light-gray mb-8">
                    Rating
                </h5>
                <div className='df aic'>
                    <Rating />
                    <Rating />
                    <Rating />
                    <Rating />
                    <Rating />
                </div>
            </div>
            <div className="df aic order-status">
                <Gray />
                <Yellow />
            </div>
        </div>
    )
}

export default OrderCard
