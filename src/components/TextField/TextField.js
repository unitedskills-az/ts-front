import pt from "prop-types";
import { useState } from "react";

import { ReactComponent as EyeIcon } from "../../assets/eye-icon.svg";

import "./TextField.scss";

const TextField = ({ type, ...rest }) => {
  const [isValueShow, setIsValueShow] = useState(false);
  const [inputType, setInputType] = useState(type);

  const handleShowClick = () => {
    setInputType("text");
    setIsValueShow(true);
  };

  const handleHideClick = () => {
    setInputType("password");
    setIsValueShow(false);
  };
  
  const Icon = () =>
    isValueShow ? (
      <EyeIcon className="text-field-icon" onClick={handleHideClick} />
    ) : (
      <EyeIcon className="text-field-icon" onClick={handleShowClick} />
    );

  return (
    <div className="input-class d-flex align-items-center justify-content-between">
      <input className="text-field" type={inputType} {...rest} />
      {type === "password" && <Icon />}
    </div>
  );
};

TextField.defaultProps = {
  type: "text",
};

TextField.propTypes = {
  type: pt.string,
};

export default TextField;
