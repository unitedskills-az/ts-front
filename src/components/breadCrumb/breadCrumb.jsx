import React from 'react';
import './breadCrumb.scss';
import { Link } from 'react-router-dom';
const breadCrumb = () => {
    return (
        <div className="breadCrumb">
            <Link to="/">
                Home
            </Link>
            <Link to="/">
                China Products
            </Link>
        </div>
    )
}
export default breadCrumb