import React from 'react'
import ChatProductImg from '../../assets/categroy.png';
import X from '../../assets/x.png'
const chatCard = (props) => {
    return (
        <div className={`chatCard ${props.className}`}>
            <div className="img-container">
                <img src={ChatProductImg} alt="chat product img" />
            </div>
            <div className='chatCard-content'>
                <h4 className="montserrat font-s-normal font-w-600 font-16 line-h-100_ c-dark-gray mb-32">100 Cotton Muslin Squares Baby 10 Pack Extra Soft Bibes</h4>
                <div className="chatCard-bottom">
                    <div>
                        <div className="title">
                            Quantity:
                        </div>
                        <p>
                            200/pieces
                        </p>
                    </div>
                    <div>
                        <div className="title">
                            Option::
                        </div>
                        <p>
                            any option
                        </p>
                    </div>
                    <div>
                        <div className="title">
                            Size
                        </div>
                        <p>
                            1-5 years
                        </p>
                    </div>
                    <div>
                        <div className="title">
                            Total:
                        </div>
                        <p>
                            <span>
                                $987654.3
                            </span>
                            <span className="yellow-border">
                                -20%
                            </span>
                        </p>
                    </div>
                </div>
                <button className='chat-delete-card'>
                    <img src={X} alt="" />
                </button>
            </div>
            <div className="amount">
                <input type="number" placeholder="20%" />
                <button>
                    <svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.97474 1.12688L1.04785 9.04518L3.09113 8.85083C3.63517 8.79909 4.0938 9.20379 4.03337 9.68228L3.3783 14.8693L8.95615 6.60419L7.06517 6.88147C6.48813 6.96609 5.98693 6.52759 6.07987 6.01944L6.97474 1.12688ZM6.42446 0.341452C6.93148 -0.335924 8.13008 0.0742604 7.98362 0.875008L7.03939 6.0374L9.0017 5.74966C9.71963 5.64439 10.2496 6.32577 9.87796 6.87638L3.97081 15.6294C3.50074 16.326 2.28322 15.9672 2.3849 15.162L3.07553 9.69351L0.949468 9.89572C0.230197 9.96414 -0.254886 9.26502 0.143011 8.73343L6.42446 0.341452Z" fill="#2D2D38" />
                    </svg>
                </button>
            </div>
        </div>
    )
}

export default chatCard
