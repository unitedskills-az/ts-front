import React, { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
const Datepicker = ({ field }) => {
  const [startDate, setStartDate] = useState(new Date());

  return field !== undefined ? (
    <DatePicker
      selected={field.value}
      className="input-class"
      onChange={(date) => field.onChange(date)}
    />
  ) : (
    <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
  );
};

export default Datepicker;
