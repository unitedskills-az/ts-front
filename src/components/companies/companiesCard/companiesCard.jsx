import React from "react";
import "./companiesCard.scss";
import { Link } from "react-router-dom";
// img
import Campanies from "../../../assets/categroy.png";
import cardIcon from "../../../assets/companies.svg";

const companiesCard = () => {
  return (
    <div className="compainiesCard">
      <Link to="/">
        <img src={Campanies} alt="campanies img" />
      </Link>
      <div className="compainiesCard-inner">
        <div className="title">
          <img src={cardIcon} alt="card icon" />
          <h4 className="markpro font-s-normal font-w-500 font-18 line-h-100_ c-dark-gray">
            Wmt Cnc Industrial Co., Ltd.
          </h4>
        </div>
        <div className="companies-card-detail">
          <div className="df aic jcsb">
            <p className="font-s-normal font-w-300 font-12 line-h-100_ c-gray  markpro">
              Business Type:
            </p>
            <span className="c-dark-gray font-s-normal font-14 line-h-100_ font-w-400 markpro">
              Trading Company
            </span>
          </div>
          <div className="df aic jcsb">
            <p className="font-s-normal font-w-300 font-12 line-h-100_ c-gray  markpro">
              Main Products:
            </p>
            <span className="c-dark-gray font-s-normal font-14 line-h-100_ font-w-400 markpro">
              Clothing
            </span>
          </div>
          <div className="df aic jcsb">
            <p className="font-s-normal font-w-300 font-12 line-h-100_ c-gray  markpro">
              Average lead time:
            </p>
            <span className="c-dark-gray font-s-normal font-14 line-h-100_ font-w-400 markpro">
              21 days
            </span>
          </div>
          <span className="font-s-normal font-w-normal font-14 line-h-100_ c-blue card-span">
            wmtcnc.turkeysuppliers.com
          </span>
        </div>
      </div>
    </div>
  );
};

export default companiesCard;
