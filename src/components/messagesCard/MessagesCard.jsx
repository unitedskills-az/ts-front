import React from 'react';
import MessageImg from '../../assets/videoImg.png';
import { Link } from 'react-router-dom';
const MessagesCard = (props) => {
    return (
        <Link to={props.link} className={`messageCard ${props.className}`}>
            <div className="df aic">
                <div className="img-container">
                    <img src={MessageImg} alt="" />
                </div>

                <h6 className="title">
                    Ms. Elizabet Beth
                </h6>
            </div>

            <p>Lorem Ipsum is simply dummy text of the printing and type...</p>

            <span>
                21.09.2021
            </span>
        </Link>
    )
}

export default MessagesCard
