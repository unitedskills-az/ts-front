import React, { useEffect, useState } from "react";
import "swiper/swiper-bundle.css";
import { Swiper, SwiperSlide } from "swiper/react";
import "./Carousel.scss";
import SwiperCore, { Navigation, Thumbs, Pagination } from "swiper";

SwiperCore.use([Navigation, Thumbs, Pagination]);

const Carousel = (data) => {
  const [thumbsSwiper, setThumbsSwiper] = useState(null);
  const [arr, setArr] = useState([]);

  useEffect(() => {
    setArr((prev) => [
      ...prev,
      data.data.img !== null ? data.data.img : "",
      ...data.data.secondaryPhotos,
    ]);
  }, [data.data.img, data.data.secondaryPhotos]);

  return (
    <div className="carousel">
      <Swiper
        style={{
          "--swiper-navigation-color": "#fff",
          "--swiper-pagination-color": "red",
        }}
        id="main"
        loop={false}
        spaceBetween={10}
        navigation={false}
        pagination={{ clickable: true }}
        thumbs={{ swiper: thumbsSwiper }}
        className="mySwiper2"
      >
        {arr.length !== 0 &&
          arr.map((item, index) => (
            <SwiperSlide item={index}>
              <img src={item.path} alt="product" />
            </SwiperSlide>
          ))}
      </Swiper>
      <Swiper
        onSwiper={setThumbsSwiper}
        loop={false}
        spaceBetween={8}
        slidesPerView={6}
        watchSlidesProgress={true}
        className="mySwiper"
        id="child"
      >
        {arr.length !== 0 &&
          arr.map((item, index) => (
            <SwiperSlide item={index}>
              <img src={item.path} alt="product" />
            </SwiperSlide>
          ))}
      </Swiper>
    </div>
  );
};

export default Carousel;
