import React from 'react';
import './filter.scss';
import { ReactComponent as Arrow } from '../../../assets/arrowinner.svg'
const Filter = () => {
    return (
        <div className='multyFilter'>
            <form action="">
                <h6 className="title">
                    Min order
                </h6>
                <label htmlFor="" className="">
                    <input type="text" placeholder="Less than" />
                    <button>
                        <Arrow />
                    </button>
                </label>
                <h6 className="title">
                    Price
                </h6>
                <label htmlFor="" className="">
                    <input type="number" placeholder="min" />
                    <input type="number" placeholder="max" />

                    <button>
                        <Arrow />
                    </button>
                </label>
                <h6 className="title">
                    Product type
                </h6>
                <label htmlFor="" className="checkbox-parent">
                    <input type="checkbox" name="filter" />
                    <div className="checkmark"></div>
                    <p>Product type 1</p>
                </label>
                <label htmlFor="" className="checkbox-parent">
                    <input type="checkbox" name="filter" />
                    <div className="checkmark"></div>
                    <p>Product type 2</p>
                </label>
                <label htmlFor="" className="checkbox-parent">
                    <input type="checkbox" name="filter" />
                    <div className="checkmark"></div>
                    <p>Product type 3</p>
                </label>
            </form>
        </div>
    )
}
export default Filter