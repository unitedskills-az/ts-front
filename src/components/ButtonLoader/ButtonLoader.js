import { Spinner } from "react-bootstrap";
import pt from "prop-types";
import cn from "classnames";

import "./ButtonLoader.sass";

const ButtonLoader = ({
  disabled,
  type,
  isLoading,
  children,
  onClick,
  ...rest
}) => {
  const classNames = cn(
    {
      "position-relative b-yellow  br-2 font-s-normal font-w-500 font-16 line-h-100_ c-light-black h-48 bnone w-100_  markpro": true,
    },
    { "b-gray": disabled || isLoading }
  );

  const handleClick = (e) => {
    e.preventDefault();
    onClick();
  };
  return (
    <button
      onClick={handleClick}
      disabled={isLoading || disabled}
      type={type}
      className={classNames}
      {...rest}
    >
      {isLoading ? (
        <Spinner className="position-absolute spinner" animation="border" />
      ) : (
        children
      )}
    </button>
  );
};

ButtonLoader.defaultProps = {
  type: "",
  isLoading: false,
};

ButtonLoader.propTypes = {
  type: pt.string,
  disabled: pt.bool,
  children: pt.oneOfType([pt.string, pt.element]),
  isLoading: pt.bool,
};

export default ButtonLoader;
