import React from "react";
import "./Search.scss";
import SearchSvg from "../../assets/search.svg";
import { useForm } from "react-hook-form";
import { useSearch } from "./common/hooks";
const Search = () => {
  const { handleSearch } = useSearch();

  const { register, handleSubmit } = useForm();
  const onSubmit = (data) => {
    handleSearch(data.keyword);
  };

  return (
    <div className="w-100_">
      <form
        className="input-group-header df aic"
        onSubmit={handleSubmit(onSubmit)}
      >
        <input
          type="text"
          placeholder="Search products or suppliers"
          {...register("keyword", { required: true })}
        />
        <div className="search-select">
          buyer
          <svg
            width="8"
            height="5"
            viewBox="0 0 8 5"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M1 1L4 4L7 1"
              stroke="#363636"
              strokeOpacity="0.25"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
        </div>
        <button type="submit">
          <img src={SearchSvg} alt="search button" />
        </button>
      </form>
    </div>
  );
};
export default Search;
