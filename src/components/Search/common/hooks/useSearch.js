import useRdirect from "../../../../hooks/useRedirect";

const useSearch = () => {
  const { redirect } = useRdirect();

  const handleSearch = (key) => {
    redirect(`/products?keyword=${key}`);
  };

  return { handleSearch };
};

export default useSearch;
