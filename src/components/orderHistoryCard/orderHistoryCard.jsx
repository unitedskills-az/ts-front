import React from 'react'

const orderHistoryCard = () => {
    return (
        <div className="orderHistoryCard">
            <div className="order-title">
                <h5>
                    Order ID
                </h5>
                <span>
                    #987654321
                </span>
            </div>
            <div className="order-title">
                <h5>
                    Order date
                </h5>
                <span>
                    11.05.2021
                </span>
            </div>
            <div className="order-title">
                <h5>
                    Discount
                </h5>
                <span>
                    -9876$
                </span>
            </div>
            <div className="order-title">
                <h5>
                    Total:
                </h5>
                <span className='c-red'>
                    US $987654.3
                </span>
            </div>
            <button className="class-download class-order-download m-0">
                <span></span>
                        Download attachment
            </button>
        </div>
    )
}

export default orderHistoryCard
