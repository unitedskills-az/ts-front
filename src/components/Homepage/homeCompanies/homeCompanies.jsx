import React from "react";
import "./homeCompanies.scss";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
import CompaniesCard from "../../../components/companies/companiesCard";
const homeCompanies = () => {
  return (
    <div className="homeCompaines">
      <div className="container">
        <h5 className="font-s-normal montserrat font-w-bold font-32 line-h-100_ ">
          New Supplier companies
        </h5>
      </div>
      <Swiper
        id="category"
        tag="section"
        wrapperTag="ul"
        loop={true}
        spaceBetween={20}
        slidesPerView={4.4}
        onSlideChange={(swiper) => {
          console.log("Slide index changed to: ", swiper.activeIndex);
        }}
        breakpoints={{
          1199: {
            slidesPerView: 4.4,
          },
          991: {
            slidesPerView: 3.5,
          },
          768: {
            slidesPerView: 2.5,
          },

          320: {
            slidesPerView: 1.4,
          },
        }}
      >
        <SwiperSlide>
          <CompaniesCard />
        </SwiperSlide>
        <SwiperSlide>
          <CompaniesCard />
        </SwiperSlide>
        <SwiperSlide>
          <CompaniesCard />
        </SwiperSlide>
      </Swiper>
    </div>
  );
};
export default homeCompanies;
