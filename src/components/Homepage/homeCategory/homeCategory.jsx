import React from "react";
import "./homeCategory.scss";
import CategoryHomeCard from "../../category/categoryHomeCard";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";

const HomepageCategory = () => {
  return (
    <div className="homepagecategory">
      <Swiper
        id="category"
        tag="section"
        wrapperTag="ul"
        loop={true}
        spaceBetween={20}
        slidesPerView={5.5}
        onSlideChange={(swiper) => {
          console.log("Slide index changed to: ", swiper.activeIndex);
        }}
        breakpoints={{
          1199: {
            slidesPerView: 6.5,
          },
          991: {
            slidesPerView: 4.5,
          },
          768: {
            slidesPerView: 3.5,
          },

          320: {
            slidesPerView: 1.5,
          },
        }}
      >
        <SwiperSlide>
          <CategoryHomeCard />
        </SwiperSlide>
        <SwiperSlide>
          <CategoryHomeCard />
        </SwiperSlide>
        <SwiperSlide>
          <CategoryHomeCard />
        </SwiperSlide>
      </Swiper>
    </div>
  );
};
export default React.memo(HomepageCategory);
