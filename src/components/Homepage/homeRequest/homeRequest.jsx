import React from "react";
import "./homeRequest.scss";
import Request from "../../../assets/request.png";

const HomeRequest = () => {
  return (
    <div className="homeRequest">
      <div className="container">
        <h5 className="font-s-normal montserrat font-w-bold font-32 line-h-100_">
          Request for quote
        </h5>
        <div className="homeRequest-contain">
          <div className="homeRequest-inner row">
            <div className="homeRequest-child xl-5 lg-5 md-5 sm-12">
              <span className="font-s-normal markpro font-w-normal font-16 line-h-100% c-dark-gray mb-24 df">
                An easy way to post your sourcing requests and get quotes.
              </span>
              <div className="input-child mb-16">
                <input type="text" placeholder="Product name" />
              </div>
              <div className="input-child mb-16">
                <textarea name="" id="" placeholder="Product name"></textarea>
              </div>
              <div className="df aic jcsb resp-df-fdc">
                <div className="input-quantity">
                  <input
                    type="number"
                    placeholder="Quantity"
                    onKeyDown={(evt) => evt.key === "e" && evt.preventDefault()}
                  />
                </div>
                <button className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro">
                  <img src={Request} alt="" />
                  Post your request
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default React.memo(HomeRequest);
