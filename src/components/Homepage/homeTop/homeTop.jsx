import React from "react";
import "../homeTop/homeTop.scss";
import CategoryHomeTop from "../../../components/category/categoryHometop";
import Slider from "./slide/slider";
const homeTop = () => {
  return (
    <div className="hometop-contian">
      <div className="container">
        <div className="row">
          <div className="xl-3 lg-3 md-3 sm-12">
            <CategoryHomeTop />
          </div>
          <div className="xl-9 lg-9 md-9 sm-12 swiper-class">
            <Slider />
          </div>
        </div>
      </div>
    </div>
  );
};
export default homeTop;
