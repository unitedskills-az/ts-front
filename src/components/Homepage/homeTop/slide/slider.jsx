import React from 'react';
import { Link } from 'react-router-dom';
import Catousel1 from '../../../../assets/swipper.png'
import './slider.scss'

import SwiperCore, { Controller, Thumbs, Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";


SwiperCore.use([Controller, Thumbs, Navigation]);
function slider() {
    return (
        <div>
            <Swiper
                id="main"
                tag="section"
                wrapperTag="ul"
                navigation
                loop={true}
                spaceBetween={1}
                slidesPerView={1}
                // onSlideChange={(swiper) => {
                //     console.log("Slide index changed to: ", swiper.activeIndex);
                // }}
            >
                <SwiperSlide>
                    <Link to={"/"}>
                        <img src={Catousel1} alt="logo" />
                    </Link>
                </SwiperSlide>
                <SwiperSlide>
                    <Link to={"/"}>
                        <img src={Catousel1} alt="logo" />
                    </Link>
                </SwiperSlide>
                <SwiperSlide>
                    <Link to={"/"}>
                        <img src={Catousel1} alt="logo" />
                    </Link>
                </SwiperSlide>
            </Swiper>
        </div>
    )
}
export default slider