import React from "react";
import "./homeRecommend.scss";
import RecommendCard from "../../../components/recommend/recommendCard/recommendCard";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
const HomeRecommend = () => {
  return (
    <div className="recommend-page">
      <div className="container">
        <h5 className="font-s-normal montserrat font-w-bold font-32 line-h-100_ mb-32">
          Recommendations for you
        </h5>

        <Swiper
          id="category"
          tag="section"
          wrapperTag="ul"
          loop={true}
          spaceBetween={20}
          slidesPerView={4.4}
          onSlideChange={(swiper) => {
            console.log("Slide index changed to: ", swiper.activeIndex);
          }}
          breakpoints={{
            1199: {
              slidesPerView: 6,
            },
            991: {
              slidesPerView: 4.5,
            },
            768: {
              slidesPerView: 3.5,
            },

            320: {
              slidesPerView: 1.5,
            },
          }}
        >
          <SwiperSlide>
            <RecommendCard />
          </SwiperSlide>
          <SwiperSlide>
            <RecommendCard />
          </SwiperSlide>
          <SwiperSlide>
            <RecommendCard />
          </SwiperSlide>
        </Swiper>
      </div>
    </div>
  );
};
export default React.memo(HomeRecommend);
