import React from 'react';
import { Link } from 'react-router-dom'
function dashboard(props) {
    return (
        <div>
            <Link to={props.link} className="dashboard-card">
                <h5>
                    {props.title}
                    <span>{props.subtitle}</span>
                </h5>
                <div>
                    <img src={props.image} alt="" />
                </div>
                <h3>
                    {props.number}
                </h3>
                <p>
                    {props.desc}
                </p>
            </Link>
        </div>
    )
}

export default dashboard
