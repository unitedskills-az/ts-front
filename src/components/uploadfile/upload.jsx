import React from "react";
import "antd/dist/antd.css";
import { Upload, Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";
const upload = ({ elem, multiple }) => {
  return (
    <div className="select-class">
      <Upload multiple={multiple === undefined ? true : multiple} ref={elem}>
        <Button icon={<UploadOutlined />}>Upload</Button>
      </Upload>
    </div>
  );
};

export default upload;
