import React from 'react';
import SearchSvg from '../../assets/search.svg';


const MIniSearc = () => {
    return (
        <div className="miniSearch">
            <form className="input-group-header df aic">
                <input type="text" placeholder="search" />
                <button type="submit">
                    <img src={SearchSvg} alt="search button" />
                </button>
            </form>
        </div>
    )
}

export default MIniSearc
