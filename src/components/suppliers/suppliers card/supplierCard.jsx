import React from 'react';
import './supplierCard.scss';
import SupplierImg from '../../../assets/pro.png';
const supplierCard = () => {
    return (
        <div className="suppliers-card">
            <div className="suppliers-card-left">
                <h5 className="title markpro font-s-normal font-w-500 font-14 line-h-150_ c-dark-gray">
                    Guangzhou Xuancai International Limited
                </h5>
                <div className="supplier-card-list">
                    <div className="df aic jcsb mb-12">
                        <p>Business Type:</p>
                        <span>Manufacturer/Factory , Trading Company</span>
                    </div>
                    <div className="df aic jcsb mb-12">
                        <p>Main Products:</p>
                        <span>Clothing</span>
                    </div>
                    <div className="df aic jcsb mb-12">
                        <p>Mgmt. Certification:</p>
                        <span>ISO 9000</span>
                    </div>
                    <div className="df aic jcsb mb-12">
                        <p>City/Province:</p>
                        <span>Zhongshan, Guangdong</span>
                    </div>
                </div>
                <div className="yellow-button w-100 mt-16">
                    Contact
                </div>
            </div>
            <div className="suppliers-card-right">
                <img src={SupplierImg} alt="supplier img" />
                <img src={SupplierImg} alt="supplier img" />
                <img src={SupplierImg} alt="supplier img" />
            </div>
        </div>
    )
}

export default supplierCard;