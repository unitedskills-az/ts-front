import React from 'react';
import { Link } from 'react-router-dom';
import VideoImg from '../../assets/videoImg.png'
const trainingVideoCard = () => {
    return (
        <Link to="/" className="training-vodeo-card">
            <span>For starters as supplier</span>
            <h5>
                How to complete account details
            </h5>
            <img src={VideoImg} alt="" />
        </Link>
    )
}

export default trainingVideoCard
