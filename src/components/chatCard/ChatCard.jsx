import React from 'react';
import chatImg from '../../assets/aboutlogo.png';
import {ReactComponent as CardLeft} from '../../assets/card-left.svg';
import {ReactComponent as CardRight} from '../../assets/card-right.svg';

const chatCard = () => {
    const empty = true
    return (
        <div className="chat-card-right">
            <div className="chat-card-header">
                <div className="img-container">
                    <img src={chatImg} alt="" />
                </div>
                <div className="chat-card-header-info">
                    <h5>
                        Ms. Elizabet Beth
                    </h5>
                    <span>
                        online
                    </span>
                </div>
            </div>
            <div className="chat-card-body">
                {(empty) ? 
                <div className="chat-card-center">
                    <CardLeft/>
                    <CardRight/>
                </div>  
                :
                <div className="chat-card-body-inner">
                    <div>
                        <div className="chat-left">
                            <div className="chat-min-card">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                <span>14:08</span>
                            </div>
                        </div>
                        <div className="chat-right">
                            <div className="chat-min-card ">
                                Lorem Ipsum is simply dummy text
                                
                                <span>14:08</span>
                            </div>
                        </div>
                        <div className="chat-left">
                            <div className="chat-min-card">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
    
                                <span>14:08</span>
                            </div>
                        </div>
                        <div className="chat-right">
                            <div className="chat-min-card ">
                                Lorem Ipsum is simply dummy text
                                
                                <span>14:08</span>
                            </div>
                        </div>
                        <div className="chat-left">
                            <div className="chat-min-card">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
    
                                <span>14:08</span>
                            </div>
                        </div>
                        <div className="chat-right">
                            <div className="chat-min-card ">
                                Lorem Ipsum is simply dummy text
                                
                                <span>14:08</span>
                            </div>
                        </div>
                        <div className="chat-right">
                            <div className="chat-min-card ">
                                Lorem Ipsum is simply dummy text
                                
                                <span>14:08</span>
                            </div>
                        </div>
                    </div>
                </div>
                }
            </div>
            <div className="chat-card-bottom">
                <div className="chat-card-bottom-child">
                    <input type="text" placeholder="Message"/>
                    <button>Send</button>
                </div>
            </div>
        </div>
    )
}

export default chatCard
