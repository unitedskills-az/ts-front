import React, { useState } from "react";
import { Link } from "react-router-dom";
import Logo from "../../../assets/logo2.svg";
import Search from "../../Search";
import Request from "../../../assets/request.png";
import Account from "../../../assets/account.svg";
import Storage from "../../../services/storage";
import CloseButton from "../../../assets/close.svg";
import { useRedirect } from "../../../hooks";
import { ReactComponent as Fav } from "../../../assets/fav.svg";
import { ReactComponent as Mes } from "../../../assets/message.svg";
import "./HeaderMiddle.scss";
import { connect } from "react-redux";
import { CHANGE__ACCOUNT__TYPE } from "../../../pages/Authentication/common/redux/actionTypes";
import MobileAccountMenu from "../../../components/Header/MobileAccountMenu";
const HeaderMiddle = ({ checkedStatus, handleAccount }) => {
  const { redirect } = useRedirect();

  const handleChnage = (checked) => {
    if (checked) {
      Storage.setType("suppliers");
      handleAccount("suppliers");
    } else {
      Storage.setType("buyers");
      handleAccount("buyers");
    }
  };
  const [state, setState] = useState("");
  const toggleAccount = () => {
    setState(state === "" ? "active" : "");
  };
  return (
    <div className="header-middle">
      <div className={`mobile-account-menu-parent ${state}`}>
        <MobileAccountMenu />
      </div>
      <div className="row py-12">
        <div className="xl-2 lg-2 md-2 sm-12">
          <Link to="/" className="logo">
            <img src={Logo} alt="" />
          </Link>
        </div>
        <div className="xl-5 lg-5 md-5 sm-12 h-100_ df aic">
          <Search />
        </div>
        <div className="xl-2 lg-2 md-2 sm-12 df aic">
          <div className="header-input-checkbox">
            <label htmlFor="switch">
              <input
                checked={checkedStatus}
                onClick={(e) => {
                  if (e.target.checked) {
                    handleChnage(e.target.checked);
                    redirect("/supplier/company-info");
                  } else {
                    handleChnage(e.target.checked);
                    redirect("/buyer/dashboard");
                  }
                }}
                type="checkbox"
                id="switch"
              />
              <span className="checkmark"></span>
              Became a Supplier
            </label>
          </div>
        </div>
        <div className="xl-2 lg-2 md-2 sm-12 h-100_ df aic jcc">
          <Link
            to={"/rfq"}
            className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro"
          >
            <img src={Request} alt="" />
            Post your request
          </Link>
        </div>
        <div className="xl-1 lg-1 md-1 sm-12 h-100_ df aic jcfe  mobile-position-account">
          <Link
            onClick={() => {
              handleChnage(false);
            }}
            to="/buyer/favorites"
            className="header-class-a"
          >
            <Fav />
          </Link>
          <Link
            className="header-class-a active mes-class"
            onClick={() => {
              handleChnage(false);
            }}
            to="/buyer/messages"
          >
            <Mes />
            {/* <span>4</span> */}
          </Link>
          <Link
            onClick={() => {
              handleChnage(false);
            }}
            to="/buyer/dashboard"
            className="account-button resp-none"
          >
            <img src={Account} alt="" />
          </Link>

          <button
            type="button"
            onClick={toggleAccount}
            className={`account-button desk-none ${state}`}
          >
            <img src={Account} alt="account" />
            <img
              src={CloseButton}
              alt="close button"
              className="close-button"
            />
          </button>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    accountTypeState: state.accountType,
    isAuth: state.isAuth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAccount: (type) =>
      dispatch({ type: CHANGE__ACCOUNT__TYPE, payload: type }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMiddle);
