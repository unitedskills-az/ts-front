import React, { useState } from "react";
import { Link } from "react-router-dom";
import logo from "../../../assets/logo2.svg";
import Search from "../../Search";
import Request from "../../../assets/request.png";
import Storage from "../../../services/storage";
import Account from "../../../assets/account.svg";
import { useRedirect } from "../../../hooks";
import { ReactComponent as Fav } from "../../../assets/fav.svg";
import { ReactComponent as Mes } from "../../../assets/message.svg";
import "./HeaderMiddle.scss";
import { connect } from "react-redux";
import { CHANGE__ACCOUNT__TYPE } from "../../../pages/Authentication/common/redux/actionTypes";
import MobileAccountMenu from '../../../components/Header/MobileAccountMenu'
import CloseButton from '../../../assets/close.svg'

const HeaderMiddle = ({ accountTypeState, handleAccount }) => {
  const { redirect } = useRedirect();

  const handleChnage = (checked) => {
    if (checked) {
      Storage.setType("suppliers");
      handleAccount("suppliers");
    } else {
      Storage.setType("buyers");
      handleAccount("buyers");
    }
  };
  const [state, setState] = useState("");
  const toggleAccount = () => {
    setState(state === "" ? "active" : "");
  }
  return (
    <div className="header-middle buyer">
      <div className={`mobile-account-menu-parent ${state}`}>
        <MobileAccountMenu />
      </div>
      <div className="row py-12">
        <div className="xl-2 lg-2 md-2 sm-12 df aic">
          <Link to="/" className="logo">
            <img src={logo} alt="" />
          </Link>
        </div>
        <div className="xl-4 lg-4 md-4 sm-12 h-100_ df aic">
          <Search />
        </div>
        <div className="xl-3 lg-3 md-3 sm-12 df aic jcfe">
          <div className="header-input-checkbox">
            <label htmlFor="switch">
              <input
                type="checkbox"
                id="switch"
                onClick={(e) => {
                  if (e.target.checked) {
                    handleChnage(e.target.checked);
                    redirect("/supplier/company-info");
                  } else {
                    handleChnage(e.target.checked);
                    redirect("/buyer/dashboard");
                  }
                }}
              />
              <span className="checkmark"></span>
              Became a Supplier
            </label>
          </div>
        </div>
        <div className="xl-2 lg-2 md-2 sm-12 h-100_ df aic jcc">
          <button className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro">
            <img src={Request} alt="" />
            Request For Quote
          </button>
        </div>
        <div className="xl-1 lg-1 md-1 sm-12 h-100_ df aic jcfe mobile-position-account">
          <Link
            className="header-class-a"
            onClick={() => {
              handleChnage(false);
            }}
            to="/buyer/favorites"
          >
            <Fav />
          </Link>
          <Link
            className="header-class-a active mes-class"
            onClick={() => {
              handleChnage(false);
            }}
            to="/buyer/messages"
          >
            <Mes />
            {/* <span>4</span> */}
          </Link>
          <Link to="/buyer/dashboard" className="account-button resp-none">
            <img src={Account} alt="" />
          </Link>
          <button type="button" onClick={toggleAccount} className={`account-button desk-none ${state}`}>
            <img src={Account} alt="" />
            <img src={CloseButton} alt="close button" className="close-button" />
          </button>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    accountTypeState: state.accountType,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAccount: (type) =>
      dispatch({ type: CHANGE__ACCOUNT__TYPE, payload: type }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMiddle);
