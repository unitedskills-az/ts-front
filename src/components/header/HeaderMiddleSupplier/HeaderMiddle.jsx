import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
// import Search from "../../Search";
// import Request from "../../../assets/request.png";
import Account from "../../../assets/account.svg";
import Plus from "../../../assets/plus.png";
import Switch from "react-switch";
import SLogo from "../../../assets/buyerLogo.svg";
import Storage from "../../../services/storage";
import { useCheckedAccountType, useRedirect } from "../../../hooks";
// import { ReactComponent as Fav } from '../../../assets/fav.svg';
import { ReactComponent as Mes } from "../../../assets/message.svg";
import "./HeaderMiddle.scss";
import { connect } from "react-redux";
import { CHANGE__ACCOUNT__TYPE } from "../../../pages/Authentication/common/redux/actionTypes";
import MobileAccountMenu from "../../../components/Header/MobileAccountMenu";
import CloseButton from "../../../assets/close.svg";

const HeaderMiddle = ({ accountTypeState, isAuth, handleAccount }) => {
  const { accountType, checkedStatus } = useCheckedAccountType();
  const [checked, setChecked] = useState(checkedStatus);
  const { redirect } = useRedirect();

  const history = useHistory();

  const handleChnage = (checked) => {
    setChecked(checked);
    if (checked) {
      Storage.setType("suppliers");
      handleAccount("suppliers");
      redirect("/supplier/dashboard");
    } else {
      Storage.setType("buyers");
      handleAccount("buyers");
      redirect("/buyer/account");
    }
  };
  const [state, setState] = useState("");
  const toggleAccount = () => {
    setState(state === "" ? "active" : "");
  };
  return (
    <div className="header-middle supplier">
      <div className={`mobile-account-menu-parent ${state}`}>
        <MobileAccountMenu />
      </div>
      <div className="row py-12">
        <div className="xl-2 lg-2 md-2 sm-12 df aic">
          <Link to="/" className="buyer-header-arrow">
            <svg
              width="33"
              height="24"
              viewBox="0 0 33 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect
                width="33"
                height="24"
                rx="6"
                transform="matrix(1 0 0 -1 0 24)"
                fill="white"
              />
              <rect
                x="0.5"
                y="-0.5"
                width="32"
                height="23"
                rx="5.5"
                transform="matrix(1 0 0 -1 0 23)"
                stroke="#2D2D38"
                stroke-opacity="0.1"
              />
              <path
                d="M21 12L13 12"
                stroke="#2D2D38"
                stroke-width="1.2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M16.1094 8L12.0007 12.0003L16.1094 16"
                stroke="#2D2D38"
                stroke-width="1.2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </Link>
          <Link to="/" className="logo">
            <img src={SLogo} alt="" />
          </Link>
        </div>
        <div className="xl-3 lg-3 md-3 sm-12 h-100_ df aic"></div>
        <div className="xl-2 lg-2 md-2 sm-12 df aic jcfe">
          <div className="header-input-checkbox resp-none">
            <label
              htmlFor="switch"
              onClick={() => {
                if (
                  Storage.get("subDomain") !== null &&
                  Storage.get("subDomain") !== ""
                ) {
                  history.push({
                    pathname: `/about-company/${Storage.get("subDomain")}`,
                  });
                }
              }}
            >
              <input
                type="checkbox"
                id="switch"
                disabled={
                  Storage.get("subDomain") !== null &&
                    Storage.get("subDomain") !== ""
                    ? false
                    : true
                }
              />
              <span className="checkmark"></span>
              visit website
            </label>
          </div>
        </div>
        <div className="xl-4 lg-4 md-4 sm-12 h-100_ df aic jcc">
          <Link
            to={"/supplier/add-product"}
            className="header-post-request font-s-normal font-w-normal font-14 line-h-100_ c-black markpro"
          >
            <img src={Plus} alt="" />
            Add product
          </Link>

          {accountType !== "account" && (
            <div
              class="switch-toggle"
              style={{
                margin: "0 30px",
                display: "flex",
                alignItems: "center",
              }}
            >
              <span className="markpro font-s-normal font-w-normal font-16 line-h-100_ c-light-black mr-8">
                Buyers
              </span>
              <Switch onChange={handleChnage} checked={checked} />
              <span className="markpro font-s-normal font-w-normal font-16 line-h-100_ c-light-black ml-8">
                Suppliers
              </span>
            </div>
          )}
        </div>
        <div className="xl-1 lg-1 md-1 sm-12 h-100_ df aic jcfe mobile-position-account">
          <Link className="header-class-a  mes-class active">
            <Mes />
            {/* <span>4</span> */}
          </Link>
          <Link
            onClick={() => {
              handleChnage(false);
            }}
            to={isAuth ? `/buyer/dashboard` : `/login`}
            className="account-button resp-none"
          >
            <img src={Account} alt="" />
          </Link>
          <button
            type="button"
            onClick={toggleAccount}
            className={`account-button desk-none ${state}`}
          >
            <img src={Account} alt="" />
            <img
              src={CloseButton}
              alt="close button"
              className="close-button"
            />
          </button>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    accountTypeState: state.accountType,
    isAuth: state.isAuth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAccount: (type) =>
      dispatch({ type: CHANGE__ACCOUNT__TYPE, payload: type }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMiddle);
