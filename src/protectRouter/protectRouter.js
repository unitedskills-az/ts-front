import React from "react";

// react router dom
import { Route } from "react-router-dom";
import { useRedirect } from "../hooks";

function ProtecedRouter({
  path,
  isBuyer,
  exact,
  isSuppliers,
  isAuth,
  isStatic,
  isLogin,
  isShow,
  component: Component,
  ...rest
}) {
  const { redirect } = useRedirect();

  return (
    <Route
      path={path}
      exact={exact}
      {...rest}
      render={(props) => {
        if (isBuyer === "buyers" && isAuth) {
          return Component;
        } else if (isSuppliers === "suppliers" && isAuth) {
          return Component;
        } else if (
          isStatic &&
          isShow !== undefined &&
          isShow.pathname.split("/")[1] !== "buyer" &&
          isShow.pathname.split("/")[1] !== "supplier"
        ) {
          return Component;
        } else if (!isAuth && isLogin) {
          return Component;
        } else {
          if (!isStatic) {
            redirect("/404");
          }
        }
      }}
    />
  );
}

export default ProtecedRouter;
