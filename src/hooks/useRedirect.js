import { useHistory } from "react-router";

const useRedirect = () => {
  const hs = useHistory();
  const redirect = (path = "") => {
    hs.push(path);
  };

  return { redirect };
};

export default useRedirect;
