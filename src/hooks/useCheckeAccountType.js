import Storage from "../services/storage";

const useCheckedAccountType = () => {
  if (Storage.getItem("accountType") === "buyers") {
    return { accountType: "buyers", checkedStatus: false };
  } else if (Storage.getItem("accountType") === "suppliers") {
    return { accountType: "suppliers", checkedStatus: true };
  } else {
    return { accountType: "account" };
  }
};

export default useCheckedAccountType;
